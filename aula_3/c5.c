#include <stdio.h>

int main ()
{
	float nota1,nota2,nota3,media_ponderada;

	printf("\nPrimeira nota:");
	scanf("%f",&nota1);	
	
	printf("Segunda nota:");
	scanf("%f",&nota2);
	
	printf("Terceira nota:");
	scanf("%f",&nota3);

	media_ponderada = (((nota1 * 4.0) + (nota2 * 3.0) + (nota3 * 3.0))/10.0);	
	
	printf("\nA média ponderada do aluno foi %f.\n\n",media_ponderada);	
	
	return 0;
}
