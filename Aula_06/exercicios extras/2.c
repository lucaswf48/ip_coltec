#include <stdio.h>

int main ()
{
	int num1, num2, m;

	printf("Digite um número:\n");
	scanf("%d",&num1);
	printf("Digite outro número:\n");
	scanf("%d",&num2);

	m = mdc(num1,num2);

	printf("O mdc dos números digitados é %d\n", m);

	return 0;
}

int mdc(int num1, int num2)
{
	int m,cont = 1,mdc;
	
	while (cont <= num1 && cont <= num2)
	{
		if (num1 % cont == 0 && num2 % cont == 0)
		{
			mdc = cont;			
		}
		cont++;
	}

	return mdc;
}