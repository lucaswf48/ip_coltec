/*4. Escrever uma função int contaimpar(int n1, int n2) que retorna o número de
inteiros impares que existem entre n1 e n2 (inclusive ambos, se for o caso). A função
deve funcionar inclusive se o valor de n2 for menor que n1.
*/
#include <stdio.h>
 
int contaimpar(int n1, int n2);
 
int main()
{
    int n1, n2, total_impar;
    printf("\nDigite o primeiro valor: (número inteiro e positivo)\n");
    scanf("%d",&n1);
    printf("\nDigite o segundo valor: (número inteiro e positivo)\n");
    scanf("%d",&n2);
    if (n2 > 0 || n1 > 0)
    {
        total_impar = contaimpar(n1,n2);
        printf("O valor de número impares no intervalor é: %d\n\n", total_impar);
    }
    else
    {
        printf("Numero invalido!\n\n");
    }
     
    return 0;
}
 
int contaimpar(int n1, int n2)
{
    int cont = 0;
    //Se o primeiro número for o maior
    if (n1 > n2)
    {
        while(n1 >= n2)
        {
            if (n1 % 2 != 0)
                cont++;
             
            n1--;
        }
    }
    //Se o segundo número for o maior
    else if(n1 < n2)
    {
        while(n2 >= n1)
        {
            if(n2 % 2 != 0)
                cont++;
 
            n2--;
        }   
    }
    //Se os números forem iguais
    if (n1 == n2)
    {
        if (n1 % 2 == 0)
        {
            cont = 0;
        }
        else
        {
            cont = 1;
        }
    }
    return cont;
}