/*3. Faça um programa para resolver uma equação de segundo grau na forma
ax2 + bx + c. A resolução deve ser feita por meio de um procedimento.
*/
#include <stdio.h>
#include <math.h>

void segundo(int a, int b,int c);
int main ()
{
	
	float a,b,c;

	printf("Digite o coeficiente 'a' da equação do segundo grau:\n");
	scanf("%f",&a);
	printf("Digite o coeficiente 'b' da equação do segundo grau:\n");
	scanf("%f",&b);
	printf("Digite o coeficiente 'c' da equação do segundo grau:\n");
	scanf("%f",&c);

	segundo(a,b,c);

	return 0;
}

void segundo(int a, int b,int c)
{
	float x, x1,delta;

	delta = pow(b,2) - 4 * a * c;
	printf("%f\n", delta);

	if (delta > 0)
	{
		x = (-b + sqrt(delta))/(2*a);
		x1 = (-b - sqrt(delta))/(2*a);
		printf("As raizes da equação são: %.2f e %.2f.\n", x,x1);
	}
	else if(delta==0)
	{
		x = (-b + sqrt(delta))/(2*a);
		printf("Delta é igual a zero, então a unica raiz é %.2f\n", x);
	}
	else
	{
		printf("Não a numero real como raiz..\n");
	}
		
	

}