#include <stdio.h>

float soma(float a, float b);

float produto(float a, float b);

float quociente(float a, float b);


int main ()

{
	int op;

	printf("Escolha uma opção de cálculo para dois números:\n");
	printf("1) Soma\n");
	printf("2) Produto\n");
	printf("3) Quociente\n");
	printf("4) Sair\n");
	printf("Opção:");
	scanf("%d",&op);

	if(op!=4){
		leia(op);
	}
	else{
		printf("Você saiu.......\n");
	}
	
	//printf("\n\n\n %d\n", op);

	return 0;
}

int leia(int op)
{
	float a,b,r;
	printf("Digite um número:\n");
	scanf("%f",&a);
	printf("Digite outro número:\n");
	scanf("%f",&b);

	switch (op)
	{
		case 1:
			r = soma(a,b);
			printf("A soma é: %.2f\n",r);
			break;
		case 2:
		    r = produto(a,b);
			printf("O produto é: %.2f\n",r);
			break;
	    case 3:
	    	r = quociente(a,b);
			printf("O quociente é: %.2f\n",r);
			break;
		
	}

}

float soma(float a, float b)
{
	return a+b;	
}


float produto(float a, float b)
{
	return a*b;	
}

float quociente(float a, float b)
{
	return a/b;	
}

