/*4. Escreva uma programa que calcule e imprima o quadrado de um número. O
cálculo deve ser feito por uma função. Repita a operação até que seja lido um
número igual a zero.*/
#include <stdio.h>

float quad(int num);
int main ()

{
	float num,r;
	do {
		
			printf("Digite um número do qual você deseja o quadrado:\n");
			scanf("%f",&num);
			if (num!=0)
			{
				r =quad(num); 
				printf("O quadrado do número digitado é: %.2f\n", r);
			}

	} while(num != 0);


	return 0;
}

float quad(int num)
{
	return num*num;
}


