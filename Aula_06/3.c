/*3. Faça um procedimento que receba por parâmetro o tempo de duração de
um experimento expresso em segundos e imprima na tela esse mesmo tempo
em horas, minutos e segundos.
*/
#include <stdio.h>

void conc(int segundos);

int main ()

{
	int segundos;

	printf("Digite o tempo da experiencia(em segundos):\n");
	scanf("%d",&segundos);

	conc(segundos);

	return 0;
}

void conc(int segundos)
{
	int horas,minutos;

	horas = segundos / 3600;
	minutos = (segundos % 3600)/60;
	segundos = ((segundos % 3600)%60);

	printf("A experiencia durou %d hora(s) %d minuto(s) %d segundo(s).\n",horas,minutos,segundos);
}