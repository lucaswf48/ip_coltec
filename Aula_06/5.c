/*5. Faça uma função que receba um valor N inteiro e positivo e que calcula o
fatorial deste valor. Retorne o resultado.
*/
#include <stdio.h>

int fat(int num);

int main ()

{
	int num;
	long double r;

	printf("Digite um número inteiro é positivo:\n");
	scanf("%d",&num);
	r = fat(num);
	printf("O fatorial do número digitado é: %.0Lf\n", r);
	return 0;
}

int fat(int num)
{
	int cont = 1;
	long double fatorial = 1;

	while (cont <= num) 
	{
		fatorial = fatorial * cont;
		++cont;
	}
	return fatorial;
}