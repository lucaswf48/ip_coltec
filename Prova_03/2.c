#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct{
	char nome[30];
	float nota[3];
}Disciplina;

typedef struct{
	char nome[30];
	int matricula;
	Disciplina disciplinas[5];
}Aluno;


Aluno* alocaTurma(int n){	
	 Aluno *p;
	 p =(Aluno*)malloc(n*sizeof(Aluno));
	 return p;
}

void LeTurma(Aluno t[],int n){

	int a,b,c;

	for(a=0;a<n;++a){
		
		printf("Digite seu nome:\n");
		scanf("%s",t[a].nome);
		__fpurge(stdin);

		printf("Digite sua matricula:\n");
		scanf("%d",&t[a].matricula);
		__fpurge(stdin);

		for(b=0;b<5;++b){
			printf("Digite o nome da sua disciplina:\n");
			scanf("%s",t[a].disciplinas[b].nome);
			__fpurge(stdin);

			for (c=0;c < 3;++c){
				//printf("Digite sua nota de numero %d:\n",c+1);
				//scanf("%f",&t[a].disciplinas[b].nota[c]);
				srand(time(NULL));
				t[a].disciplinas[b].nota[c] = rand()%101;
				__fpurge(stdin);

			}
			
		}
	}

}

float calculaMediaAlunoPorDisciplina(Aluno t[],int n,int alu,int mat){

	int a;
	float media = 0.0;
	
	for (a=0;a < 3;++a){
		
		media += t[alu].disciplinas[mat].nota[a];
	}

	media = media/3.0;
	
	return media;
}

int main(){

	Aluno *Turma;
	int n,i,j;
	float media;

	printf("Digite o numero de alunos\n");
	scanf("%d",&n);
	Turma = alocaTurma(n);
	LeTurma(Turma,n);
	
	for(i=0;i<n;i++){
		for(j=0;j<5;j++){
			media=calculaMediaAlunoPorDisciplina(Turma,n,i,j);
			
			printf("Aluno: %s Matricula: %d Diciplina: %s Media: %f\n",Turma[i].nome,Turma[i].matricula,Turma[i].disciplinas[j].nome,media);
		}
		printf("\n");
	}

	free(Turma);
	return 0;
}
