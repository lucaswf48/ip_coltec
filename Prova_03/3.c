#include <stdio.h>
#include <stdlib.h>

void menu(){
	printf("Digite:\n");
	printf("1) Para a letra 'a'\n");
	printf("2) Para a letra 'b'\n");
	printf("0) Para sair\n");
}

void letraA(){

	int m,n;
	do{
		printf("Digite um valor:");
		scanf("%d",&m);
		printf("Digite outro valor:");
		scanf("%d",&n);
	}while(m < 0 || n < 0);

	printf("Resposta: %d.\n",ackermann(m,n)); 

}

int ackermann(int m,int n){

	if(m==0)
		return n+1;
	else{
		if(m > 0 && n == 0)
			ackermann(m-1,1);
		else{
			ackermann(m-1,ackermann(m,n-1));
		}
	}
}

float* alocaVetor(int tam){

	float *vet;

	vet = (float*)calloc(tam,sizeof(float));

	return vet;
}

void ler_vetor_float(float vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		printf("Digite o elemento %d\n", j+1);
		scanf("%f",&vet[j]);
	}
	printf("\n");
}

float mediaVetor(float *v,int n,int j,float soma){

	if(j==n)
		return soma/n;
	else
		mediaVetor(v,n,j+1,soma+=v[j]);

		//somaVetorRecursivo(vet,tamanho,a=a+1,soma += vet[a]);
}

void letraB(){

	int n;
	float *ve;
	printf("Digite o numero de elementos do vetor:\n");
	scanf("%d",&n);
	ve=alocaVetor(n);
	ler_vetor_float(ve,n);

	printf("A media dos elementos do vetor é: %f\n",mediaVetor(ve,n,0,0));

}


int main(){

	int escolha;

	menu();
	scanf("%d",&escolha);
	switch(escolha){
			case 1:
				letraA();
				break;
			case 2:
				letraB();
				break;
			case 0:
				break;
			default:
				printf("Opção invalida\nTente novamente.");
				break;
		}

	
	return 0;
}
