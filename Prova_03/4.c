#include <stdio.h>
#include <stdlib.h>

int** aloca_matriz_int(int j,int i){
	 
	 int **p,cont;
	 
	 p = (int **)calloc(j,sizeof(int *));
	 if(p == NULL){
	 	printf("ERRO");
	 	exit(0);
	 }
	 for(cont = 0;cont < j;++cont){
	 	p[cont] = (int *)calloc(i,sizeof(int));
	 	if(p == NULL){
	 		printf("ERRO");
	 		exit(0);
	 	}
	 }
	 return p;
}

void ler(int **p,int j,int i){
	 int cont1,cont;
	 for(cont = 0;cont < j;++cont){
	 	for(cont1 = 0;cont1 < i;++cont1){
	 		srand(time(NULL));
	 		 printf("Digite o elemento %d %d: ",cont,cont1);
			 scanf("%d",&p[cont][cont1]);
			 //p[cont][cont1] = rand()%101;
	 	}
	 }

	 printf("Matriz gerada:\n");
	 for (cont=0;cont<j;++cont){
		for (cont1=0;cont1<j;++cont1){
			printf("%d ", p[cont][cont1]);
		}
		printf("\n");
	}

	
}

void menu(){
	printf("Digite:\n");
	printf("1) Para a letra 'a'\n");
	printf("2) Para a letra 'b'\n");
	printf("0) Para sair\n");
}

void transposta(int **a,int **b,int n){

	int j,i;

	for (i=0;i<n;++i){
		for (j=0;j<n;++j){
			a[j][i]=b[i][j];
		}
	}

}

void inversa(int **a,int **b,int n){

	int j,i;

	for (i=0;i<n;++i){
		for (j=0;j<n;++j){
			if(i!=j)
				a[i][j]=b[i][j]*(-1);
			else
				a[i][j]=b[i][j];
		}
	}

}

void compara(int **a,int **b,int n){

	int i,j;

	/*for (i=0;i<n;++i){
		for (j=0;j<n;++j){
			printf("%d ", b[i][j]);
		}
		printf("\n");
	}
	printf("\n\n");
	for (i=0;i<n;++i){
		for (j=0;j<n;++j){
			printf("%d ", a[i][j]);
		}
		printf("\n");
	}*/

	for (i=0;i<n;++i){
		for (j=0;j<n;++j){
			if(a[i][j]!=b[i][j]){
				printf("Matriz não é anti-simetrica\n");
				exit(0);
			}

		}
	}

	printf("Matriz anti-simetrica\n");
}


void letraA(){

	int n;
	int **mat,**matTransposta,**matOposta;
	printf("Qual e o numero de linhas e de colunas da matriz:\n(Numero de linhas é igual ao numero de colunas)\n");
	scanf("%d",&n);
	mat = aloca_matriz_int(n,n);
	matTransposta = aloca_matriz_int(n,n);
	matOposta = aloca_matriz_int(n,n);
	ler(mat,n,n);
	transposta(matTransposta,mat,n);
	inversa(matOposta,mat,n);
	compara(matTransposta,matOposta,n);
}

void letraB(){

	int n,cont,cont1,d=0;
	int **mat;
	printf("Qual e o numero de linhas e de colunas da matriz:\n(Numero de linhas é igual ao numero de colunas)\n");
	scanf("%d",&n);
	mat=aloca_matriz_int(n,n);
	for(cont = 0;cont < n;++cont){
		mat[cont][cont]=1;
	}
	for(cont=n-1;cont >=0;--cont){
		mat[d][cont]=1;
		d++;
	}
	printf("Matriz gerada:\n");
	for (cont=0;cont<n;++cont){
		for (cont1=0;cont1<n;++cont1){
			printf("%d ", mat[cont][cont1]);
		}
		printf("\n");
	}

}

int main(){

	int escolha;
	menu();
	scanf("%d",&escolha);
	switch(escolha){
			case 1:
				letraA();
				break;
			case 2:
				letraB();
				break;
			case 0:
				break;
			default:
				printf("Opção invalida\nTente novamente.");
				break;
	}
	return 0;
}
