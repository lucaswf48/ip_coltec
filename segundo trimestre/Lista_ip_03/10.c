/*10. Fa¸ca uma fun¸c˜ao recursiva para exibir os n´umeros compreendidos entre a e b, sendo a e b
parˆametros da fun¸c˜ao. Se b for menor que a, exiba uma mensagem de erro*/
#include <stdio.h>

int r(int a,int b){


	if(a <= b){
		printf("%d\n", a);
		return r(a + 1,b);
	}
	else{
		return 0;
	}

}


int main(){
	
	int a,b;
	
	printf("Digite o valor de 'a'\n");
	scanf("%d",&a);
	__fpurge(stdin);
	
	printf("Digite o valor de 'b'\n");	
	scanf("%d",&b);
	__fpurge(stdin);
	
	if(a < b){
		printf("Intervalo:\n");
		r(a,b);
	}
	else
		printf("tente novamente");
	return 0;
}