/*1. Faca uma funcao que recebe, por parametro, uma matriz A(5,5) e retorna a soma dos seus elementos*/

#include <stdio.h>
#include <time.h>
#define DIM 5

int somaMatriz(int mat[DIM][DIM],int j,int i){

	int r = 0;
	
	for(j = 0;j < DIM;j++){
		for(i = 0;i < DIM;i++){
			r += mat[j][i];
		}
	}

	
	return r;
}


int main(){

	int matriz1[DIM][DIM],
		j,				  
		i;				  

	
	for(j = 0;j < DIM;j++){
		for(i = 0;i < DIM;i++){
			srand(time(NULL));
			matriz1[j][i] = rand()%1000;		
		}
	}

	printf("\nMatriz gerada:\n");

	
	for(j = 0;j < DIM;j++){
		for(i = 0;i < DIM;i++){
			
			printf("%d ", matriz1[j][i]);
		}
		printf("\n");
	}

	
	printf("\n\nSoma dos seus elementos: %d.\n\n",somaMatriz(matriz1,j,i));

	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*2. Fa ̧ca uma funcao que recebe, por parametro, uma matriz A(6,6) e retorna a soma dos elementos
da sua diagonal principal e da sua diagonal secund́aria.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 6


int** ler_matriz_int(int j,int i){
 
    int **p,    
        cont,   
        cont1;  

    
    p = (int **)malloc(j*sizeof(int *));
     
    
    if(p == NULL){
        printf("ERRO");
        exit(0);
    }
     
 
    for(cont = 0;cont < j;++cont){
        
     
        p[cont] = (int *)malloc(i*sizeof(int));
        
     
        if(p == NULL){
            printf("ERRO");
            exit(0);
        }
    }
     
    for(cont = 0;cont < j;++cont){
     
        for(cont1 = 0;cont1 < i;++cont1){
     
            srand(time(NULL));
            p[cont][cont1] = rand()%1000;
        }
    }
     
    
    return p;
}


int somaDiagonal(int **m,int *soma){
	
    int cont,               
        soma_primeira = 0,  
        d = 0;              
	
    for(cont = 0;cont < DIM;cont++){
        
        
        soma_primeira += m[cont][cont];
    }

    for(cont = DIM - 1;cont >= 0;--cont){
     
        *soma += m[d][cont];
     
        d++;
    }

    return soma_primeira;
}



int main(){

	int **matriz,     
        soma1 = 0,    
        soma2 = 0,    
        j,            
        i;	          
	
    
    
    matriz = ler_matriz_int(DIM,DIM);
	
     
	soma1 = somaDiagonal(matriz,&soma2); 
    printf("\nMatriz gerada:\n\n");
    for(j = 0;j < DIM;++j){
        for(i = 0;i < DIM;i++){
            printf("%d ", matriz[j][i]);
        }
        printf("\n");
    }

    printf("\nSoma da primeira diagonal: %d\n", soma1);
    printf("Soma da segunda diagonal: %d\n\n", soma2);
	
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*3. Fa¸ca uma funcao que recebe, por parametro, uma matriz A(7,6) e retorna a soma dos elementos
da linha 5 e da coluna 3.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define DIM1 7
#define DIM2 6


int** ler_matriz_int(int j,int i){
 
    int **p,    
        cont,   
        cont1;  
 
    
    p = (int **)malloc(j*sizeof(int *));
     
    
    if(p == NULL){
        printf("ERRO");
        exit(0);
    }
     
 
    for(cont = 0;cont < j;++cont){
        
        
        p[cont] = (int *)malloc(i*sizeof(int));
        
        
        if(p == NULL){
            printf("ERRO");
            exit(0);
        }
    }
     
    for(cont = 0;cont < j;++cont){
     
        for(cont1 = 0;cont1 < i;++cont1){
     
            srand(time(NULL));
            p[cont][cont1] = rand()%1000;
        }
    }
     
    
    return p;
}


int somaLinhaEColuna(int **m,int *s){

	int res = 0, 
        cont;    

    
	for(cont = 0;cont < DIM2;++cont){
		res += m[5][cont]; 
	}

    
	for(cont = 0;cont < DIM1;++cont){
		*s += m[cont][3]; 
	}
	return res;
}

int main(){
	
	int **matriz,  
        soma1 = 0, 
        soma2 = 0, 
        j,         
        i;         

    
	matriz = ler_matriz_int(DIM1,DIM2);

	soma1 = somaLinhaEColuna(matriz,&soma2);

    printf("\nMatriz gerada:\n\n");
	for(j = 0;j < DIM1;++j){
        for(i = 0;i < DIM2;i++){
            printf("%d ", matriz[j][i]);
        }
        printf("\n");
    }

    printf("\nA soma dos elementos da linha 5 é: %d\n", soma1);
    printf("A soma dos elementos da coluna 3 é: %d\n\n", soma2);

	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*4. Faca uma funcao que recebe, por parametro, uma matriz A(6,6) e retorna o menor elemento da
sua diagonal secundaria.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 6

int** ler_matriz_int(int j,int i){
 
    int **p,cont,cont1;
 
    p = (int **)malloc(j*sizeof(int *));
     
    if(p == NULL){
        printf("ERRO");
        exit(0);
    }
     
 
    for(cont = 0;cont < j;++cont){
     
        p[cont] = (int *)malloc(i*sizeof(int));
     
        if(p == NULL){
            printf("ERRO");
            exit(0);
        }
    }
     
    for(cont = 0;cont < j;++cont){
     
        for(cont1 = 0;cont1 < i;++cont1){
            srand(time(NULL));
            //scanf("%d",&p[cont][cont1]);
            p[cont][cont1] = (rand()%1000);
        	
        }
    }
     
 
    return p;
}

int somaDiagonal(int **m,int *soma){
	
    int cont,testa,d=0;
	
    testa = m[0][DIM - 1];

    for(cont = DIM - 1;cont >= 0;--cont){
       if(testa > m[d][cont])
      		testa = m[d][cont];
       d++;
    }

    return testa;
}



int main(){

	int **matriz,soma1 = 0,soma2 = 0,j,i;	
	matriz = ler_matriz_int(DIM,DIM);
	
	soma1 = somaDiagonal(matriz,&soma2);
    printf("\nMatriz gerada:\n\n");
    for(j = 0;j < DIM;++j){
        for(i = 0;i < DIM;i++){
            printf("%d ", matriz[j][i]);
        }
        printf("\n");
    }
    printf("\nMenor elemento:\n");
    printf("%d\n", soma1);
    
	
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*5. Faca um procedimento que receba duas matrizes de dimensoes especificadas pelo usuario e retorne
a multiplicacao de uma pela outra, quando possıvel.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int** aloca_matriz_int(int j,int i){

	int **p,cont;

	p = (int **)calloc(j,sizeof(int *));
	
	if(p == NULL){
		printf("ERRO");
		exit(0);
	}
	

	for(cont = 0;cont < j;++cont){
	
		p[cont] = (int *)calloc(i,sizeof(int));
	
		if(p == NULL){
			printf("ERRO");
			exit(0);
		}
	}
	
	

	return p;
}

void ler(int **p,int j,int i){

	int cont1,cont;
	for(cont = 0;cont < j;++cont){
	
		for(cont1 = 0;cont1 < i;++cont1){
			srand(time(NULL));
			//scanf("%d",&p[cont][cont1]);
			p[cont][cont1] = rand()%101;
		}
	}
}

int main(){

	int linhamatriz1,
		colunamatriz1,
		linhamatriz2,
		colunamatriz2,
		j,
		i,
		r;
	int **matriz1,
		**matriz2,
		**matriz_resultado;
	
	do{

		printf("Digite a quantidade de linhas da primeira matriz: ");
		scanf("%d",&linhamatriz1);
		printf("Digite a quantidade de colunas da primeira matriz: ");
		scanf("%d",&colunamatriz1);

		printf("Digite a quantidade de linhas da segunda matriz:");
		scanf("%d",&linhamatriz2);
		printf("Digite a quantidade de colunas da segunda matriz:");
		scanf("%d",&colunamatriz2);

		printf("\n");
	}while(linhamatriz2 != colunamatriz1);

	matriz1 = aloca_matriz_int(linhamatriz1,colunamatriz1);
	matriz2 = aloca_matriz_int(linhamatriz2,colunamatriz2);
	matriz_resultado = aloca_matriz_int(linhamatriz1,colunamatriz2);

	ler(matriz1,linhamatriz1,colunamatriz1);
	ler(matriz2,linhamatriz2,colunamatriz2);

	for(i=0;i<linhamatriz1;i++){	
		for(j=0;j<colunamatriz2;j++){
			for(r=0;r<colunamatriz1;r++){
				matriz_resultado[i][j]+=(matriz1[i][r]*matriz2[r][j]);
			}
		}
		printf("\n");
	}
	
	printf("Primeira matriz gerada:\n");
	for(j = 0;j < linhamatriz1;++j){
		for(i = 0;i < colunamatriz1;++i){
			printf("%d ", matriz1[j][i]);
		}
		printf("\n");
	}

	printf("\n");

	printf("Segunda matriz gerada:\n");
	for(j = 0;j < linhamatriz2;++j){
		for(i = 0;i < colunamatriz2;++i){
			printf("%d ", matriz2[j][i]);
		}
		printf("\n");
	}

	printf("\n");

	printf("Matriz gerada:\n");
	for(j = 0;j < linhamatriz1;++j){
		for(i = 0;i < colunamatriz2;++i){
			printf("%d ", matriz_resultado[j][i]);
		}
		printf("\n");
	}
	
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Questão 6)
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct pontos{
	float x;
	float y;
	float z;
}pot;

void continua(){
	printf("Digite qualquer tecla para continuar:\n");
	scanf("%*c");
	__fpurge(stdin);
}


pot* aloca_estrutura(int *n){
	system("clear");
	pot *p;

	printf("Quantos pontos:\n");
	scanf("%d",n);

	p =(pot*)malloc(*n*sizeof(pot));

	return p;
}

void ler_pontos(pot *p, int n){

	int cont;
	for(cont = 0;cont < n;++cont){
		printf("Ponto %d\n", cont+1);
		scanf("%f %f %f",&p[cont].x,&p[cont].y,&p[cont].z);
		__fpurge(stdin);
	}
	system("clear");
}

void imprimir_pontos(pot *p,int n){

	system("clear");
	int cont;
	for(cont = 0;cont < n;++cont){
		printf("Ponto %d\n", cont+1);
		printf("%.3f, %.3f, %.3f \n", p[cont].x,p[cont].y,p[cont].z);
	}
	continua();
	system("clear");
}

float distancia(pot *p,int *n){

	float dis;
	int a,b;
	system("clear");
	pedir_ponto:
	printf("Quais pontos \n");
	scanf("%d %d",&a,&b);
	a--;
	b--;
	if (a>*n - 1|| b > *n-1){
		printf("Ponto invalido\n");
		goto pedir_ponto;
	}	
		

	__fpurge(stdin);
	
	dis = pow(p[a].x - p[b].x,2)+pow(p[a].y - p[b].y,2)+pow(p[a].z - p[b].z,2);
	dis = pow(dis,0.5);

	return dis;

}

float distancia_consecutiva(pot *p,int n){
	
	int cont,dis;

	for(cont = 0;cont + 1 < n;++cont){
		printf("Distancia dos pontos %d e %d\n", cont+1,cont+2);
		dis = pow(p[cont].x - p[cont+1].x,2)+pow(p[cont].y - p[cont+1].y,2)+pow(p[cont].z - p[cont+1].z,2);
		dis = pow(dis,0.5);
		printf("%d\n", dis);	
	}
	
}

int main(){

	char escolha;
	pot *pon = NULL;
	int n = 0;
	float d;
	system("clear");
	do{
		printf("a - Para ler n pontos\n");
		printf("b - Para imprimir os pontos lidos\n");
		printf("c - Para calcular as distância de 2 pontos(a escolha do usuário)\n");
		printf("d - Para calcular as distância de todos os pontos consecutivos\n");
		printf("q - Para sair do programa\n");
		printf("Escolha:");
		scanf("%c",&escolha);
		__fpurge(stdin);

		switch(escolha){
			case 'a':
				pon = aloca_estrutura(&n);
				ler_pontos(pon,n);
				break;
			case 'b':
				imprimir_pontos(pon,n);			
				break;		
			case 'c':
				if (n > 1)
				{
					d = distancia(pon,&n);
					printf("A distancia é\n");
					printf("%.3f\n", d);
					continua();
					system("clear");
				}
				else{
					printf("Insira mais de um ponto\n");
					continua();
					system("clear");
				}
				
				break;
			case 'd':
				if(n > 1){
					distancia_consecutiva(pon,n);
					continua();
					system("clear");
				}
				else{
					printf("Insira mais de um ponto\n");
					continua();
					system("clear");
				}
				break;
			case 'q':
					printf("chau\n");
					break;
			default:
			        printf("OPÇÃO INVALIDA.\n");
			        continua();
			        system("clear");
			        break;
		}
		

	}while(escolha != 'q');
	system("clear");
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Questão 7)
#include <stdio.h>

#define DIM 10
#define DIM1 10

typedef struct livro{
	
	char titulo[DIM];
	char autor[DIM];
	char editora[DIM];
	int ano;
	int emprestado;
	int codigo;

}liv;

void continua(){
	printf("Digite qualquer tecla para continuar:\n");
	scanf("%*c");
}

void menu(){

	printf("Digite:\n");
	printf("1 - Para incluir livro\n");
	printf("2 - Para listar livros cadastrados\n");
	printf("3 - Para emprestar livro\n");
	printf("4 - Para devolver livro\n");
	printf("5 - Para listar os livros emprestados\n");
	printf("6 - Para listar o livros disponiveis para emprestimo\n");
	printf("0 - Para sair\n");

}

int incluirLivro(liv p[]){

	int a = 0,pode = 0;
	
	
	for(a = 0;a < DIM1;++a){
		if (p[a].codigo == -1)
		{
			pode++;
		}
		//printf("%d\n", p[a].codigo);	
	}
	if (pode == 0){

		printf("cheio\n");
		return 0;
	}

	for(a = 0;a < DIM1;++a){

		if(p[a].codigo == -1){
			break;
		}
	}
	

	printf("Digite o nome do livro:\n");
	fgets(p[a].titulo,50,stdin);
	__fpurge(stdin);
	
	printf("Digite o nome do autor:\n");
	fgets(p[a].autor,50,stdin);
	__fpurge(stdin);
	
	printf("Digite o nome da editora:\n");
	fgets(p[a].editora,50,stdin);
	__fpurge(stdin);
	
	printf("Digite o ano de publicação:\n");
	scanf("%d",&p[a].ano);
	__fpurge(stdin);
	
	printf("Digite o codigo do livro:\n");
	scanf("%d",&p[a].codigo);
	__fpurge(stdin);

	p[a].emprestado = 1;
	continua();
	system("clear");

}

int listarLivros(liv p[]){

	int cont;

	for(cont = 0;cont < DIM1;++cont){
		if(p[cont].codigo != -1){
			printf("Titulo do livro:\n");
			printf("%s",p[cont].titulo);
			printf("Situaçao do livro:\n");
			printf("%d\n",p[cont].emprestado);
			printf("\n"); 	
		}
	}
	continua();
	system("clear");
}

void emprestaLivro(liv em[]){
	int n,cont;
	printf("Qual o codigo do livro:\n");
	scanf("%d",&n);
	__fpurge(stdin);

	for(cont = 0;cont < DIM;++cont){
		if(n==em[cont].codigo){
			printf("Livro emprestado com sucesso.\n");
			em[cont].emprestado=0;
			cont=-1;
			break;
		}
	}

	if (cont != -1){
		printf("O livro não esta disponivel\n");
	}

	continua();
	system("clear");

}

void devolveLivro(liv em[]){

	int n,cont;
	printf("Qual o codigo do livro:\n");
	scanf("%d",&n);
	__fpurge(stdin);

	for(cont = 0;cont < DIM;++cont){
		if(n==em[cont].codigo){
			printf("Livro devolvido com sucesso.\n");
			em[cont].emprestado=1;
			cont=-1;
			break;
		}
	}

	if (cont != -1){
		printf("O livro não esta disponivel\n");
	}

	continua();
	system("clear");

}

void listaDisponivel(liv em[]){

	int cont;

	for(cont = 0;cont < DIM;++cont){
		if(em[cont].emprestado==1){
			printf("Livro disponivel:");
			printf("%s", em[cont].titulo);
			printf("Codigo:");
			printf("%d\n", em[cont].codigo);
		}
	}
	printf("\n");
	continua();
	system("clear");
}

void listaEmprestado(liv em[]){
	int cont;

	for(cont = 0;cont < DIM;++cont){
		//printf("daw %d\n", em[cont].codigo);
		if(em[cont].emprestado==0 && em[cont].codigo != -1){
			printf("Livro emprestado:");
			printf("%s", em[cont].titulo);
			printf("Codigo:");
			printf("%d\n", em[cont].codigo);
		}
	}
	printf("\n");
	continua();
	system("clear");
}


int main(){

	liv vet[DIM1];
	int cont;
	int escolha;

	for(cont = 0;cont < DIM1;++cont){
		vet[cont].codigo = -1;
	}

	
	system("clear");
	do{
	
		menu();
		scanf("%d",&escolha);
		__fpurge(stdin);
		if(escolha == 0)
			break;

		switch(escolha){
			case 1:
				system("clear");
				incluirLivro(vet);
				break;
			case 2:
				system("clear");
				listarLivros(vet);
				break;
			case 3:
				system("clear");
				emprestaLivro(vet);
				break;
			case 4:
				system("clear");
				devolveLivro(vet);
				break;
			case 5:
				system("clear");
				listaEmprestado(vet);
				break;
			case 6:
				system("clear");
				listaDisponivel(vet);
				break;
			case 0:
				system("clear");
				exit(0);
				break;
			default:
				printf("Opção invalida!!!!!!!!\n");
				continua();
				system("clear");
				break;
		}


	}while(1);

	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*8. Faca uma funcao recursiva para exibir na tela de 1 a 10.*/
#include <stdio.h>

int r(int a){

	if(a < 11){
		printf("%d\n", a);
		return r(a + 1);
	}
	else{
		return 0;
	}

}


int main(){
	
	r(1);
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*9. Faca uma funcao recursiva para exibir na tela os numeros ımpares de 1 a 30.*/
#include <stdio.h>

int r(int a){

	if(a < 31){
		printf("%d\n", a);
		return r(a + 2);
	}
	else{
		return 0;
	}

}


int main(){
	
	r(1);
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*10. Faca uma funcao recursiva para exibir os numeros compreendidos entre a e b, sendo a e b
parametros da funcao. Se b for menor que a, exiba uma mensagem de erro*/
#include <stdio.h>

int r(int a,int b){


	if(a <= b){
		printf("%d\n", a);
		return r(a + 1,b);
	}
	else{
		return 0;
	}

}


int main(){
	
	int a,b;
	
	printf("Digite o valor de 'a'\n");
	scanf("%d",&a);
	__fpurge(stdin);
	
	printf("Digite o valor de 'b'\n");	
	scanf("%d",&b);
	__fpurge(stdin);
	
	if(a < b){
		printf("Intervalo:\n");
		r(a,b);
	}
	else
		printf("tente novamente");
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*11. Pesquise a sequencia de Fibonacci e faca uma funcao recursiva para exibir seus n primeiros termos,
sendo n um parametro da funcao.*/

#include <stdio.h>

int r(int a)
{
   if(a==1 || a==2)
       return 1;
   else
       return r(a-1) + r(a-2); 
}

int main()
{
   int n,i;
  printf("Digite um valor para 'n'\n");
  scanf("%d",&n);
  printf("%d primeiros valores:\n", n);
  
  for(i=0; i<n; i++){
      printf("%d, ",r(i+1));
  }

  printf("\n");
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*12. Fa¸ca uma funcao recursiva que receba um vetor de numeros reais e seu tamanho e retorne a soma
de seus elementos*/

#include <stdio.h>
#include <time.h>
#define DIM 10

float r(float d[],int dimen,float acc){
	if(dimen - 1>=0){
		return r(d,dimen - 1,acc+=d[dimen-1]);
	}
	else{
		return acc;
	}
}

int main(){
	
	int a;
	float v[DIM],rr,acc;
	printf("\nVetor gerado:\n");
	for(a = 0;a < DIM;++a){
		srand(time(NULL));
		v[a] = rand()%101;

		printf("%.3f\n", v[a]);
	}
	printf("\nSoma dos seus elementos:\n");
	rr = r(v,DIM,acc);
	printf("%.3f\n", rr);
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*13. Faca uma funcao recursiva que receba um vetor de numeros reais e seu tamanho e retorne o menor
elemento deste vetor.*/

#include <stdio.h>
#include <time.h>
#define DIM 10
int r(float vv[],int d,int m){
	if(d - 1>=0){
		return r(vv,d - 1,m > vv[d-1]? vv[d-1] : m);
	}
	else{
		return m;
	}
}

int main(){
	
	int a;
	float v[DIM],rr,m;
	printf("\nVetor gerado:\n");
	for(a = 0;a < DIM;++a){
		srand(time(NULL));
		//scanf("%f",&v[a]);
		v[a]=rand()%100;
		printf("%.3f\n", v[a]);
	}
	m = v[0];
	rr = r(v,DIM,m);
	printf("\nMenor elemento:\n");
	printf("%.3f\n", rr);
	return 0;
}