/*1. Faca uma funcao que recebe, por parametro, uma matriz A(5,5) e retorna a soma dos seus elementos*/

#include <stdio.h>
#include <time.h>
#define DIM 5

int somaMatriz(int mat[DIM][DIM],int j,int i){

	int r = 0;
	
	for(j = 0;j < DIM;j++){
		for(i = 0;i < DIM;i++){
			r += mat[j][i];
		}
	}

	
	return r;
}


int main(){

	int matriz1[DIM][DIM],
		j,				  
		i;				  

	
	for(j = 0;j < DIM;j++){
		for(i = 0;i < DIM;i++){
			srand(time(NULL));
			matriz1[j][i] = rand()%1000;		
		}
	}

	printf("\nMatriz gerada:\n");

	
	for(j = 0;j < DIM;j++){
		for(i = 0;i < DIM;i++){
			
			printf("%d ", matriz1[j][i]);
		}
		printf("\n");
	}

	
	printf("\n\nSoma dos seus elementos: %d.\n\n",somaMatriz(matriz1,j,i));

	return 0;
}