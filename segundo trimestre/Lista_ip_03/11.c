/*11. Pesquise a sequˆencia de Fibonacci e fa¸ca uma fun¸c˜ao recursiva para exibir seus n primeiros termos,
sendo n um parˆametro da fun¸c˜ao.*/

#include <stdio.h>

int r(int a)
{
   if(a==1 || a==2)
       return 1;
   else
       return r(a-1) + r(a-2); 
}

int main()
{
   int n,i;
  printf("Digite um valor para 'n'\n");
  scanf("%d",&n);
  printf("%d primeiros valores:\n", n);
  
  for(i=0; i<n; i++){
      printf("%d, ",r(i+1));
  }

  printf("\n");
  return 0;
} 

