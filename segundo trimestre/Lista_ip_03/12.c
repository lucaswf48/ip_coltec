/*12. Fa¸ca uma fun¸c˜ao recursiva que receba um vetor de n´umeros reais e seu tamanho e retorne a soma
de seus elementos*/

#include <stdio.h>
#include <time.h>
#define DIM 10

float r(float d[],int dimen,float acc){
	if(dimen - 1>=0){
		return r(d,dimen - 1,acc+=d[dimen-1]);
	}
	else{
		return acc;
	}
}

int main(){
	
	int a;
	float v[DIM],rr,acc;
	printf("\nVetor gerado:\n");
	for(a = 0;a < DIM;++a){
		srand(time(NULL));
		v[a] = rand()%101;

		printf("%.3f\n", v[a]);
	}
	printf("\nSoma dos seus elementos:\n");
	rr = r(v,DIM,acc);
	printf("%.3f\n", rr);
	return 0;
}