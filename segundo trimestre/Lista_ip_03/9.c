/*9. Fa¸ca uma fun¸c˜ao recursiva para exibir na tela os n´umeros ´ımpares de 1 a 30.*/
#include <stdio.h>

int r(int a){

	if(a < 31){
		printf("%d\n", a);
		return r(a + 2);
	}
	else{
		return 0;
	}

}


int main(){
	
	r(1);
	return 0;
}