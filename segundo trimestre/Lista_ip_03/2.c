/*2. Fa ̧ca uma fun ̧c ̃
ao que recebe, por parˆametro, uma matriz A(6,6) e retorna a soma dos elementos
da sua diagonal principal e da sua diagonal secund ́aria.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 6


int** ler_matriz_int(int j,int i){
 
    int **p,    
        cont,   
        cont1;  

    
    p = (int **)malloc(j*sizeof(int *));
     
    
    if(p == NULL){
        printf("ERRO");
        exit(0);
    }
     
 
    for(cont = 0;cont < j;++cont){
        
     
        p[cont] = (int *)malloc(i*sizeof(int));
        
     
        if(p == NULL){
            printf("ERRO");
            exit(0);
        }
    }
     
    for(cont = 0;cont < j;++cont){
     
        for(cont1 = 0;cont1 < i;++cont1){
     
            srand(time(NULL));
            p[cont][cont1] = rand()%1000;
        }
    }
     
    
    return p;
}


int somaDiagonal(int **m,int *soma){
	
    int cont,               
        soma_primeira = 0,  
        d = 0;              
	
    for(cont = 0;cont < DIM;cont++){
        
        
        soma_primeira += m[cont][cont];
    }

    for(cont = DIM - 1;cont >= 0;--cont){
     
        *soma += m[d][cont];
     
        d++;
    }

    return soma_primeira;
}



int main(){

	int **matriz,     
        soma1 = 0,    
        soma2 = 0,    
        j,            
        i;	          
	
    
    
    matriz = ler_matriz_int(DIM,DIM);
	
     
	soma1 = somaDiagonal(matriz,&soma2); 
    printf("\nMatriz gerada:\n\n");
    for(j = 0;j < DIM;++j){
        for(i = 0;i < DIM;i++){
            printf("%d ", matriz[j][i]);
        }
        printf("\n");
    }

    printf("\nSoma da primeira diagonal: %d\n", soma1);
    printf("Soma da segunda diagonal: %d\n\n", soma2);
	
	return 0;
}


