/*3. Fa¸ca uma fun¸c˜ao que recebe, por parˆametro, uma matriz A(7,6) e retorna a soma dos elementos
da linha 5 e da coluna 3.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define DIM1 7
#define DIM2 6


int** ler_matriz_int(int j,int i){
 
    int **p,    
        cont,   
        cont1;  
 
    
    p = (int **)malloc(j*sizeof(int *));
     
    
    if(p == NULL){
        printf("ERRO");
        exit(0);
    }
     
 
    for(cont = 0;cont < j;++cont){
        
        
        p[cont] = (int *)malloc(i*sizeof(int));
        
        
        if(p == NULL){
            printf("ERRO");
            exit(0);
        }
    }
     
    for(cont = 0;cont < j;++cont){
     
        for(cont1 = 0;cont1 < i;++cont1){
     
            srand(time(NULL));
            p[cont][cont1] = rand()%1000;
        }
    }
     
    
    return p;
}


int somaLinhaEColuna(int **m,int *s){

	int res = 0, 
        cont;    

    
	for(cont = 0;cont < DIM2;++cont){
		res += m[5][cont]; 
	}

    
	for(cont = 0;cont < DIM1;++cont){
		*s += m[cont][3]; 
	}
	return res;
}

int main(){
	
	int **matriz,  
        soma1 = 0, 
        soma2 = 0, 
        j,         
        i;         

    
	matriz = ler_matriz_int(DIM1,DIM2);

	soma1 = somaLinhaEColuna(matriz,&soma2);

    printf("\nMatriz gerada:\n\n");
	for(j = 0;j < DIM1;++j){
        for(i = 0;i < DIM2;i++){
            printf("%d ", matriz[j][i]);
        }
        printf("\n");
    }

    printf("\nA soma dos elementos da linha 5 é: %d\n", soma1);
    printf("A soma dos elementos da coluna 3 é: %d\n\n", soma2);

	return 0;
}
