 #include <stdio.h>


#define DIM 10
#define DIM1 10

typedef struct livro{
	
	char titulo[DIM];
	char autor[DIM];
	char editora[DIM];
	int ano;
	int emprestado;
	int codigo;

}liv;

void continua(){
	printf("Digite qualquer tecla para continuar:\n");
	scanf("%*c");
}

void menu(){

	printf("Digite:\n");
	printf("1 - Para incluir livro\n");
	printf("2 - Para listar livros cadastrados\n");
	printf("3 - Para emprestar livro\n");
	printf("4 - Para devolver livro\n");
	printf("5 - Para listar os livros emprestados\n");
	printf("6 - Para listar o livros disponiveis para emprestimo\n");
	printf("0 - Para sair\n");

}

int incluirLivro(liv p[]){

	int a = 0,pode = 0;
	
	
	for(a = 0;a < DIM1;++a){
		if (p[a].codigo == -1)
		{
			pode++;
		}
		//printf("%d\n", p[a].codigo);	
	}
	if (pode == 0){

		printf("cheio\n");
		return 0;
	}

	for(a = 0;a < DIM1;++a){

		if(p[a].codigo == -1){
			break;
		}
	}
	

	printf("Digite o nome do livro:\n");
	fgets(p[a].titulo,50,stdin);
	__fpurge(stdin);
	
	printf("Digite o nome do autor:\n");
	fgets(p[a].autor,50,stdin);
	__fpurge(stdin);
	
	printf("Digite o nome da editora:\n");
	fgets(p[a].editora,50,stdin);
	__fpurge(stdin);
	
	printf("Digite o ano de publicação:\n");
	scanf("%d",&p[a].ano);
	__fpurge(stdin);
	
	printf("Digite o codigo do livro:\n");
	scanf("%d",&p[a].codigo);
	__fpurge(stdin);

	p[a].emprestado = 1;
	continua();
	system("clear");

}

int listarLivros(liv p[]){

	int cont;

	for(cont = 0;cont < DIM1;++cont){
		if(p[cont].codigo != -1){
			printf("Titulo do livro:\n");
			printf("%s",p[cont].titulo);
			printf("Situaçao do livro:\n");
			printf("%d\n",p[cont].emprestado);
			printf("\n"); 	
		}
	}
	continua();
	system("clear");
}

void emprestaLivro(liv em[]){
	int n,cont;
	printf("Qual o codigo do livro:\n");
	scanf("%d",&n);
	__fpurge(stdin);

	for(cont = 0;cont < DIM;++cont){
		if(n==em[cont].codigo){
			printf("Livro emprestado com sucesso.\n");
			em[cont].emprestado=0;
			cont=-1;
			break;
		}
	}

	if (cont != -1){
		printf("O livro não esta disponivel\n");
	}

	continua();
	system("clear");

}

void devolveLivro(liv em[]){

	int n,cont;
	printf("Qual o codigo do livro:\n");
	scanf("%d",&n);
	__fpurge(stdin);

	for(cont = 0;cont < DIM;++cont){
		if(n==em[cont].codigo){
			printf("Livro devolvido com sucesso.\n");
			em[cont].emprestado=1;
			cont=-1;
			break;
		}
	}

	if (cont != -1){
		printf("O livro não esta disponivel\n");
	}

	continua();
	system("clear");

}

void listaDisponivel(liv em[]){

	int cont;

	for(cont = 0;cont < DIM;++cont){
		if(em[cont].emprestado==1){
			printf("Livro disponivel:");
			printf("%s", em[cont].titulo);
			printf("Codigo:");
			printf("%d\n", em[cont].codigo);
		}
	}
	printf("\n");
	continua();
	system("clear");
}

void listaEmprestado(liv em[]){
	int cont;

	for(cont = 0;cont < DIM;++cont){
		//printf("daw %d\n", em[cont].codigo);
		if(em[cont].emprestado==0 && em[cont].codigo != -1){
			printf("Livro emprestado:");
			printf("%s", em[cont].titulo);
			printf("Codigo:");
			printf("%d\n", em[cont].codigo);
		}
	}
	printf("\n");
	continua();
	system("clear");
}


int main(){

	liv vet[DIM1];
	int cont;
	int escolha;

	for(cont = 0;cont < DIM1;++cont){
		vet[cont].codigo = -1;
	}

	
	system("clear");
	do{
	
		menu();
		scanf("%d",&escolha);
		__fpurge(stdin);
		if(escolha == 0)
			break;

		switch(escolha){
			case 1:
				system("clear");
				incluirLivro(vet);
				break;
			case 2:
				system("clear");
				listarLivros(vet);
				break;
			case 3:
				system("clear");
				emprestaLivro(vet);
				break;
			case 4:
				system("clear");
				devolveLivro(vet);
				break;
			case 5:
				system("clear");
				listaEmprestado(vet);
				break;
			case 6:
				system("clear");
				listaDisponivel(vet);
				break;
			case 0:
				system("clear");
				exit(0);
				break;
			default:
				printf("Opção invalida!!!!!!!!\n");
				continua();
				system("clear");
				break;
		}


	}while(1);

	return 0;
}