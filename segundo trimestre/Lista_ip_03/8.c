/*8. Fa¸ca uma fun¸c˜ao recursiva para exibir na tela de 1 a 10.*/
#include <stdio.h>

int r(int a){

	if(a < 11){
		printf("%d\n", a);
		return r(a + 1);
	}
	else{
		return 0;
	}

}


int main(){
	
	r(1);
	return 0;
}