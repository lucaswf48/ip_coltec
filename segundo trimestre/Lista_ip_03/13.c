/*13. Fa¸ca uma fun¸c˜ao recursiva que receba um vetor de n´umeros reais e seu tamanho e retorne o menor
elemento deste vetor.*/

#include <stdio.h>
#include <time.h>
#define DIM 10
int r(float vv[],int d,int m){
	if(d - 1>=0){
		return r(vv,d - 1,m > vv[d-1]? vv[d-1] : m);
	}
	else{
		return m;
	}
}

int main(){
	
	int a;
	float v[DIM],rr,m;
	printf("\nVetor gerado:\n");
	for(a = 0;a < DIM;++a){
		srand(time(NULL));
		//scanf("%f",&v[a]);
		v[a]=rand()%100;
		printf("%.3f\n", v[a]);
	}
	m = v[0];
	rr = r(v,DIM,m);
	printf("\nMenor elemento:\n");
	printf("%.3f\n", rr);
	return 0;
}