#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct pontos{
	float x;
	float y;
	float z;
}pot;

void continua(){
	printf("Digite qualquer tecla para continuar:\n");
	scanf("%*c");
	__fpurge(stdin);
}


pot* aloca_estrutura(int *n){
	system("clear");
	pot *p;

	printf("Quantos pontos:\n");
	scanf("%d",n);

	p =(pot*)malloc(*n*sizeof(pot));

	return p;
}

void ler_pontos(pot *p, int n){

	int cont;
	for(cont = 0;cont < n;++cont){
		printf("Ponto %d\n", cont+1);
		scanf("%f %f %f",&p[cont].x,&p[cont].y,&p[cont].z);
		__fpurge(stdin);
	}
	system("clear");
}

void imprimir_pontos(pot *p,int n){

	system("clear");
	int cont;
	for(cont = 0;cont < n;++cont){
		printf("Ponto %d\n", cont+1);
		printf("%.3f, %.3f, %.3f \n", p[cont].x,p[cont].y,p[cont].z);
	}
	continua();
	system("clear");
}

float distancia(pot *p,int *n){

	float dis;
	int a,b;
	system("clear");
	pedir_ponto:
	printf("Quais pontos \n");
	scanf("%d %d",&a,&b);
	a--;
	b--;
	if (a>*n - 1|| b > *n-1){
		printf("Ponto invalido\n");
		goto pedir_ponto;
	}	
		

	__fpurge(stdin);
	
	dis = pow(p[a].x - p[b].x,2)+pow(p[a].y - p[b].y,2)+pow(p[a].z - p[b].z,2);
	dis = pow(dis,0.5);

	return dis;

}

float distancia_consecutiva(pot *p,int n){
	
	int cont,dis;

	for(cont = 0;cont + 1 < n;++cont){
		printf("Distancia dos pontos %d e %d\n", cont+1,cont+2);
		dis = pow(p[cont].x - p[cont+1].x,2)+pow(p[cont].y - p[cont+1].y,2)+pow(p[cont].z - p[cont+1].z,2);
		dis = pow(dis,0.5);
		printf("%d\n", dis);	
	}
	
}

int main(){

	char escolha;
	pot *pon = NULL;
	int n = 0;
	float d;
	system("clear");
	do{
		printf("a - Para ler n pontos\n");
		printf("b - Para imprimir os pontos lidos\n");
		printf("c - Para calcular as distância de 2 pontos(a escolha do usuário)\n");
		printf("d - Para calcular as distância de todos os pontos consecutivos\n");
		printf("q - Para sair do programa\n");
		printf("Escolha:");
		scanf("%c",&escolha);
		__fpurge(stdin);

		switch(escolha){
			case 'a':
				pon = aloca_estrutura(&n);
				ler_pontos(pon,n);
				break;
			case 'b':
				imprimir_pontos(pon,n);			
				break;		
			case 'c':
				if (n > 1)
				{
					d = distancia(pon,&n);
					printf("A distancia é\n");
					printf("%.3f\n", d);
					continua();
					system("clear");
				}
				else{
					printf("Insira mais de um ponto\n");
					continua();
					system("clear");
				}
				
				break;
			case 'd':
				if(n > 1){
					distancia_consecutiva(pon,n);
					continua();
					system("clear");
				}
				else{
					printf("Insira mais de um ponto\n");
					continua();
					system("clear");
				}
				break;
			case 'q':
					printf("chau\n");
					break;
			default:
			        printf("OPÇÃO INVALIDA.\n");
			        continua();
			        system("clear");
			        break;
		}
		

	}while(escolha != 'q');
	system("clear");
	return 0;
}