/*5. Fa¸ca um procedimento que receba duas matrizes de dimens˜oes especificadas pelo usu´ario e retorne
a multiplica¸c˜ao de uma pela outra, quando poss´ıvel.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int** aloca_matriz_int(int j,int i){

	int **p,cont;

	p = (int **)calloc(j,sizeof(int *));
	
	if(p == NULL){
		printf("ERRO");
		exit(0);
	}
	

	for(cont = 0;cont < j;++cont){
	
		p[cont] = (int *)calloc(i,sizeof(int));
	
		if(p == NULL){
			printf("ERRO");
			exit(0);
		}
	}
	
	

	return p;
}

void ler(int **p,int j,int i){

	int cont1,cont;
	for(cont = 0;cont < j;++cont){
	
		for(cont1 = 0;cont1 < i;++cont1){
			srand(time(NULL));
			//scanf("%d",&p[cont][cont1]);
			p[cont][cont1] = rand()%101;
		}
	}
}

int main(){

	int linhamatriz1,
		colunamatriz1,
		linhamatriz2,
		colunamatriz2,
		j,
		i,
		r;
	int **matriz1,
		**matriz2,
		**matriz_resultado;
	
	do{

		printf("Digite a quantidade de linhas da primeira matriz: ");
		scanf("%d",&linhamatriz1);
		printf("Digite a quantidade de colunas da primeira matriz: ");
		scanf("%d",&colunamatriz1);

		printf("Digite a quantidade de linhas da segunda matriz:");
		scanf("%d",&linhamatriz2);
		printf("Digite a quantidade de colunas da segunda matriz:");
		scanf("%d",&colunamatriz2);

		printf("\n");
	}while(linhamatriz2 != colunamatriz1);

	matriz1 = aloca_matriz_int(linhamatriz1,colunamatriz1);
	matriz2 = aloca_matriz_int(linhamatriz2,colunamatriz2);
	matriz_resultado = aloca_matriz_int(linhamatriz1,colunamatriz2);

	ler(matriz1,linhamatriz1,colunamatriz1);
	ler(matriz2,linhamatriz2,colunamatriz2);

	for(i=0;i<linhamatriz1;i++){	
		for(j=0;j<colunamatriz2;j++){
			for(r=0;r<colunamatriz1;r++){
				matriz_resultado[i][j]+=(matriz1[i][r]*matriz2[r][j]);
			}
		}
		printf("\n");
	}
	
	printf("Primeira matriz gerada:\n");
	for(j = 0;j < linhamatriz1;++j){
		for(i = 0;i < colunamatriz1;++i){
			printf("%d ", matriz1[j][i]);
		}
		printf("\n");
	}

	printf("\n");

	printf("Segunda matriz gerada:\n");
	for(j = 0;j < linhamatriz2;++j){
		for(i = 0;i < colunamatriz2;++i){
			printf("%d ", matriz2[j][i]);
		}
		printf("\n");
	}

	printf("\n");

	printf("Matriz gerada:\n");
	for(j = 0;j < linhamatriz1;++j){
		for(i = 0;i < colunamatriz2;++i){
			printf("%d ", matriz_resultado[j][i]);
		}
		printf("\n");
	}
	
	return 0;
}
