#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <termios.h> //Biblioteca para esconder as letras durante a digitação da senha
#include <unistd.h> //Biblioteca para esconder as letras durante a digitação da senha
#include "utilidades.h"
#include "gerente.h"
#include "cliente.h"
#include "getch.h"
int Dia_Hoje, Mes_Hoje, Ano_Hoje, Dia_Arquivo, Mes_Arquivo, Ano_Arquivo;
char Dia_Hoje_Char[2], Mes_Hoje_Char[2], Ano_Hoje_Char[5];
//Função Escrever_Dados: Recebe as informações do usuario, criptografa essas informaçẽos e escreve em seu arquivo.
void Escrever_Dados(char Login[], char Nome[], int Dinheiro, int Transferencia, int Dia, int Mes, int Ano) {
    FILE *Arquivo;
    char Nome_do_Arquivo[9];
    strcpy(Nome_do_Arquivo, Login);
    strcat(Nome_do_Arquivo, ".info");
    Arquivo=fopen(Nome_do_Arquivo, "w");
    char Dados[140], Dados_Codificados[280], Dinheiro_Char[15], Transferencia_Char[15], Dia_Char[3], Mes_Char[3], Ano_Char[4];
    int i, j;
    int aux;
    strcpy(Dados, Nome);
    Itoa(Dinheiro, Dinheiro_Char);
    strcat(Dados, Dinheiro_Char);
    strcat(Dados, ";");
    Itoa(Transferencia, Transferencia_Char);
    strcat(Dados, Transferencia_Char);
    strcat(Dados, ";");
    Itoc(Dia, Dia_Char);
    strcat(Dados, Dia_Char);
    Itoc(Mes, Mes_Char);
    strcat(Dados, Mes_Char);
    Itoa(Ano, Ano_Char);
    strcat(Dados, Ano_Char);
    aux=strlen(Dados);
    for(i=0; i<aux; i++) {
        Dados_Codificados[i]=Dados[i]/10+'0';
        Dados_Codificados[i+aux]=Dados[i]%10+'0';
    }
    for(i=0; i<aux*2; i++) {
        fprintf(Arquivo, "%c", Dados_Codificados[i]);
    }
    fprintf(Arquivo, "\n");
    fclose(Arquivo);
    //printf("Debug: Dados Escritos: %s %s %s %d %d %d %d %d\n", Dados, Login, Nome, Dinheiro, Transferencia, Dia, Mes, Ano);
}
//Função Ler_Dados: Recebe o login de um usuário, procura o arquivo deste usuário, descriptografa o arquivo e retorna todas as informações contidas nele.
void Ler_Dados(char Login[], char Nome[], int *Dinheiro, int *Transferencia, int *Dia, int *Mes, int *Ano) {
    FILE *Arquivo;
    char Nome_do_Arquivo[9], Dados[280], Dados_Decodificados[140];
    int Data=0;
    int i, j, k, l;
    int aux;
    *Dinheiro=0;
    *Transferencia=0;
    strcpy(Nome_do_Arquivo, Login);
    strcat(Nome_do_Arquivo, ".info");
    Arquivo=fopen(Nome_do_Arquivo, "r");
    for(i=0; i<280; i++) {
        fscanf(Arquivo, "%c", &Dados[i]);
        if(Dados[i]=='\n') {
            Dados[i]='\0';
            break;
        }
    }
    aux=strlen(Dados)/2;
    for(i=0; i<140; i++) {
        Dados_Decodificados[i]=0;
    }
    for(i=1; i>=0; i--) {
        for(j=0; j<aux; j++) {
            Dados_Decodificados[j]=Dados_Decodificados[j]+(i*9+1)*Atoi(Dados[j+aux-(i*aux)]);
        }
    }
    Dados_Decodificados[j]='\0';
    j=0;
    k=0;
    for(i=0; i<strlen(Dados_Decodificados); i++) {
        if(Dados_Decodificados[i]==';') {
            i++;
            k++;
        }
        if(Dados_Decodificados[i]>='A'||Dados_Decodificados[i]==' ') {
            Nome[i]=Dados_Decodificados[i];
            j++;
        }
        else if(k==0) {
            *Dinheiro=*Dinheiro*10+Atoi(Dados_Decodificados[i]);
        }
        else if(k==1) {
            *Transferencia=*Transferencia*10+Atoi(Dados_Decodificados[i]);
        }
        else if(k==2) {
            Data=Data*10+Atoi(Dados_Decodificados[i]);
        }
    }
    Nome[j]='\0';
    *Dia=Data/1000000;
    *Mes=Data/10000-*Dia*100;
    *Ano=Data-*Dia*1000000-*Mes*10000;
    fclose(Arquivo);
    //printf("Debug: Dados Lidos: %s %s %s %d %d %d %d %d\n", Dados_Decodificados, Login, Nome, *Dinheiro, *Transferencia, *Dia, *Mes, *Ano);
}
//Função Atualizar_Trasnferencia: Confere se o dia atual é o mesmo guardado no arquivo data.data, se não corresponderem, a função vai acessar o arquivo login.infox e descritografa-lo para ter acesso à todos os arquivos de usuários, então ela le as informações de cada usuário e reescreve, porém, com todos o limites de transferencia resetados para 600.
void Atualizar_Transferencia() {
    FILE *Arquivo, *Arquivo2;
    char String[140], String_Codificada[280], Conta_Cliente[6], Arquivo_da_Conta_Cliente[9], Nome[100];
    int Dia_Arquivo, Mes_Arquivo, Ano_Arquivo, Dinheiro, Transferencia, Dia, Mes, Ano;
    int i, j;
    int aux;
    Arquivo=fopen("data.data", "r");
    fscanf(Arquivo, "%d %d %d", &Dia_Arquivo, &Mes_Arquivo, &Ano_Arquivo);
    fclose(Arquivo);
    if(Dia_Hoje!=Dia_Arquivo||Mes_Hoje!=Mes_Arquivo||Ano_Hoje!=Ano_Arquivo) {
        Arquivo=fopen("data.data", "w");
        fprintf(Arquivo, "%d %d %d\n", Dia_Hoje, Mes_Hoje, Ano_Hoje);
        fclose(Arquivo);
        Arquivo=fopen("login.infox", "r");
        while(!feof(Arquivo)) {
            __fpurge(stdin);
            fscanf(Arquivo, "%s\n", String_Codificada);
            aux=strlen(String_Codificada)/2;
            for(i=0; i<26; i++) {
                String[i]=0;
            }
            for(i=1; i>=0; i--) {
                for(j=0; j<aux; j++) {
                    String[j]=String[j]+(i*9+1)*Atoi(String_Codificada[j+aux-(i*aux)]);
                }
            }
            for(i=0; i<6; i++) {
                if(String[i]<='9') {
                    Conta_Cliente[i]=String[i];
                }
                else {
                    break;
                }
            }
            Conta_Cliente[i]='\0';
            strcpy(Arquivo_da_Conta_Cliente, Conta_Cliente);
            strcat(Arquivo_da_Conta_Cliente, ".info");
            if(Arquivo2=fopen(Arquivo_da_Conta_Cliente, "r")) {
                fclose(Arquivo2);
                if(strlen(Conta_Cliente)==4) {
                    Ler_Dados(Conta_Cliente, Nome, &Dinheiro, &Transferencia, &Dia, &Mes, &Ano);
                    Escrever_Dados(Conta_Cliente, Nome, Dinheiro, 600, Dia, Mes, Ano);
                }
            }
        }
    }
}
//Função Atualizar_Data: Carrega a data atual do sistema e chama a função Atualizar_Transferencia, para que esta atualize os limites de transferencia caso seja necessário.
void Atualizar_Data() {
    time_t currentTime;
    struct tm *timeinfo;
    char stringTime[51];

    currentTime= time(NULL);
    timeinfo= localtime(&currentTime);


    strftime(stringTime, 51, "%d", timeinfo);
    strcpy(Dia_Hoje_Char, stringTime);
    Dia_Hoje=Atoi(stringTime[0])*10+Atoi(stringTime[1]);

    strftime(stringTime, 51, "%m", timeinfo);
    strcpy(Mes_Hoje_Char, stringTime);
    Mes_Hoje=Atoi(stringTime[0])*10+Atoi(stringTime[1]);

    strftime(stringTime, 51, "20%y", timeinfo);
    strcpy(Ano_Hoje_Char, stringTime);
    Ano_Hoje_Char[4]='\0';
    Ano_Hoje=Atoi(stringTime[0])*1000+Atoi(stringTime[1])*100+Atoi(stringTime[2])*10+Atoi(stringTime[3]);

    Atualizar_Transferencia();
}
//Função Cliente: Faz o menu para o usuário do tipo cliente, aqui ele tem acesso à todas as opções disponiveis para este tipo de usuário.
void Cliente(char Login[]) {
    char Nome[100], Nome_Destinatario[100], Destinatario[4], Arquivo_do_Destinatario[9], Nome_do_Arquivo[9], Arquivo_Extrato[12], Char;
    int Dinheiro, Transferencia, Dia, Mes, Ano, Opcao, Valor;
    int Dinheiro_Destinatario, Transferencia_Destinatario, Dia_Destinatario, Mes_Destinatario, Ano_Destinatario;
    FILE *Arquivo;
    strcpy(Nome_do_Arquivo, Login);
    strcat(Nome_do_Arquivo, ".info");
    if(Arquivo=fopen(Nome_do_Arquivo, "r")) {
        fclose(Arquivo);
    }
    else {
        return;
    }
    while(1) {
        Dinheiro=0;
        Transferencia=0;
        Dinheiro_Destinatario=0;
        Transferencia_Destinatario=0;
        system("clear");
        Ler_Dados(Login, Nome, &Dinheiro, &Transferencia, &Dia, &Mes, &Ano);
        printf("Bem Vindo %s\n", Nome);
        printf("Escolha uma opção:\n");
        printf("1. Vizualizar informações\n");
        printf("2. Depositar dinheiro\n");
        printf("3. Sacar dinheiro\n");
        printf("4. Transferir dinheiro\n");
        printf("5. Ver extrato\n");
        printf("6. Sair\n");
        __fpurge(stdin);
        scanf("%d", &Opcao);
        if(Opcao==1) {
            Atualizar_Data();
            printf("Saldo: %d\n", Dinheiro);
            printf("Limite de saque/trasferência: %d\n", Transferencia);
            sleep(3);
        }
        if(Opcao==2) {
            printf("Quando dinheiro vai depositar?\n");
            scanf("%d", &Valor);
            if(Valor<=0) {
                printf("Erro01: Valor inválido\n");
            }
            else {
                printf("Operação realizada com sucesso\n");
                Dinheiro+=Valor;
                Escrever_Dados(Login, Nome, Dinheiro, Transferencia, Dia, Mes, Ano);
                strcpy(Arquivo_Extrato, Login);
                strcat(Arquivo_Extrato, ".extrato");
                Arquivo=fopen(Arquivo_Extrato, "a");
                Atualizar_Data();
                fprintf(Arquivo, "%s/%s/%s \t Você depositou %d e ficou com %d no total\n", Dia_Hoje_Char, Mes_Hoje_Char, Ano_Hoje_Char, Valor, Dinheiro);
                fclose(Arquivo);
            }
            sleep(3);
        }
        if(Opcao==3) {
            printf("Quanto dinheiro vai sacar?\n");
            scanf("%d", &Valor);
            if(Valor<=0) {
                printf("Erro01: Valor inválido\n");
            }
            else if(Transferencia<Valor) {
                printf("Erro03: Valor acima do seu limite\n");
            }
            else if(Dinheiro<Valor) {
                printf("Erro05: Você não tem todo esse dinheiro\n");
            }
            else {
                printf("Operação realizada com sucesso\n");
                Dinheiro-=Valor;
                Transferencia-=Valor;
                Escrever_Dados(Login, Nome, Dinheiro, Transferencia, Dia, Mes, Ano);
                strcpy(Arquivo_Extrato, Login);
                strcat(Arquivo_Extrato, ".extrato");
                Arquivo=fopen(Arquivo_Extrato, "a");
                Atualizar_Data();
                fprintf(Arquivo, "%s/%s/%s \t Você sacou %d e ficou com %d no total\n", Dia_Hoje_Char, Mes_Hoje_Char, Ano_Hoje_Char, Valor, Dinheiro);
                fclose(Arquivo);
            }
            sleep(3);
        }
        if(Opcao==4) {
            printf("Quanto dinheiro vai transferir?\n");
            scanf("%d", &Valor);
            if(Valor<=0) {
                printf("Erro01: Valor inválido\n");
            }
            else if(Dinheiro<Valor) {
                printf("Erro05: Você não tem todo esse dinheiro\n");
            }
            else if(Transferencia<Valor) {
                printf("Erro06: Valor acima do seu limite\n");
            }
            else {
                printf("Para quem você vai mandar esse dinheiro?\n");
                scanf("%s", Destinatario);
                if(strlen(Destinatario)!=4) {
                    printf("Erro07: Essa conta não existe\n");
                    sleep(3);
                    continue;
                }
                strcpy(Arquivo_do_Destinatario, Destinatario);
                strcat(Arquivo_do_Destinatario, ".info");
                if(Arquivo=fopen(Arquivo_do_Destinatario, "r")) {
                    fclose(Arquivo);
                    Ler_Dados(Destinatario, Nome_Destinatario, &Dinheiro_Destinatario, &Transferencia_Destinatario, &Dia_Destinatario, &Mes_Destinatario, &Ano_Destinatario);
                    Dinheiro_Destinatario+=Valor;
                    Dinheiro-=Valor;
                    Transferencia-=Valor;
                    Escrever_Dados(Destinatario, Nome_Destinatario, Dinheiro_Destinatario, Transferencia_Destinatario, Dia_Destinatario, Mes_Destinatario, Ano_Destinatario);
                    Escrever_Dados(Login, Nome, Dinheiro, Transferencia, Dia, Mes, Ano);
                    strcpy(Arquivo_Extrato, Login);
                    strcat(Arquivo_Extrato, ".extrato");
                    Arquivo=fopen(Arquivo_Extrato, "a");
                    Atualizar_Data();
                    fprintf(Arquivo, "%s/%s/%s \t Você transferiu %d para %s e ficou com %d no total\n", Dia_Hoje_Char, Mes_Hoje_Char, Ano_Hoje_Char, Valor, Destinatario, Dinheiro);
                    fclose(Arquivo);
                    strcpy(Arquivo_Extrato, Destinatario);
                    strcat(Arquivo_Extrato, ".extrato");
                    Arquivo=fopen(Arquivo_Extrato, "a");
                    Atualizar_Data();
                    fprintf(Arquivo, "%s/%s/%s \t Você recebeu %d de %s e ficou com %d no total\n", Dia_Hoje_Char, Mes_Hoje_Char, Ano_Hoje_Char, Valor, Login, Dinheiro_Destinatario);
                    fclose(Arquivo);
                }
                else {
                    printf("Erro07: Essa conta não existe\n");
                }
            }
            sleep(3);
        }
        if(Opcao==5) {
            strcpy(Arquivo_Extrato, Login);
            strcat(Arquivo_Extrato, ".extrato");
            if(Arquivo=fopen(Arquivo_Extrato, "r")) {
                while(!feof(Arquivo)) {
                    fscanf(Arquivo, "%c", &Char);
                    printf("%c", Char);
                }
            }    
            else {
                printf("Não há nenhuma informação disponível ainda\n");
            }
            sleep(7);
        }
        if(Opcao==6) {
            break;
        }
    }
}