#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <termios.h> //Biblioteca para esconder as letras durante a digitação da senha
#include <unistd.h> //Biblioteca para esconder as letras durante a digitação da senha
#include "utilidades.h"
#include "gerente.h"
#include "cliente.h"
#include "getch.h"
int Dia_Hoje, Mes_Hoje, Ano_Hoje, Dia_Arquivo, Mes_Arquivo, Ano_Arquivo;
char Dia_Hoje_Char[2], Mes_Hoje_Char[2], Ano_Hoje_Char[5];
//Função Soma_Dinheiro: Acha todos os clientes ativos através do arquivo login.infox, acessa os arquivos de cada um destes usuários, lê suas informações e soma seus saldos na variável Dinheiro_Total, então, ela imprime o valor desta variável.
void Soma_Dinheiro() {
    FILE *Arquivo, *Arquivo2;
    Arquivo = fopen("login.infox" , "r");
    char String_Codificada[52],String_Decodificada[26], Nome[100], Login[5];
    char Nome_do_Arquivo[9];
    long int Dinheiro_Total=0;
    int i, j;
    int aux;
    int Dinheiro, Transferencia, Dia, Mes, Ano;

    while(!feof(Arquivo)) {
        fscanf(Arquivo, "%s\n", String_Codificada);

        aux=strlen(String_Codificada)/2;
        for(i=0; i<26; i++) {
            String_Decodificada[i]=0;
        }
        for(i=1; i>=0; i--) {
            for(j=0; j<aux; j++) {
                String_Decodificada[j]=String_Decodificada[j]+(i*9+1)*Atoi(String_Codificada[j+aux-(i*aux)]);
            }
        }
        String_Decodificada[j]='\0';
        if(String_Decodificada[4]>'9') {
            for(i=0; i<4; i++) {
                Login[i]=String_Decodificada[i];
            }
            Login[i]='\0';
            strcpy(Nome_do_Arquivo, Login);
            strcat(Nome_do_Arquivo, ".info");
            if(Arquivo2=fopen(Nome_do_Arquivo, "r")) {
                fclose(Arquivo2);
                Ler_Dados(Login, Nome, &Dinheiro, &Transferencia, &Dia, &Mes, &Ano);
                Dinheiro_Total+=Dinheiro;
            }
        }

    }
    printf("O saldo bancário total é: %ld\n", Dinheiro_Total);
}
//Função Ordem_Alfabetica: Acha todos os clientes ativos através do arquivo login.infox, acessa os arquivos de cada um destes usuários, lê suas informações e armazena cada um dos Nomes destes usuários na matriz Lista_de_Nomes, assim como os Logins na matriz Lista_de_Logins, então, a função faz um bubble sort com os nomes para organizá-los em ordem alfabética, os logins acompanham os Nomes de seus usuários respectivamente.
void Ordem_Alfabetica() {
    FILE *Arquivo, *Arquivo2;
    Arquivo = fopen("login.infox" , "r");
    char String_Codificada[52],String_Decodificada[26], Lista_de_Nomes[9000][100],Lista_de_Logins[9000][5];
    char Nome_do_Arquivo[9];
    int i, j, k=0;
    int aux;
    char Nome[100];
    int Dinheiro, Transferencia, Dia, Mes, Ano;

    while(!feof(Arquivo)) {
        fscanf(Arquivo, "%s\n", String_Codificada);

        aux=strlen(String_Codificada)/2;
        for(i=0; i<26; i++) {
            String_Decodificada[i]=0;
        }
        for(i=1; i>=0; i--) {
            for(j=0; j<aux; j++) {
                String_Decodificada[j]=String_Decodificada[j]+(i*9+1)*Atoi(String_Codificada[j+aux-(i*aux)]);
            }
        }
        String_Decodificada[j]='\0';
        if(String_Decodificada[4]>'9') {
            for(i=0; i<4; i++) {
                Lista_de_Logins[k][i]=String_Decodificada[i];
            }
            Lista_de_Logins[k][i]='\0';
            strcpy(Nome_do_Arquivo, Lista_de_Logins[k]);
            strcat(Nome_do_Arquivo, ".info");
            if(Arquivo2=fopen(Nome_do_Arquivo, "r")) {
                fclose(Arquivo2);
                Ler_Dados(Lista_de_Logins[k], Lista_de_Nomes[k], &Dinheiro, &Transferencia, &Dia, &Mes, &Ano);
                k++;
            }
        }
    }
    for(i=0; i<k; i++) {
        for(j=0; j<k; j++) {
            if(strcmp(Lista_de_Nomes[i],Lista_de_Nomes[j])<0) {
                char Nome_Temporario[100];
                strcpy(Nome_Temporario,Lista_de_Nomes[j]);
                strcpy(Lista_de_Nomes[j],Lista_de_Nomes[i]);
                strcpy(Lista_de_Nomes[i],Nome_Temporario);
                char Login_Temporario[4];
                strcpy(Login_Temporario,Lista_de_Logins[j]);
                strcpy(Lista_de_Logins[j],Lista_de_Logins[i]);
                strcpy(Lista_de_Logins[i],Login_Temporario);
            }
        }
    }    
	for(i=0; i<k; i++) {
        printf("%s \t %s\n",Lista_de_Logins[i],Lista_de_Nomes[i]);
	}
}
//Função Achou: Recebe um login e uma linha do arquivo login.infox, descriptografa essa linha e responde 1 se o login criptografado na linha é o mesmo que o login recebido, caso contrário responde 0.
int Achou(char Login[], char String[]) {
    char String_Decodificada[27], Login_Decodificado[6], Senha_Decodificada[20];
    int i, j;
    int aux;
    aux=strlen(String)/2;
    for(i=0; i<26; i++) {
        String_Decodificada[i]=0;
    }
    for(i=1; i>=0; i--) {
        for(j=0; j<aux; j++) {
            String_Decodificada[j]=String_Decodificada[j]+(i*9+1)*Atoi(String[j+aux-(i*aux)]);
        }
    }
    String_Decodificada[j]='\0';
    j=0;
    for(i=0; i<strlen(String_Decodificada); i++) {
        if(String_Decodificada[i]<'A') {
            Login_Decodificado[i]=String_Decodificada[i];
            j++;
        }
        else {
            Senha_Decodificada[i-j]=String_Decodificada[i];
        }
    }
    Login_Decodificado[j]='\0';
    Senha_Decodificada[i-j]='\0';
    if(!strcmp(Login, Login_Decodificado)) {
        return 1;
    }
    else {
        return 0;
    }
}
//Função Existe: Recebe um login, lê linha por linha do arquivo login.infox e manda o login e cada uma das linhas para a função Achou, se alguma chamada da função Achou responder 1, a função Existe vai responder 1, mas se nenhuma das chamadas responder 1, então a função Existe responde 0.
int Existe(char Login[]) {
    FILE *Arquivo;
    Arquivo=fopen("login.infox", "r");
    char String[280];
    int i;
    while(!feof(Arquivo)) {
        fscanf(Arquivo, "%s", String);
        if(Achou(Login, String)) {
            return 1;
        }
    }
    return 0;
}
//Função Cortesia: Recebe um login do tipo gerente, abre o arquivo deste gerente, lê seus dados e imprime "Bem Vindo {Nome do Gerente}".
void Cortesia(char Login[]) {
    FILE *Arquivo;
    char Nome_do_Arquivo[11], Nome[200], Nome_Decodificado[100];
    int i, j;
    int aux;
    strcpy(Nome_do_Arquivo, Login);
    strcat(Nome_do_Arquivo, ".info");
    Arquivo=fopen(Nome_do_Arquivo, "r");
    for(i=0; i<200; i++) {
        fscanf(Arquivo, "%c", &Nome[i]);
        if(Nome[i]=='\n') {
            Nome[i]='\0';
            break;
        }
    }
    aux=strlen(Nome)/2;
    for(i=0; i<100; i++) {
        Nome_Decodificado[i]=0;
    }
    for(i=1; i>=0; i--) {
        for(j=0; j<aux; j++) {
            Nome_Decodificado[j]=Nome_Decodificado[j]+(i*9+1)*Atoi(Nome[j+aux-(i*aux)]);
        }
    }
    for(i=0; i<aux; i++) {
        if(Nome_Decodificado[i]=='0') {
            Nome_Decodificado[i]='\0';
            break;
        }
    }
    printf("Bem Vindo %s\n", Nome_Decodificado);
    fclose(Arquivo);
}
//Função Gerente: Faz o menu para o usuário do tipo gerente, aqui ele tem acesso à todas as opções disponiveis para este tipo de usuário. Além disso, caso seja a primeira inicialização do programa, esta função trata de criar um novo gerente.
void Gerente(char Login[]) {
    char Nome[100], Conta_Cliente[6], Arquivo_da_Conta_Cliente[9], Nome_do_Arquivo[11], Apagar_Conta[14], Senha[20], String[26], String_Codificada[52], Entrada, Numero[6], Dia_Char[3], Mes_Char[3], Ano_Char[4], Arquivo_Extrato[12];
    int Dinheiro, Transferencia, Dia, Mes, Ano, Opcao, Valor;
    int i, j;
    int aux;
    FILE *Arquivo, *Arquivo2;
    if(!strcmp(Login, "0")) {
        for(i=100000; i<100099; i++) {
            Itoa(i, Numero);
            if(!Existe(Numero)) {
                break;
            }
        }
        printf("Defina uma senha de até 20 dígitos\n(Só permitimos letras, outros serão desconsiderados)\n");
        __fpurge(stdin);
        for(i=0; i<20; i++) {
            Senha[i]=(char)getch();
            if(Senha[i]=='\n') {
                Senha[i]='\0';
                break;
            }
            if(Senha[i]==127) {
                i-=2;
            }
            else if(!(Senha[i]>='A'&&Senha[i]<='Z'||Senha[i]>='a'&&Senha[i]<='z')) {
                i--;
            }
            if(i<-1) {
                i=-1;
            }
        }
        printf("Digite o nome do usuário\n(Sem acentos ou qualquer caractere estranho)\n");
        __fpurge(stdin);
        for(j=0; j<100; j++) {
            scanf("%c", &Entrada);
            if(Entrada=='\n') {
                Nome[j]='\0';
                break;
            }
            Nome[j]=Entrada;
        }
        strcpy(String, Numero);
        strcat(String, Senha);
        Arquivo=fopen("login.infox", "a");
        aux=strlen(String);
        for(j=0; j<aux; j++) {
            String_Codificada[j]=String[j]/10+'0';
            String_Codificada[j+aux]=String[j]%10+'0';
        }
        for(j=0; j<aux*2; j++) {
            fprintf(Arquivo, "%c", String_Codificada[j]);
        }
        fprintf(Arquivo, "\n");
        fclose(Arquivo);
        Atualizar_Data();
        Escrever_Dados(Numero, Nome, 0, 0, Dia_Hoje, Mes_Hoje, Ano_Hoje);
        printf("Seu número é %s\n", Numero);
        sleep(3);
    }
    strcpy(Nome_do_Arquivo, Login);
    strcat(Nome_do_Arquivo, ".info");
    if(Arquivo=fopen(Nome_do_Arquivo, "r")) {
        fclose(Arquivo);
    }
    else {
        return;
    }
    while(1) {
        system("clear");
        Cortesia(Login);
        printf("Escolha Uma Opção:\n");
        printf("1. Listar clientes por ordem alfabética\n");
        printf("2. Listar clientes por ordem de data de cadastro\n");
        printf("3. Mudar limite de transferência\n");
        printf("4. Criar conta\n");
        printf("5. Apagar conta\n");
        printf("6. Saldo total\n");
        printf("7. Sair\n");
        __fpurge(stdin);
        scanf("%d", &Opcao);
        if(Opcao==1) {
            Ordem_Alfabetica();
            sleep(7);
        }
        if(Opcao==2) {
            Arquivo=fopen("login.infox", "r");
            while(!feof(Arquivo)) {
                __fpurge(stdin);
                fscanf(Arquivo, "%s\n", String_Codificada);
                aux=strlen(String_Codificada)/2;
                for(i=0; i<26; i++) {
                    String[i]=0;
                }
                for(i=1; i>=0; i--) {
                    for(j=0; j<aux; j++) {
                        String[j]=String[j]+(i*9+1)*Atoi(String_Codificada[j+aux-(i*aux)]);
                    }
                }
                for(i=0; i<6; i++) {
                    if(String[i]<='9') {
                        Conta_Cliente[i]=String[i];
                    }
                    else {
                        break;
                    }
                }
                Conta_Cliente[i]='\0';
                strcpy(Arquivo_da_Conta_Cliente, Conta_Cliente);
                strcat(Arquivo_da_Conta_Cliente, ".info");
                if(Arquivo2=fopen(Arquivo_da_Conta_Cliente, "r")) {
                    fclose(Arquivo2);
                    if(strlen(Conta_Cliente)==4) {
                        Ler_Dados(Conta_Cliente, Nome, &Dinheiro, &Transferencia, &Dia, &Mes, &Ano);
                        Itoc(Dia, Dia_Char);
                        Itoc(Mes, Mes_Char);
                        Itoa(Ano, Ano_Char);
                        printf("%s/%s/%s \t %s \t %s\n", Dia_Char, Mes_Char, Ano_Char, Conta_Cliente, Nome);
                    }
                }
            }
            fclose(Arquivo);
            sleep(7);
        }
        if(Opcao==3) {
            printf("Qual é o novo valor de transferência?\n");
            scanf("%d", &Valor);
            if(Valor<0) {
                printf("Erro01: Valor inválido\n");
            }
            else {
                printf("Qual conta será alterada?\n");
                scanf("%s", Conta_Cliente);
                if(strlen(Conta_Cliente)!=4) {
                    printf("Erro07: Essa conta não existe\n");
                    sleep(3);
                    continue;
                }
                strcpy(Arquivo_da_Conta_Cliente, Conta_Cliente);
                strcat(Arquivo_da_Conta_Cliente, ".info");
                if(Arquivo=fopen(Arquivo_da_Conta_Cliente, "r")) {
                    fclose(Arquivo);
                    Ler_Dados(Conta_Cliente, Nome, &Dinheiro, &Transferencia, &Dia, &Mes, &Ano);
                    Escrever_Dados(Conta_Cliente, Nome, Dinheiro, Valor, Dia, Mes, Ano);
                    strcpy(Arquivo_Extrato, Conta_Cliente);
                    strcat(Arquivo_Extrato, ".extrato");
                    Arquivo=fopen(Arquivo_Extrato, "a");
                    Atualizar_Data();
                    fprintf(Arquivo, "%s/%s/%s \t Seu limite de transferência foi mudado de %d para %d\n", Dia_Hoje_Char, Mes_Hoje_Char, Ano_Hoje_Char, Transferencia, Valor);
                    printf("Limite de transferência alterado com sucesso\n");
                    fclose(Arquivo);
                }
                else {
                    printf("Erro07: Essa conta não existe\n");
                }
            }
            sleep(3);
        }
        if(Opcao==4) {
            printf("Qual é o tipo da conta?\n");
            printf("1. Cliente\n");
            printf("2. Gerente\n");
            scanf("%d", &Valor);
            if(Valor==1) {
                for(i=1000; i<9999; i++) {
                    Itoa(i, Numero);
                    if(!Existe(Numero)) {
                        break;
                    }
                }
                printf("Defina uma senha de até 20 dígitos\n(Só permitimos letras, outros serão desconsiderados)\n");
                __fpurge(stdin);
                for(i=0; i<20; i++) {
                    Senha[i]=(char)getch();
                    if(Senha[i]=='\n') {
                        Senha[i]='\0';
                        break;
                    }
                    if(Senha[i]==127) {
                        i-=2;
                    }
                    else if(!(Senha[i]>='A'&&Senha[i]<='Z'||Senha[i]>='a'&&Senha[i]<='z')) {
                        i--;
                    }
                    if(i<-1) {
                        i=-1;
                    }
                }
                printf("Digite o nome do usuário\n(Sem acentos ou qualquer caractere estranho)\n");
                __fpurge(stdin);
                for(j=0; j<100; j++) {
                    scanf("%c", &Entrada);
                    if(Entrada=='\n') {
                        Nome[j]='\0';
                        break;
                    }
                    Nome[j]=Entrada;
                }
                strcpy(String, Numero);
                strcat(String, Senha);
                Arquivo=fopen("login.infox", "a");
                aux=strlen(String);
                for(j=0; j<aux; j++) {
                    String_Codificada[j]=String[j]/10+'0';
                    String_Codificada[j+aux]=String[j]%10+'0';
                }
                for(j=0; j<aux*2; j++) {
                    fprintf(Arquivo, "%c", String_Codificada[j]);
                }
                fprintf(Arquivo, "\n");
                fclose(Arquivo);
                Atualizar_Data();
                Escrever_Dados(Numero, Nome, 0, 600, Dia_Hoje, Mes_Hoje, Ano_Hoje);
                printf("Seu numero é %s\n", Numero);
            }
            if(Valor==2) {
                for(i=100000; i<100099; i++) {
                    Itoa(i, Numero);
                    if(!Existe(Numero)) {
                        break;
                    }
                }
                printf("Defina uma senha de até 20 dígitos\n(Só permitimos letras, outros serão desconsiderados)\n");
                __fpurge(stdin);
                for(i=0; i<20; i++) {
                    Senha[i]=(char)getch();
                    if(Senha[i]=='\n') {
                        Senha[i]='\0';
                        break;
                    }
                    if(Senha[i]==127) {
                        i-=2;
                    }
                    else if(!(Senha[i]>='A'&&Senha[i]<='Z'||Senha[i]>='a'&&Senha[i]<='z')) {
                        i--;
                    }
                    if(i<-1) {
                        i=-1;
                    }
                }
                printf("Digite o nome do usuário\n(Sem acentos ou qualquer caractere estranho)\n");
                __fpurge(stdin);
                for(j=0; j<100; j++) {
                    scanf("%c", &Entrada);
                    if(Entrada=='\n') {
                        Nome[j]='\0';
                        break;
                    }
                    Nome[j]=Entrada;
                }
                strcpy(String, Numero);
                strcat(String, Senha);
                Arquivo=fopen("login.infox", "a");
                aux=strlen(String);
                for(j=0; j<aux; j++) {
                    String_Codificada[j]=String[j]/10+'0';
                    String_Codificada[j+aux]=String[j]%10+'0';
                }
                for(j=0; j<aux*2; j++) {
                    fprintf(Arquivo, "%c", String_Codificada[j]);
                }
                fprintf(Arquivo, "\n");
                fclose(Arquivo);
                Atualizar_Data();
                Escrever_Dados(Numero, Nome, 0, 0, Dia_Hoje, Mes_Hoje, Ano_Hoje);
                printf("Seu número é %s\n", Numero);
            }
            sleep(3);
        }
        if(Opcao==5) {
            printf("Qual conta você quer apagar?\n");
            scanf("%s", Conta_Cliente);
            strcpy(Arquivo_da_Conta_Cliente, Conta_Cliente);
            strcat(Arquivo_da_Conta_Cliente, ".info");
            if(Arquivo=fopen(Arquivo_da_Conta_Cliente, "r")) {
                fclose(Arquivo);
                if(!strcmp(Login, Conta_Cliente)) {
                    printf("Erro11: Você não pode se apagar\n");
                }
                else {
                    strcpy(Apagar_Conta, "rm ");
                    strcat(Apagar_Conta, Conta_Cliente);
                    strcat(Apagar_Conta, ".info");
                    system(Apagar_Conta);
                    printf("Conta apagada com sucesso\n");
                }
            }
            else {
                printf("Erro07: Essa conta não existe\n");
            }
            sleep(3);
        }
        if(Opcao==6) {
            Soma_Dinheiro();
            sleep(3);
        }
        if(Opcao==7) {
            break;
        }
        if(!strcmp(Login, "0")) {
            return;
        }
    }
}