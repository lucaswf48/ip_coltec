#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <termios.h> //Biblioteca para esconder as letras durante a digitação da senha
#include <unistd.h> //Biblioteca para esconder as letras durante a digitação da senha
#include "utilidades.h"
#include "gerente.h"
#include "cliente.h"
#include "getch.h"
//Função main: Recebe o login e a senha do usuário que está tentando logar e envia esses dados para a função Log para que esta faça a autenticação, caso seja autenticado, o usuário será redirecionado para seu devido menu.
int main() {
    FILE *Arquivo;
    if(!(Arquivo=fopen("login.infox", "r"))) {
        system("clear");
        system("touch login.infox");
        system("touch data.data");
        Gerente("0");
    }
    char Login[6], Senha[20];
    int i;
    while(1) {
        system("clear");
        Arquivo=fopen("data.txt", "r");
        printf("Caso queira desligar o sistema, basta digitar ""0000"" no campo ""Login""\n");
        printf("Login: ");
        __fpurge(stdin);
        scanf("%s", Login);
        if(!strcmp(Login, "0000")) {
            return 0;
        }
        printf("Senha: ");
        __fpurge(stdin);
        for(i=0; i<20; i++) {
            Senha[i]=(char)getch();
            if(Senha[i]=='\n') {
                Senha[i]='\0';
                break;
            }
            if(Senha[i]==127) {
                i-=2;
            }
            else if(!(Senha[i]>='A'&&Senha[i]<='Z'||Senha[i]>='a'&&Senha[i]<='z')) {
                i--;
            }
            if(i<-1) {
                i=-1;
            }
        }
        if(Log(Login, Senha)) {
            if(strlen(Login)==4) {
                Cliente(Login);
            }
            if(strlen(Login)==6) {
                Gerente(Login);
            }
        }
    }
}