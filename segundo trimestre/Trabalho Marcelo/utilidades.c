#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <termios.h> //Biblioteca para esconder as letras durante a digitação da senha
#include <unistd.h> //Biblioteca para esconder as letras durante a digitação da senha
#include "utilidades.h"
#include "gerente.h"
#include "cliente.h"
#include "getch.h"
int Dia_Hoje, Mes_Hoje, Ano_Hoje, Dia_Arquivo, Mes_Arquivo, Ano_Arquivo;
char Dia_Hoje_Char[2], Mes_Hoje_Char[2], Ano_Hoje_Char[5];
//Função Atoi: Essa função faz parte da decodificação de dados. Seu objetivo é transformar os char's entre '0' e '9' em 0 e 9, respectivamente.
int Atoi(char ASCII) {
    return ASCII-'0';
}
//Função Digitos: Recebe um número e responde a quantidade de algarismos desse número.
int Digitos(int Numero) {
    int i, j=0;
    for(i=Numero; i>=1; i/=10) {
        j++;
    }
    return j;
}
//Função Itoa: Recebe um número, transforma ele em string e armazena na string ASCII, a qual também foi é uma entrada da função.
void Itoa(int Numero, char ASCII[]) {
    int Tamanho=Digitos(Numero);
    if(Tamanho==0) {
        Tamanho=1;
    }
    int i;
    ASCII[Tamanho]='\0';
    for(i=Tamanho-1; i>=0; i--) {
        ASCII[i]=Numero%10+'0';
        Numero/=10;
    }
}
//Função Itoc: Recebe um número de até dois dígitos para ser utilizado no calendário. Ou seja, se ele recebe o número 6, ele retorna a string "06". Se ele recebe 16, ele retorna a string "16".
void Itoc(int Numero, char Calendario[]) {
    Calendario[0]=Numero/10+'0';
    Calendario[1]=Numero%10+'0';
    Calendario[2]='\0';
}
//Função Decodificador_de_infox: Recebe um login, uma senha e uma linha do arquivo login.infox. Em seguida, ela descriptografa a linha do arquivo e verifica se todos os dados batem.
int Decodificador_de_infox(char Login[], char Senha[], char String[]) {
    char String_Decodificada[27], Login_Decodificado[6], Senha_Decodificada[20];
    int i, j;
    int aux;
    aux=strlen(String)/2;
    for(i=0; i<26; i++) {
        String_Decodificada[i]=0;
    }
    /*
    for(i=0;i<aux;i++) {
    	String_Decodificada[i]=10*Atoi(String[i]);
    	String_Decodificada[i]=Atoi(String[i+aux]);
    }
    i==1		1*9+1==10
    i==0		0*9+1==1
    */
    for(i=1; i>=0; i--) {
        for(j=0; j<aux; j++) {
            String_Decodificada[j]=String_Decodificada[j]+(i*9+1)*Atoi(String[j+aux-(i*aux)]);
        }
    }
    String_Decodificada[j]='\0';
    j=0;
    for(i=0; i<strlen(String_Decodificada); i++) {
        if(String_Decodificada[i]<'A') {
            Login_Decodificado[i]=String_Decodificada[i];
            j++;
        }
        else {
            Senha_Decodificada[i-j]=String_Decodificada[i];
        }
    }
    Login_Decodificado[j]='\0';
    Senha_Decodificada[i-j]='\0';
    if(!strcmp(Login, Login_Decodificado)&&!strcmp(Senha, Senha_Decodificada)) {
        return 1;
    }
    else {
        return 0;
    }
}
//Função Log: Recebe um login e uma senha. Lê as linhas do arquivo login.infox e com o auxilio da função Decodificador_de_infox autentica o login do usuario.
int Log(char Login[], char Senha[]) {
    FILE *Arquivo;
    char String[53], Lixo;
    int i;
    Arquivo=fopen("login.infox", "r");
    while(1) {
        for(i=0; i<53; i++) {
            fscanf(Arquivo, "%c", &String[i]);
            if(String[i]=='\n') {
                String[i]='\0';
                if(Decodificador_de_infox(Login, Senha, String)) {
                    fclose(Arquivo);
                    return 1;
                }
                else {
                    break;
                }
            }
            if(feof(Arquivo)) {
                fclose(Arquivo);
                return 0;
            }
        }
    }
}