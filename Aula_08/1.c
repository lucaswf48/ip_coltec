/*
1) Fazer um programa para contar o número de espaços em brancos de
uma string
*/
#include <stdio.h>

int contaEspaco(char string[]);

int main(){

	int r;
	const int tamanho = 51;
	char s1[tamanho];

	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	r = contaEspaco(s1);

	printf("O total de espaços em branco é %d.\n", r);

	return 0;
}

int contaEspaco(char string[]){

	int cont, total = 0;

	for (cont = 0; string[cont] != '\0';++cont){
		if (string[cont] == ' ')
		{
			++total;
		}
	}

	return total;
}

