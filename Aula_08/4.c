/*
4) Escrever um programa para ler uma string (com mais de uma palavra)
e faça com que a primeira letra de cada palavra ﬁque em maiúscula. Para
isso, basta subtrair 32 do elemento que deseja alterar para maiúsculo.

*/
#include <stdio.h>

int main(){

	const int tamanho = 51;
	int cont;
	char s1[tamanho];

	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	for (cont = 0; s1[cont] != '\0';++cont){
		if (s1[cont] == ' '){
		if(s1[cont + 1]>=97 && s1[cont + 1]<=122)			
			s1[cont + 1] = s1[cont + 1] - 32; 
		}
	}
	if(s1[0] != 32 && s1[0]>=97 && s1[0]<=122)
		s1[0] = s1[0] - 32;


	printf("A nova frase é\n");
	printf("%s\n", s1);

	return 0;
}