/*
2) Fazer um procedimento para imprimir uma string, recebida como
parâmetro, sem os espaços em branco.

*/
#include <stdio.h>


int main(){

	int r,cont, x;
	const int tamanho = 51;
	char s1[tamanho];

	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	
	for (cont = 0; s1[cont] != '\0';++cont){
		if (s1[cont] == ' '){			
			for (x = cont;s1[x] != '\0';++x){
					s1[x] = s1[x + 1];
				}
		}
	}

	s1[cont + 1] = '\0';

	printf("A nova string é %s\n", s1);

	return 0;
}



