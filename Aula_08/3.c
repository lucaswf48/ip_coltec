/*
3) Fazer um programa para contar o número de vogais numa string
*/

#include <stdio.h>

int main(){

	const int tamanho = 51;
	int cont,total = 0;
	char s1[tamanho];

	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	for(cont = 0;s1[cont] != '\0';++cont){
		
		if (s1[cont] == 'a' || s1[cont] == 'e' || s1[cont] == 'i' || s1[cont] == 'o' || s1[cont] == 'u'||s1[cont] == 'A'||s1[cont] == 'E' || s1[cont] == 'I' || s1[cont] == 'O'|| s1[cont] == 'U')
		{
			total++;
		}
		
	}

	printf("O número de vogais presente na frase é %d\n", total);
	return 0;
}
