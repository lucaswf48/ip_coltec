/*Desenvolva um programa que leia um vetor de números reais,um escalar e imprima o resultado da multiplicação do vetor pelo escalar.*/
#include <stdio.h>


void lerVetorEscalar();
void calEscalar(float val[],float esca);

int main (){

	lerVetorEscalar();
	
	return 0;
}

void lerVetorEscalar(){

	float valores[10],escalar;
	int cont = 0;
	
	do{
		printf("Informe o valor do vetor da posição %d.\n", (cont + 1));
		scanf("%f",&valores[cont]);
		cont++;
	}while (cont <= 9);

	printf("Digite o valor do escalar:\n");
	scanf("%f",&escalar);

	calEscalar(valores,escalar);

	//return 0;
}

void calEscalar(float val[],float esca){

	int a;

	printf("Os novos valores do vetor:");

	for (a = 0;a <= 9; a++){
			val[a] = val[a] * esca;
			printf("%.0f, ", val[a]);
		}

	printf("\n");

	//return 0;
}