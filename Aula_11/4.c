/*4. Adicione ao programa anterior uma função para calcular o desvio
padrão e a variância das médias dos alunos. Grave essa informação
no final do arquivo de saída.*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct aluno{

	char nome[50];
	int n0;
	int n1;
	float media;
	float desvio_de_padrao;
	float variancia;

}aluno;


int main(){

	const int DIM = 50;

	int numero_de_alunos,
		i = 0,	//Contador
		nota1,
		nota2;

	aluno *p;	

	char entrada[] = "entrada.txt",
		 saida[] = "saida.txt",
		  nome_aluno[DIM];

	FILE *arquivo;
	FILE *arquivo2;

	arquivo = fopen(entrada,"w");
	if (arquivo == NULL){
		printf("ERRO\n");
		exit(0);
	}

	printf("Quantos alunos:\n");
	scanf("%d",&numero_de_alunos);

	p = (aluno*)malloc(numero_de_alunos*(sizeof(aluno)));


	do{

		printf("Digite o nome do aluno:");
		scanf("%s",nome_aluno);
		printf("Digite a primeira nota:");
		scanf("%d",&nota1);
		printf("Digite a segunda nota:");
		scanf("%d",&nota2);
		printf("\n");
		fprintf(arquivo, "\n %s %d %d \n", nome_aluno,nota1,nota2);
		i++;

	}while(i < numero_de_alunos);

	fclose(arquivo);

	arquivo = fopen(entrada,"r");
	if (arquivo == NULL){
		printf("ERRO\n");
		exit(0);
	}
	i = 0;
	arquivo2 = fopen(saida,"w");
	if (arquivo2 == NULL){
		printf("ERRO\n");
		exit(0);
	}

	while(getc(arquivo) != EOF){
		
		if(fgetc(arquivo) == EOF)
			break;
		
		fscanf(arquivo,"%s %d %d",p[i].nome,&p[i].n0,&p[i].n1);
		
		p[i].media = (p[i].n0+p[i].n1)/2;
		p[i].desvio_de_padrao = (pow((p[i].n0-p[i].media),2)+pow((p[i].n1-p[i].media),2))/2;
		p[i].variancia = pow(p[i].desvio_de_padrao,0.5);

		fprintf(arquivo2, "%s %.1f %.3f %.3f\n",p[i].nome,p[i].media,p[i].desvio_de_padrao,p[i].variancia);
		
		i++;
		
		if(i == numero_de_alunos)
			break;
	}

	fclose(arquivo);

	return 0;
}