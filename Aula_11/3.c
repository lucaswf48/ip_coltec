
/*3. Faça um programa que leia de um arquivo as informações sobre
o nome e as 2 notas de diversos alunos. Imprima a listagem de
alunos e suas respectivas médias. O programa deve criar o arquivo
de entrada e criar um arquivo de saída com o nome e a média de
cada aluno. Armazene as informações dos alunos em um registro
(struct).*/
#include <stdio.h>
#include <stdlib.h>

typedef struct aluno{

	char nome[50];
	int n0;
	int n1;
	float media;

}aluno;


int main(){

	const int DIM = 50;

	int numero_de_alunos,
		i = 0,	//Contador
		nota1,
		nota2;

	aluno *p;	

	char entrada[] = "entrada.txt",
		 saida[] = "saida.txt",
		  nome_aluno[DIM];

	FILE *arquivo;
	FILE *arquivo2;

	arquivo = fopen(entrada,"w");
	if (arquivo == NULL){
		printf("ERRO\n");
		exit(0);
	}

	printf("Quantos alunos:\n");
	scanf("%d",&numero_de_alunos);

	p = (aluno*)malloc(numero_de_alunos*(sizeof(aluno)));


	do{

		printf("Digite o nome do aluno:");
		scanf("%s",nome_aluno);
		printf("Digite a primeira nota:");
		scanf("%d",&nota1);
		printf("Digite a segunda nota:");
		scanf("%d",&nota2);
		printf("\n");
		fprintf(arquivo, "\n %s %d %d \n", nome_aluno,nota1,nota2);
		i++;

	}while(i < numero_de_alunos);

	fclose(arquivo);

	arquivo = fopen(entrada,"r");
	if (arquivo == NULL){
		printf("ERRO\n");
		exit(0);
	}
	i = 0;
	arquivo2 = fopen(saida,"w");
	if (arquivo2 == NULL){
		printf("ERRO\n");
		exit(0);
	}

	while(getc(arquivo) != EOF){
		
		if(fgetc(arquivo) == EOF)
			break;
		
		fscanf(arquivo,"%s %d %d",p[i].nome,&p[i].n0,&p[i].n1);
		
		p[i].media = (p[i].n0+p[i].n1)/2;

		fprintf(arquivo2, "%s %.1f\n",p[i].nome,p[i].media);
		
		i++;
		
		if(i == numero_de_alunos)
			break;
	}

	fclose(arquivo);

	return 0;
}