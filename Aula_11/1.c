#include <stdio.h>

int main(){
	
	FILE *daw;
	char nome[50],
		 carac;
	int n = 0;

	printf("Digite o nome do arquivo:\n");
	scanf("%s",nome);

	daw = fopen(nome,"r");
	if(daw == NULL){
		printf("ERRO");
		exit(0);
	}

    do{
    	carac = fgetc(daw);
    	if (carac =='\n')
    		n++;
    }while(!feof(daw));

    printf("%d\n", n+1);

	return 0;
}							