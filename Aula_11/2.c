// 2. Faça uma função que retorne o número de caracteres de um
// arquivo. Espaços e quebras de linha não devem ser contados
#include <stdio.h>

int main(){
	
	FILE *daw;
	char nome[50],
		 carac;
	int n = 0;

	printf("Digite o nome do arquivo:\n");
	scanf("%s",nome);

	daw = fopen(nome,"r");
	if(daw == NULL){
		printf("ERRO");
		exit(0);
	}

    do{
    	carac = fgetc(daw);
    	if (carac != '\n' && carac != ' ' && carac != EOF)
    		n++;
    	
    }while(carac != EOF);

    printf("%d\n", n);

	
	return 0;
}