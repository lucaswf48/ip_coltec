/*3. Desenvolver um algoritmo para ler o número do dia da semana e
imprimir o seu respectivo nome por extenso. Considerar o número 1 como
domingo, 2 para segunda etc. Caso o dia da semana não exista (menor do
que 1 ou maior do que 7), exibir a mensagem "Dia da semana inválido".*/

#include <stdio.h>

int main ()

{	
	int dia_semana;

	printf("Digite o dia da semana:\n");
	scanf("%d",&dia_semana);

	switch (dia_semana)
	{
		case 1:
				printf("Domingo\n");
				break;
		case 2:
				printf("Segunda\n");
				break;
		case 3:
				printf("Terça\n");
				break;
		case 4:
				printf("Quarta\n");
				break;
		case 5:
				printf("Quinta\n");
				break;
		case 6:
				printf("Sexta\n");
				break;
		case 7:
				printf("Sabado\n");
				break;
		default :
			printf("Dia da semana inválido.\n");
				break;
	}
	return 0;
}