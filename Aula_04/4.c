/*4. Fazer um algoritmo para ler dois números e um dos símbolos das
operações: +, -, * e /. Imprimir o resultado da operação efetuada sobre
os números lidos*/

#include <stdio.h>

int main ()

{
	float num1,num2,resultado;
	char operacao;
	printf("=====================================\n");
	printf("Digite o primeiro número:\n");
	scanf("%f",&num1);

	printf("Digite o segundo número:\n");
	scanf("%f",&num2);

	printf("Qual operação você deseja realisar:\n");
	printf("Para adição digite: +\n");
	printf("Para subtrãção digite: -\n");
	printf("Para multiplicação digite: *\n");
	printf("Para divisão digite: /\n");
	scanf(" %c",&operacao);

	switch (operacao)
	{
		case '+':
				resultado = num1 + num2;
				printf("A soma é:%f.\n", resultado);
				break;
		case '-':
				resultado = num1 - num2; 
				printf("A subtração é: %f.\n", resultado);
				break;

		case '*':
				resultado = num1 * num2;
				printf("A multiplicação é: %f.\n", resultado);
				break;
		case '/':
				if(num2!=0)
				{
				resultado = num1 / num2;
				printf("A divisão é: %f.\n", resultado);
				}
				else
				{
					printf("O denominador não pode ser 0");
				}
				break;			
		default:
				printf("Operador invalido.\n");
				break;
	}

	printf("=====================================\n");

	return 0;
}
