/*6. Tendo como dados de entrada a altura e o sexo de uma pessoa,
construa um programa que calcule seu peso ideal, utilizando as seguintes
fórmulas:
para homens: (72.7*h)-58
para mulheres: (62.1*h)-44.7
Informe também se a pessoa está acima ou abaixo deste peso.*/

#include <stdio.h>

int main ()

{
	int sexo;
	float peso_ideal,altura,peso;

	printf("Qual é o seu sexo:\n");
	printf("Para o sexo feminino digite: 1.\n");
	printf("Para o sexo masculino digite: 2.\n");
	scanf("%d",&sexo);

	printf("Digite sua altura:\n");
	scanf("%f",&altura);
	printf("Digite seu peso:");
	scanf("%f",&peso);

	switch (sexo)
	{
		case 1:
				peso_ideal = (62.1 * altura) - 44.7;
				printf("O seu peso ideal é: %f.\n",peso_ideal);
				if(peso == peso_ideal)
				{
					printf("E seu peso é ideal.");
				}
				else if(peso > peso_ideal)
				{
					printf("Seu peso esta acima do ideal.\n");
				}
				else
				{
					printf("Seu peso esta abaixo do ideal.\n");
				}
				break;
		case 2:
				peso_ideal = (72.7 * altura) - 58;
				printf("O seu peso ideal é: %f.\n",peso_ideal);
				if(peso == peso_ideal)
				{
					printf("E seu peso é ideal.");
				}
				else if(peso > peso_ideal)
				{
					printf("Seu peso esta acima do ideal.\n");
				}
				else
				{
					printf("Seu peso esta abaixo do ideal.\n");
				}
				break;
		default:
				printf("Tente novamente.\n");
				break;
	}

	return 0;
}
