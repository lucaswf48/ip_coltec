/*20. Escrever uma fun¸c˜ao int contabranco(char string[]), que retorna o n´umero de espa¸cos em branco
contidos na string passada como parˆametro.
Ex: n = contabrancos(”a b c”); /* n recebe 3 */
/*n = contabrancos(”abc ”); /* n recebe 2 */
/*n = contabrancos(”abc”); /* n recebe 0 */
#include <stdio.h>

void leiaString();
int contabranco(char frase[]);

int main(){
	leiaString();
	return 0;
}

void leiaString(){

	const int tamanho = 51;
	char s1[tamanho];
	int r;

	//Lê a string
	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	r = contabranco(s1);

	printf("O total de espaços em branco é %d\n", r);
	
}

int contabranco(char frase[]){

	int cont,total = 0;

	for(cont = 0;frase[cont] != '\0'; cont++){

		if(frase[cont] == ' '){
			total++;
		}
	}

	return total;
}