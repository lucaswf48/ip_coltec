/*16. Escrever uma funcao int so positivo(int vetor[], int tamanho), que substitui por zero todos os
numeros negativos do vetor passado por par^ametro, sendo que o numero de elementos do vetor e passado
para a funcao no par^ametro tamanho. A funcao deve retornar o numero de valores que foram substitudos.
Ex: int v[5] = f3, -5, 2, -1, 4g;
tr = so positivo(v,5); printf("%d", tr); /* 2 */
#include <stdio.h>

int lerVetor();
int so_positivo(int *vetor, int tamanho);

int main ()

{
	int tr;

	tr = lerVetor();

	printf("O total de números substituidos foi %d.\n", tr);
	return 0;
}

int lerVetor(){

	int valores[10],cont = 0,in;
	
	do{
		printf("Informe o valor do vetor da posição %d.\n", (cont + 1));
		scanf("%d",&valores[cont]);
		cont++;
	}while (cont <= 9);

	in = so_positivo(valores,10);

	return in;
}

int so_positivo(int *vetor, int tamanho){

	int c,res = 0;

	for (c = 0;c <= tamanho;c++){

			if (vetor[c] < 0)
			{
				vetor[c] = 0;
				res++;
			}
		}

	return res;
}
