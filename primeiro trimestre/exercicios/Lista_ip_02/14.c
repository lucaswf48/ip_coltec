/*14. Implemente uma fun¸c ao que, dado um valor, retorne se esse valor pertence ou n˜ao a um vetor de
inteiros de tamanho 10.*/
#include <stdio.h>

int esta(int *p,int nn);

int main (){

	int v[10],cont = 0,r,n;
	do{
		printf("Digite um valor inteiro:\n");
		scanf("%d",&v[cont]);
        cont++;
	}while(cont <= 9);

	printf("Digite o número que você deseja testar:\n");
	scanf("%d",&n);

	r = esta(v,n);

	switch(r){

		case 1:
			printf("O número esta pertence ao vetor.\n");
			break;
		case 0:
			printf("O número não pertence ao vetor.\n");
			break;
	}

	return 0;
}

int esta(int *p,int nn){

	int c, resposta = 0;
	for(c = 0; c <= 9; c++){

		if (nn == p[c])	{

			resposta = 1;
		}
	}
	return resposta;
}
