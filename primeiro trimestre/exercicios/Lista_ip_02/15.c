/*15. Implemente uma funcao que retorne a media dos valores armazenados em um vetor de inteiros de
tamanho 10.*/
#include <stdio.h>

float media(int *p);

int main (){

	int valor[10],cont = 0;
	float medi;

	do {

		printf("Informe o valor do vetor na posição %d\n", (cont + 1));
		scanf("%d",&valor[cont]);
		cont++;
	} while (cont <= 9);

	medi = media(valor);

	printf("A media dos valores digitados é %.3f.\n", medi);
	return 0;
}

float media(int *p){

	int contador;
	float soma = 0;

	for (contador = 0;contador <= 9; contador++){
			soma = soma + p[contador]; 
		}

	soma = soma / 10;

	return soma;
}


