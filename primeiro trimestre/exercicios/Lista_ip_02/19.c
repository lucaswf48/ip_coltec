/*19. Escrever uma fun¸c˜ao int ultima(char string[], char c) que retorna qual a ´ultima posi¸c˜ao na string
em que aparece o caracter c. Se o caracter n˜ao estiver na string, retornar -1.
Ex: char str[]=”teste”;
int q;
q=ultima(str, ’t’); /* q recebe 3 */
/*q=ultima(str, ’x’); /* q recebe -1 */
#include <stdio.h>

void leiaString();
int ultima(char string[],char carac);

int main(){
    leiaString();
	return 0;
}

void leiaString(){

	const int tamanho = 51;
	char s1[tamanho],caracter;
	int r;

	//Lê a string
	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	printf("Digite o caracter que você deseja procurar na frase\n");
	scanf("%c",&caracter);


	r = ultima(s1,caracter);

	if (r == -1){
		printf("O caracter não aparece na frase.\n");
	}
	else{
		printf("A ultima posição que o caracter aparece é %d.\n", r);
	}

}

int ultima(char string[],char carac){

	int c,posicao,encontrar = 0;

	for (c = 0; string[c] != '\0';c++){

		if (string[c] == carac){

			posicao = c;
			encontrar++;
		}
	}
	if (encontrar == 0){

		return -1;
	}
	else{

		return posicao;
	}
}
