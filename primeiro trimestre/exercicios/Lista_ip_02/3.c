/*3. Faca uma funcao que recebe por parametro o raio de uma esfera e calcula o seu volume (v = 4/3.π.R³).*/
#include <stdio.h>
#include <math.h>

float volume(int raio);

int main()
{
	float raio,v;
	
	printf("\nDigite o valor do raio da esfera:(centimetros)\n");
	scanf("%f",&raio);

	// A variavel 'v' ira receber o valor retornado da função volume.
	v = volume(raio);
	
	printf("O volume da esfera é %.3f centimetros cubicos.\n\n", v);

	return 0;
}

float volume(int raio)
{
	//M_PI é a constante da biblioteca math.c para o valor de π.
	return (4 * M_PI * pow(raio,3))/3;
}