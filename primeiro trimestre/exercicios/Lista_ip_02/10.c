/*10) Implemente uma funcao que retorne o maior elemento de um vetor de inteiros de tamanho 10.*/
#include <stdio.h>

int maiorelemento(int *p);

int main (){

	int v[10], cont = 0,m;
	do{
		printf("Digite um valor inteiro:\n");
		scanf("%d",&v[cont]);
        cont++;
	}while(cont <= 9);

	m = maiorelemento(v);
	printf("O maior número digitado foi  %d\n", m);

	return 0;
}

int maiorelemento(int *p){

	int maior,c = 0;
	while (c <= 9){

		if (c == 0){

			maior = p[c];
		}
		if (p[c] >= maior){

			maior = p[c];
		}
		c++;
	}

	return maior;
}