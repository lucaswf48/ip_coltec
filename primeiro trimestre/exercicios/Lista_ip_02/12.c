/*12. Implemente um procedimento que ordene um vetor de inteiros de tamanho 10.*/
#include <stdio.h>

void ordenado(int *p);

int main (){

	int v[10],i = 0;
	do{
		printf("Digite um valor inteiro:\n");
		scanf("%d",&v[i]);
		i++;
	}while(i <= 9);

	ordenado(v);

	return 0;
}

void ordenado(int *p){

	int a,b,al;

	for (a = 0; a <= 9;a++){

		for(b = 0; b <= 9;b++){

			if (p[a] < p[b]){

				al	= p[a];
				p[a] = p[b];
				p[b] = al;
			}
		}
	}

	for(a = 0;a <= 9;a++){
		
		printf("%d,", p[a]);
	}
}

