/*22. Escrever um procedimento void copiaate(char destino[], char origem[], char parar) que copia para
a string destino os caracteres da string origem que estao antes da primeira ocorrˆencia do caracter parar
ou at´e o final de origem, se parar n˜ao for encontrado.
Ex: char str[80];
copiaate(str, ”testando a funcao”, ’a’); /* str recebe ”test”*/
/*copiaate(str, ”testando a funcao”, ’n’); /* str recebe ”testa”*/
/*copiaate(str, ”testando a funcao”, ’o’); /* str recebe ”testand”*/

#include <stdio.h>

void copiaate(char destino[], char origem[],char parar);
void leiaString();

int main(){
	leiaString();
	return 0;
}

void leiaString(){
	const int tamanho = 51;
	char s1[tamanho],str[tamanho],pare;

	//Lê a string
	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	printf("Até qual caracter você deseja copiar a string:\n");
	scanf("%c",&pare);

	copiaate(str,s1,pare);
}

void copiaate(char destino[], char origem[],char parar){

	int cont,c = 0;

	for(cont = 0; origem[cont] != parar;cont++){
		if(origem[cont] == '\0'){
			printf("O caracter não foi encontrado.\n");
			c++;
			break;
		}
		destino[cont] = origem[cont]; 
	}

	destino[cont] = '\0';
	if(c == 0){
		printf("A nova string é: ");
		printf("%s", destino);
		printf("\n");
	}
	else{
		printf("A string é igual a original: %s\n", origem);
	}
}