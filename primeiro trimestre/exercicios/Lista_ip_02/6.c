/*6. Escrever uma fun¸c˜ao int divisao(int dividendo, int divisor, int *resto), que retorna a divis˜ao inteira
(sem casas decimais) de dividendo por divisor e armazena no par ˆmetro resto, passado por referˆencia, o
resto da divis˜ao.
Ex: int r, d;
d = divisao(5, 2, &r);
printf(”Resultado:%d Resto:%d”, d, r); /* Resultado:2 Resto:1 */

#include <stdio.h>

int divisao(int dividendo, int divisor, int *resto);

int main (){

	int divide,diviso,rest,r;
	printf("Digite o valor do dividendo:\n");
	scanf("%d",&divide);
	printf("Digite o valor do divisor:\n");
	scanf("%d",&diviso);

	r = divisao(divide,diviso,&rest);
	
	printf("O resultado da divisão inteira é %d.\n", r);
	printf("Ja o resto da divisão é %d.\n", rest);
	return 0;
}

int divisao(int dividendo, int divisor, int *resto){
	
	*resto = dividendo % divisor;
	return divisor/divisor;
}