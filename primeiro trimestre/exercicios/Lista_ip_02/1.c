/*1. Faça uma função que receba a idade de uma pessoa em anos, meses e dias e
retorna essa idade expressa em dias.
*/
#include <stdio.h>

int dia(int anos,int meses,int dias);

int main ()
{
	int anos,meses,dias,r;

	printf("Quantos anos de idade você tem?\n");
	scanf("%d",&anos);
	printf("e quantos meses?\n");
	scanf("%d",&meses);
	printf("e quantos dias?\n");
	scanf("%d",&dias);

	r = dia(anos,meses,dias);

	printf("Sua idade em dias é: %d\n",r);
	return 0;
}


int dia(int anos,int meses,int dias)
{
	anos = anos * 365;
	meses = meses * 30;
	dias = dias + meses + anos;

	return dias;
}
