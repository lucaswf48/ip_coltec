/*21. Escrever um procedimento void ninvert(char destino[], char origem[], int num), que copia invertido
para a string destino, os num primeiros caracteres da string origem. Se num for maior que o tamanho da
string, copiar todos os caracteres.
EX: char s1[80] = ”ABCDE”;
char s2[80];
ninvert(s2, s1, 3); /* s2 = ”CBA”*/
/*ninvert(s2, s1, 10); /* s2 = ”EDCBA”*/
#include <stdio.h>

void leiaString();
void ninvert(char destino[], char origem[], int n);
int tamanhoString(char origem[]);

int main(){

	leiaString();
	return 0;
}
void leiaString(){

	const int tamanho = 51;
	char s1[tamanho],s2[tamanho];
	int num;

	//Lê a string
	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	printf("Qual 'n' caracteres da frase você quer copiar para a outra string:\n");
	scanf(" %d",&num);

	ninvert(s2,s1,num);

}

void ninvert(char destino[], char origem[], int n){
	
	n--;
    int al = n,cont,tam;

    tam = tamanhoString(origem);    
    
    if (n + 1 >= tam + 1)
    {
    	al = tam;
    	for (cont = 0;cont <= tam;cont++)
    	{
    		destino[cont] = origem[al];
    		al--;
    	}
    }
    else{

    	for (cont = 0; cont <= al; ++cont)
   		{
    		destino[cont] = origem[n];
    		n--;
   	    }	
    
    destino[al + 1] = '\0';	
    }

    
	
	printf("A string gerada é:%s\n",destino);

}

int tamanhoString(char origem[]){

	int tam;

	for (tam = 0;origem[tam] != '\0';tam++)
	{}

	return tam - 2;
}
