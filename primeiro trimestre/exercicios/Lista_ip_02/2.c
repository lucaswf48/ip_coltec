#include <stdio.h>

char conceito(int media_final);

int main()
{
	float media_final;
	char con;

	printf("Digite da sua nota final:\n");
	scanf("%f",&media_final);
	con = conceito(media_final);
	printf("O seu conceito foi: %c.\n",con);
	return 0;
}

char conceito(int media_final)
{
	if (media_final >= 0 && media_final <= 49)
		return 'D';
	else if (media_final >= 50 && media_final <= 69)
		return 'C';
	else if (media_final >= 70 && media_final <= 89)
		return 'B';
	else if (media_final >= 90 && media_final <= 100)
		return 'A';

}