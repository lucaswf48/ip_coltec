/*8. Escreva uma fun¸c˜ao que receba como parˆametro um valor n inteiro e positivo e que calcule a
seguinte soma: S = 1 + 1/2 + 1/3 + 1/4 + ... + 1/n. A funcao dever´a retornar o valor de S.
*/
#include <stdio.h>

float cal(int n);

int main (){

	int num;
	float r;

	printf("Digite um valor inteiro positivo:");
	scanf("%d",&num);

	r = cal(num);

	printf("O resultado é %f.\n", r);

	return 0;
}

float cal(int n){

	float s = 0;
	int cont = 1;
	while (cont <= n){

		s = s + (1.0/cont);
		cont++;
	}

	return s;
}
