/*7. Escrever uma fun¸c˜ao int somaintervalo(int n1, int n2) que retorna a soma dos n´umeros inteiros que
existem no intervalo fechado entre n1 e n2. A fun¸c˜ao deve funcionar inclusive se o valor de n2 for menor
que n1.
Ex: n=somaintervalo(3, 6); /* n recebe 18 (3 + 4 + 5 + 6) */
/*n=somaintervalo(5,5); /* n recebe 5 (5) */
/*n=somaintervalo(-2,3); /* n recebe 3 (-2 + -1 + 0 + 1 + 2 + 3) */
/*n=somaintervalo(4, 0); /* n recebe 10 (4 + 3 + 2 + 1 + 0) */
#include <stdio.h>

int somaintervalo(int n1,int n2);

int main (){

	int num1,num2,r;

	printf("\nDigite o primeiro número:\n");
	scanf("%d",&num1);
	printf("Digite o segundo número:\n");
	scanf("%d",&num2);

	r = somaintervalo(num1,num2);

    printf("A soma dos valores do intervalo é %d\n\n",r);
	return 0;
}

int somaintervalo(int n1,int n2){

	int soma = 0;
	if (n1 > n2){

		while (n1 >= n2){

			soma = soma + n1;
			n1--;
		}

	}
	else if (n2 > n1){

		while (n2 >= n1){

			soma = soma + n2;
			n2--;
		}

	}
	else{

		soma = n1;
	}

	return soma;
}
