/*9. Escrever uma funcao que receba um vetor com 10 valores e retorne quantos destes valores sao
negativos.*/
#include <stdio.h>

int negativos(int *p);

int main ()
{
	int cont = 0,v[10],total = 0;
	do{
		printf("Digite um valor inteiro:\n");
		scanf("%d",&v[cont]);
        cont++;
	}while(cont <= 9);

	total = negativos(v);

	printf("O total de número menores que 0 digitados é %d\n", total);
	return 0;
}

int negativos(int *p){

	int total = 0,cont = 0;
	while (cont <= 9){

		if (p[cont] < 0){
			
			total++;
		}
		cont++;
	}

	return total;
}

