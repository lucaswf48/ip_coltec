/*18. Escrever um procedimento void stringup(char destino[], char origem[]), que copia todos os caracteres
da string origem para destino, convertendo-os para mai´uscula.
Ex: char s1[20], s2[20]=”aula de c”;
stringup(s1, s2);
printf(”%s”, s1); /* AULA DE C */
#include <stdio.h>

void leiaString();
void stringup(char destino[],char origem[]);

int main(){

	//Chama a função que lê strings.
	leiaString();
	return 0;
}

//Função para ler strings.
void leiaString(){

	const int tamanho = 51;
	char s1[tamanho],s2[tamanho];

	//Lê a string
	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);
	

	stringup(s2,s1);

}

void stringup(char destino[],char origem[]){
	
	int cont;
	for (cont = 0;origem[cont] != '\0';cont++) {

		//Se o caracter for diferente de ' ' e estiver entre 97 e 122
		if(origem[cont] != 32 && origem[cont]>=97 && origem[cont]<=122){
			destino[cont] = origem[cont] - 32;
		}
		else{
			destino[cont] = origem[cont];
		}
		
	}
	
	printf("A frase em caixa alta é:\n");
	printf("%s", destino);
	printf("\n");

}