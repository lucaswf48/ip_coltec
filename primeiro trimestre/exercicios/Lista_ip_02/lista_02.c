/*
1) Faça uma função que receba a idade de uma pessoa em anos, meses e dias
retorna essa idade expressa em dias.
*/

#include <stdio.h>

int dia(int anos,int meses,int dias);

int main (){

	int anos,meses,dias,r;

	printf("Quantos anos de idade você tem?\n");
	scanf("%d",&anos);
	printf("e quantos meses?\n");
	scanf("%d",&meses);
	printf("e quantos dias?\n");
	scanf("%d",&dias);

	r = dia(anos,meses,dias);

	printf("Sua idade em dias é: %d\n",r);
	return 0;
}


int dia(int anos,int meses,int dias){

	anos = anos * 365;
	meses = meses * 30;
	dias = dias + meses + anos;

	return dias;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
2) Fa¸ca uma funcao que recebe a media final de um aluno por parametro e retorna o seu conceito,
conforme a tabela abaixo:
*/

#include <stdio.h>

char conceito(int media_final);

int main(){

	float media_final;
	char con;

	printf("Digite da sua nota final:\n");
	scanf("%f",&media_final);
	con = conceito(media_final);
	printf("O seu conceito foi: %c.\n",con);
	return 0;
}

char conceito(int media_final){

	if (media_final >= 0 && media_final <= 49)
		return 'D';
	else if (media_final >= 50 && media_final <= 69)
		return 'C';
	else if (media_final >= 70 && media_final <= 89)
		return 'B';
	else if (media_final >= 90 && media_final <= 100)
		return 'A';

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
3) Faca uma funcao que recebe por parametro o raio de uma esfera e calcula o seu volume (v = 4/3.π.R³).
*/

#include <stdio.h>
#include <math.h>

float volume(int raio);

int main()
{
	float raio,v;

	printf("\nDigite o valor do raio da esfera:(centimetros)\n");
	scanf("%f",&raio);

	// A variavel 'v' ira receber o valor retornado da função volume.
	v = volume(raio);

	printf("O volume da esfera é %.3f centimetros cubicos.\n\n", v);

	return 0;
}

float volume(int raio){

	//M_PI é a constante da biblioteca math.c para o valor de π.
	return (4 * M_PI * pow(raio,3))/3;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
4) Escrever uma funcao int contaimpar(int n1, int n2) que retorna o numero de inteiros impares que
existem entre n1 e n2 (inclusive ambos, se for o caso). A funcao deve funcionar inclusive se o valor de n2
for menor que n1.
*/

#include <stdio.h>

int contaimpar(int n1, int n2);

int main()
{
	int n1, n2, total_impar;
	printf("\nDigite o primeiro valor: (número inteiro e positivo)\n");
	scanf("%d",&n1);
	printf("\nDigite o segundo valor: (número inteiro e positivo)\n");
	scanf("%d",&n2);
	if (n2 > 0 || n1 > 0){

		total_impar = contaimpar(n1,n2);
		printf("O valor de número impares no intervalor é: %d\n\n", total_impar);
	}
	else{

		printf("Numero invalido!\n\n");
	}

	return 0;
}

int contaimpar(int n1, int n2){

	int cont = 0;
	//Se o primeiro número for o maior
	if (n1 > n2){

		while(n1 >= n2){

			if (n1 % 2 != 0)
				cont++;

			n1--;
		}
	}
	//Se o segundo número for o maior
	else if(n1 < n2){

		while(n2 >= n1){

			if(n2 % 2 != 0)
				cont++;

			n2--;
		}
	}
	//Se os números forem iguais
	if (n1 == n2){

		if (n1 % 2 == 0){

			cont = 0;
		}
		else{

			cont = 1;
		}
	}
	return cont;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
5) Escrever um procedimento void estacao(int dia, int mes), que exibe no vıdeo qual a estacao do ano
da data passada por parametro. Lembrando que a primavera comeca no dia 23 de setembro, o verao em
21 de dezembro, o outono em 21 de marco e o inverno em 21 de junho.
Ex: estacao(25,10); /* 25/10 e primavera.
estacao(29,12); /* 29/12 ´e verao.
*/

#include <stdio.h>

void estacao(int dia, int mes);

int main (){

	int dia, mes;
	printf("Digite a data no formato DD/MM:\n");
	scanf("%d%*c%d",&dia,&mes);
	if (dia < 1 || dia > 31 || mes < 1 || mes > 12){

		printf("Data invalida.\n");
	}
	else{

		estacao(dia,mes);
	}
	return 0;
}

void estacao(int dia, int mes){

	int esta;
	if (mes >= 9){

		if (mes == 9 && dia < 23){

			esta = 1; //Inverno
		}
		else{

			esta = 2; //Primavera.
		}
	}
	if (mes == 12){

		if (dia < 21){

			esta = 2;//Primavera
		}
		else{

			esta = 3;//Verão
		}
	}
	if (mes <= 3){

		if (dia < 21){

			esta = 3;//Verão
		}
		else{

			esta = 4;//Outono
		}
	}
	if (mes > 3 && mes <= 6){

		if (dia < 21){

			esta = 4; //Outono
		}
		else{

			esta = 1;
		}
	}
	if (mes > 6 && mes < 9){

		esta = 1;
	}

	switch (esta){

		case 1:
			printf("Inverno.\n");
			break;
		case 2:
			printf("Primavera.\n");
			break;
        case 3:
            printf("Verão.");
            break;
        case 4:
            printf("Outono.");
            break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
6) Escrever uma funcao int divisao(int dividendo, int divisor, int *resto), que retorna a divisao inteira
(sem casas decimais) de dividendo por divisor e armazena no par ˆmetro resto, passado por referencia, o
resto da divisao.
Ex: int r, d;
d = divisao(5, 2, &r);
printf(”Resultado:%d Resto:%d”, d, r); /* Resultado:2 Resto:1
*/
#include <stdio.h>

int divisao(int dividendo, int divisor, int *resto);

int main (){

	int divide,diviso,rest,r;
	printf("Digite o valor do dividendo:\n");
	scanf("%d",&divide);
	printf("Digite o valor do divisor:\n");
	scanf("%d",&diviso);

	r = divisao(divide,diviso,&rest);

	printf("O resultado da divisão inteira é %d.\n", r);
	printf("Ja o resto da divisão é %d.\n", rest);
	return 0;
}

int divisao(int dividendo, int divisor, int *resto){

	*resto = dividendo % divisor;
	return divisor/divisor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
7) Escrever uma fun¸cao int somaintervalo(int n1, int n2) que retorna a soma dos n´umeros inteiros que
existem no intervalo fechado entre n1 e n2. A fun¸c˜ao deve funcionar inclusive se o valor de n2 for menor
que n1.
Ex: n=somaintervalo(3, 6); /* n recebe 18 (3 + 4 + 5 + 6) */
/*n=somaintervalo(5,5); /* n recebe 5 (5) */
/*n=somaintervalo(-2,3); /* n recebe 3 (-2 + -1 + 0 + 1 + 2 + 3) */
/*n=somaintervalo(4, 0); /* n recebe 10 (4 + 3 + 2 + 1 + 0)
*/

#include <stdio.h>

int somaintervalo(int n1,int n2);

int main (){

	int num1,num2,r;

	printf("\nDigite o primeiro número:\n");
	scanf("%d",&num1);
	printf("Digite o segundo número:\n");
	scanf("%d",&num2);

	r = somaintervalo(num1,num2);

    printf("A soma dos valores do intervalo é %d\n\n",r);
	return 0;
}

int somaintervalo(int n1,int n2){

	int soma = 0;
	if (n1 > n2){

		while (n1 >= n2){

			soma = soma + n1;
			n1--;
		}

	}
	else if (n2 > n1){

		while (n2 >= n1){

			soma = soma + n2;
			n2--;
		}

	}
	else{

		soma = n1;
	}

	return soma;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
8) Escreva uma fun¸c˜ao que receba como parˆametro um valor n inteiro e positivo e que calcule a
seguinte soma: S = 1 + 1/2 + 1/3 + 1/4 + ... + 1/n. A funcao dever´a retornar o valor de S.
*/

#include <stdio.h>

float cal(int n);

int main (){

	int num;
	float r;

	printf("Digite um valor inteiro positivo:");
	scanf("%d",&num);

	r = cal(num);

	printf("O resultado é %f.\n", r);

	return 0;
}

float cal(int n){

	float s = 0;
	int cont = 1;
	while (cont <= n){

		s = s + (1.0/cont);
		cont++;
	}

	return s;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
9) Escrever uma funcao que receba um vetor com 10 valores e retorne quantos destes valores sao
negativos.
*/

#include <stdio.h>

int negativos(int *p);

int main ()
{
	int cont = 0,v[10],total = 0;
	do{
		printf("Digite um valor inteiro:\n");
		scanf("%d",&v[cont]);
        cont++;
	}while(cont <= 9);

	total = negativos(v);

	printf("O total de número menores que 0 digitados é %d\n", total);
	return 0;
}

int negativos(int *p){

	int total = 0,cont = 0;
	while (cont <= 9){

		if (p[cont] < 0){

			total++;
		}
		cont++;
	}

	return total;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
10) Implemente uma funcao que retorne o maior elemento de um vetor de inteiros de tamanho 10.
*/

#include <stdio.h>

int maiorelemento(int *p);

int main (){

	int v[10], cont = 0,m;
	do{
		printf("Digite um valor inteiro:\n");
		scanf("%d",&v[cont]);
        cont++;
	}while(cont <= 9);

	m = maiorelemento(v);
	printf("O maior número digitado foi  %d\n", m);

	return 0;
}

int maiorelemento(int *p){

	int maior,c = 0;
	while (c <= 9){

		if (c == 0){

			maior = p[c];
		}
		if (p[c] >= maior){

			maior = p[c];
		}
		c++;
	}

	return maior;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
11) Implemente uma funcao que retorne o menor elemento de um vetor de inteiros de tamanho 10.
*/

#include <stdio.h>

int menorelemento(int *p);

int main (){

	int v[10], cont = 0,m;
	do{
		printf("Digite um valor inteiro:\n");
		scanf("%d",&v[cont]);
        cont++;
	}while(cont <= 9);

	m = menorelemento(v);
	printf("O menor número digitado foi  %d\n", m);

	return 0;
}

int menorelemento(int *p){

	int menor,c = 0;
	while (c <= 9){

		if (c == 0){

			menor = p[c];
		}
		if (p[c] <= menor){

			menor = p[c];
		}
		c++;
	}

	return menor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
12) Implemente um procedimento que ordene um vetor de inteiros de tamanho 10.
*/

#include <stdio.h>

void ordenado(int *p);

int main (){

	int v[10],i = 0;
	do{
		printf("Digite um valor inteiro:\n");
		scanf("%d",&v[i]);
		i++;
	}while(i <= 9);

	ordenado(v);

	return 0;
}

void ordenado(int *p){

	int a,b,al;

	for (a = 0; a <= 9;a++){

		for(b = 0; b <= 9;b++){

			if (p[a] < p[b]){

				al	= p[a];
				p[a] = p[b];
				p[b] = al;
			}
		}
	}

	for(a = 0;a <= 9;a++){

		printf("%d,", p[a]);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
13) Escrever uma fun¸c˜ao int somavet(int vetor[], int tamanho), que recebe por parˆametro um vetor
de inteiros e o seu tamanho e retorna a soma de seus elementos.
Ex: int lista[4]={100, 20, 10, 5};
int total;
total = somavet(lista, 4); /* total recebe 135
*/

#include <stdio.h>

int somavet(int *n,int tamanho);

int main (){

	int total,tamanho,c = 0;
	printf("Qual é o tamanho do vetor:\n");
	scanf("%d",&tamanho);
	int v[tamanho];
	while (c < tamanho){

    	printf("Digite um valor:\n");
        scanf("%d",&v[c]);
        c++;
    }

	total = somavet(v,tamanho);

	printf("A soma dos elementos é %d.\n",total);
	return 0;
}

int somavet(int *n,int tamanho){

	int a,soma = 0;
	for (a = 0; a < tamanho; a++){

		soma = soma + n[a];
	}
	return soma;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
14) Implemente uma funcao que, dado um valor, retorne se esse valor pertence ou nao a um vetor de
inteiros de tamanho 10.
*/

#include <stdio.h>

int esta(int *p,int nn);

int main (){

	int v[10],cont = 0,r,n;
	do{
		printf("Digite um valor inteiro:\n");
		scanf("%d",&v[cont]);
        cont++;
	}while(cont <= 9);

	printf("Digite o número que você deseja testar:\n");
	scanf("%d",&n);

	r = esta(v,n);

	switch(r){

		case 1:
			printf("O número esta pertence ao vetor.\n");
			break;
		case 0:
			printf("O número não pertence ao vetor.\n");
			break;
	}

	return 0;
}

int esta(int *p,int nn){

	int c, resposta = 0;
	for(c = 0; c <= 9; c++){

		if (nn == p[c])	{

			resposta = 1;
		}
	}
	return resposta;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
15) Implemente uma funcao que retorne a media dos valores armazenados em um vetor de inteiros de
tamanho 10.
*/

#include <stdio.h>

float media(int *p);

int main (){

	int valor[10],cont = 0;
	float medi;

	do {

		printf("Informe o valor do vetor na posição %d\n", (cont + 1));
		scanf("%d",&valor[cont]);
		cont++;
	} while (cont <= 9);

	medi = media(valor);

	printf("A media dos valores digitados é %.3f.\n", medi);
	return 0;
}

float media(int *p){

	int contador;
	float soma = 0;

	for (contador = 0;contador <= 9; contador++){
			soma = soma + p[contador];
		}

	soma = soma / 10;

	return soma;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
16. Escrever uma funcao int so positivo(int vetor[], int tamanho), que substitui por zero todos os
numeros negativos do vetor passado por parametro, sendo que o numero de elementos do vetor e passado
para a funcao no parametro tamanho. A funcao deve retornar o numero de valores que foram substitudos.
Ex: int v[5] = f3, -5, 2, -1, 4g;
tr = so positivo(v,5); printf("%d", tr); /* 2
*/

#include <stdio.h>

int lerVetor();
int so_positivo(int *vetor, int tamanho);

int main ()

{
	int tr;

	tr = lerVetor();

	printf("O total de números substituidos foi %d.\n", tr);
	return 0;
}

int lerVetor(){

	int valores[10],cont = 0,in;

	do{
		printf("Informe o valor do vetor da posição %d.\n", (cont + 1));
		scanf("%d",&valores[cont]);
		cont++;
	}while (cont <= 9);

	in = so_positivo(valores,10);

	return in;
}

int so_positivo(int *vetor, int tamanho){

	int c,res = 0;

	for (c = 0;c <= tamanho;c++){

			if (vetor[c] < 0)
			{
				vetor[c] = 0;
				res++;
			}
		}

	return res;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
17. Escreva uma funcao int contc(char str[], char c) que retorna o numero de vezes que o caracter c
aparece na string str, ambos passados como parametros.
Ex: char texto[]=”EXEMPLO”;
x=contc(texto,’E’); /* x recebe 2 */
/*x=contc(texto,’L’); /* x recebe 1 */
/*x=contc(texto,’W’); /* x recebe 0
*/

#include <stdio.h>

int contCaracter(char string[], char carac);

int main(){
	//Tamanho maximo da string.
	const int TAMANHO = 51;
	int x = 0,cont = 0;
	char texto[TAMANHO],caracter;

	printf("Digite uma frase:\n");

	//fgets:nome da string, número de caracteres amarzenados, entrada padrão.
	fgets(texto,50,stdin);

	//Limpa o buffer.
	//__fpurge(stdin);

	printf("Qual caracter você deseja procurar na frase:\n");

	//Lê o caracter que deseja testar.
	caracter = getchar();

	//Chama a função de teste.
	x = contCaracter(texto, caracter);


	printf("O caracter '%c' aparece na frase %d vez(es).\n", caracter,x);
	return 0;
}

//Função para testar o caracter.
int contCaracter(char string[], char carac){

	int cont,total = 0;

	for (cont = 0;string[cont] != '\0';cont++){

		if (string[cont] == carac)
		{
			total++;
		}
	}

	//Retorna o número de vezes que o caracter aparece.
	return total;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
18. Escrever um procedimento void stringup(char destino[], char origem[]), que copia todos os caracteres
da string origem para destino, convertendo-os para mai´uscula.
Ex: char s1[20], s2[20]=”aula de c”;
stringup(s1, s2);
printf(”%s”, s1); /* AULA DE C
*/

#include <stdio.h>

void leiaString();
void stringup(char destino[],char origem[]);

int main(){

	//Chama a função que lê strings.
	leiaString();
	return 0;
}

//Função para ler strings.
void leiaString(){

	const int tamanho = 51;
	char s1[tamanho],s2[tamanho];

	//Lê a string
	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);


	stringup(s2,s1);

}

void stringup(char destino[],char origem[]){

	int cont;
	for (cont = 0;origem[cont] != '\0';cont++) {

		//Se o caracter for diferente de ' ' e estiver entre 97 e 122
		if(origem[cont] != 32 && origem[cont]>=97 && origem[cont]<=122){
			destino[cont] = origem[cont] - 32;
		}
		else{
			destino[cont] = origem[cont];
		}

	}

	printf("A frase em caixa alta é:\n");
	printf("%s", destino);
	printf("\n");

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
19) Escrever uma funcao int ultima(char string[], char c) que retorna qual a ultima posicao na string
em que aparece o caracter c. Se o caracter nao estiver na string, retornar -1.
Ex: char str[]=”teste”;
int q;
q=ultima(str, ’t’); /* q recebe 3 */
/*q=ultima(str, ’x’); /* q recebe -1
*/

#include <stdio.h>

void leiaString();
int ultima(char string[],char carac);

int main(){
    leiaString();
	return 0;
}

void leiaString(){

	const int tamanho = 51;
	char s1[tamanho],caracter;
	int r;

	//Lê a string
	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	printf("Digite o caracter que você deseja procurar na frase\n");
	scanf("%c",&caracter);


	r = ultima(s1,caracter);

	if (r == -1){
		printf("O caracter não aparece na frase.\n");
	}
	else{
		printf("A ultima posição que o caracter aparece é %d.\n", r);
	}

}

int ultima(char string[],char carac){

	int c,posicao,encontrar = 0;

	for (c = 0; string[c] != '\0';c++){

		if (string[c] == carac){

			posicao = c;
			encontrar++;
		}
	}
	if (encontrar == 0){

		return -1;
	}
	else{

		return posicao;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
20. Escrever uma funcao int contabranco(char string[]), que retorna o n´umero de espacos em branco
contidos na string passada como parˆametro.
Ex: n = contabrancos(”a b c”); /* n recebe 3 */
/*n = contabrancos(”abc ”); /* n recebe 2 */
/*n = contabrancos(”abc”); /* n recebe 0
*/

#include <stdio.h>

void leiaString();
int contabranco(char frase[]);

int main(){
	leiaString();
	return 0;
}

void leiaString(){

	const int tamanho = 51;
	char s1[tamanho];
	int r;

	//Lê a string
	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	r = contabranco(s1);

	printf("O total de espaços em branco é %d\n", r);

}

int contabranco(char frase[]){

	int cont,total = 0;

	for(cont = 0;frase[cont] != '\0'; cont++){

		if(frase[cont] == ' '){
			total++;
		}
	}

	return total;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
21. Escrever um procedimento void ninvert(char destino[], char origem[], int num), que copia invertido
para a string destino, os num primeiros caracteres da string origem. Se num for maior que o tamanho da
string, copiar todos os caracteres.
EX: char s1[80] = ”ABCDE”;
char s2[80];
ninvert(s2, s1, 3); /* s2 = ”CBA”*/
/*ninvert(s2, s1, 10); /* s2 = ”EDCBA”
*/

#include <stdio.h>

void leiaString();
void ninvert(char destino[], char origem[], int n);
int tamanhoString(char origem[]);

int main(){

	leiaString();
	return 0;
}
void leiaString(){

	const int tamanho = 51;
	char s1[tamanho],s2[tamanho];
	int num;

	//Lê a string
	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	printf("Qual 'n' caracteres da frase você quer copiar para a outra string:\n");
	scanf(" %d",&num);

	ninvert(s2,s1,num);

}

void ninvert(char destino[], char origem[], int n){

	n--;
    int al = n,cont,tam;

    tam = tamanhoString(origem);

    if (n + 1 >= tam + 1)
    {
    	al = tam;
    	for (cont = 0;cont <= tam;cont++)
    	{
    		destino[cont] = origem[al];
    		al--;
    	}
    }
    else{

    	for (cont = 0; cont <= al; ++cont)
   		{
    		destino[cont] = origem[n];
    		n--;
   	    }

    destino[al + 1] = '\0';
    }



	printf("A string gerada é:%s\n",destino);

}

int tamanhoString(char origem[]){

	int tam;

	for (tam = 0;origem[tam] != '\0';tam++)
	{}

	return tam - 2;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
Escrever um procedimento void copiaate(char destino[], char origem[], char parar) que copia para
a string destino os caracteres da string origem que est˜ao antes da primeira ocorrˆencia do caracter parar
ou at´e o final de origem, se parar n˜ao for encontrado.
Ex: char str[80];
copiaate(str, ”testando a funcao”, ’a’); /* str recebe ”test”*/
/*copiaate(str, ”testando a funcao”, ’n’); /* str recebe ”testa”*/
/*copiaate(str, ”testando a funcao”, ’o’); /* str recebe ”testand”
*/

#include <stdio.h>

void copiaate(char destino[], char origem[],char parar);
void leiaString();

int main(){
	leiaString();
	return 0;
}

void leiaString(){
	const int tamanho = 51;
	char s1[tamanho],str[tamanho],pare;

	//Lê a string
	printf("Digite uma frase:\n");
	fgets(s1,50,stdin);

	printf("Até qual caracter você deseja copiar a string:\n");
	scanf("%c",&pare);

	copiaate(str,s1,pare);
}

void copiaate(char destino[], char origem[],char parar){

	int cont,c = 0;

	for(cont = 0; origem[cont] != parar;cont++){
		if(origem[cont] == '\0'){
			printf("O caracter não foi encontrado.\n");
			c++;
			break;
		}
		destino[cont] = origem[cont];
	}

	destino[cont] = '\0';
	if(c == 0){
		printf("A nova string é: ");
		printf("%s", destino);
		printf("\n");
	}
	else{
		printf("A string é igual a original: %s\n", origem);
	}
}
