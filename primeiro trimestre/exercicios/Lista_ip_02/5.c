/*5. Escrever um procedimento void estacao(int dia, int mes), que exibe no v´ıdeo qual a esta¸c˜ao do ano
da data passada por parˆametro. Lembrando que a primavera come¸ca no dia 23 de setembro, o ver˜ao em
21 de dezembro, o outono em 21 de mar¸co e o inverno em 21 de junho.
Ex: estacao(25,10); /* 25/10 ´e primavera.
estacao(29,12); /* 29/12 ´e ver˜ao. */
#include <stdio.h>

void estacao(int dia, int mes);

int main (){

	int dia, mes;
	printf("Digite a data no formato DD/MM:\n");
	scanf("%d%*c%d",&dia,&mes);
	if (dia < 1 || dia > 31 || mes < 1 || mes > 12){

		printf("Data invalida.\n");
	}
	else{

		estacao(dia,mes);
	}
	return 0;
}

void estacao(int dia, int mes){

	int esta;
	if (mes >= 9){

		if (mes == 9 && dia < 23){

			esta = 1; //Inverno
		}
		else{

			esta = 2; //Primavera.
		}
	}
	if (mes == 12){

		if (dia < 21){

			esta = 2;//Primavera
		}
		else{

			esta = 3;//Verão
		}
	}
	if (mes <= 3){

		if (dia < 21){

			esta = 3;//Verão
		}
		else{

			esta = 4;//Outono
		}	
	}
	if (mes > 3 && mes <= 6){

		if (dia < 21){

			esta = 4; //Outono
		}
		else{

			esta = 1;
		}
	}
	if (mes > 6 && mes < 9){

		esta = 1;
	}

	switch (esta){
		
		case 1:
			printf("Inverno.\n");
			break;
		case 2:
			printf("Primavera.\n");
			break;
        case 3:
            printf("Verão.");
            break;
        case 4:
            printf("Outono.");
            break;
	}
}
