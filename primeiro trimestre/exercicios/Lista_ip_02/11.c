/*11. Implemente uma funcao que retorne o menor elemento de um vetor de inteiros de tamanho 10.
*/
#include <stdio.h>

int menorelemento(int *p);

int main (){

	int v[10], cont = 0,m;
	do{
		printf("Digite um valor inteiro:\n");
		scanf("%d",&v[cont]);
        cont++;
	}while(cont <= 9);

	m = menorelemento(v);
	printf("O menor número digitado foi  %d\n", m);

	return 0;
}

int menorelemento(int *p){

	int menor,c = 0;
	while (c <= 9){

		if (c == 0){

			menor = p[c];
		}
		if (p[c] <= menor){
			
			menor = p[c];
		}
		c++;
	}

	return menor;
}
