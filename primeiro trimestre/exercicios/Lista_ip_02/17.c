/*17. Escreva uma fun¸c˜ao int contc(char str[], char c) que retorna o n´umero de vezes que o caracter c
aparece na string str, ambos passados como parˆametros.
Ex: char texto[]=”EXEMPLO”;
x=contc(texto,’E’); /* x recebe 2 */
/*x=contc(texto,’L’); /* x recebe 1 */
/*x=contc(texto,’W’); /* x recebe 0 */
#include <stdio.h>

int contCaracter(char string[], char carac);

int main(){
	//Tamanho maximo da string.
	const int TAMANHO = 51;
	int x = 0,cont = 0;
	char texto[TAMANHO],caracter;

	printf("Digite uma frase:\n");

	//fgets:nome da string, número de caracteres amarzenados, entrada padrão.
	fgets(texto,50,stdin);

	//Limpa o buffer.
	//__fpurge(stdin);

	printf("Qual caracter você deseja procurar na frase:\n");

	//Lê o caracter que deseja testar.
	caracter = getchar();

	//Chama a função de teste.
	x = contCaracter(texto, caracter);

	
	printf("O caracter '%c' aparece na frase %d vez(es).\n", caracter,x);
	return 0;
}

//Função para testar o caracter.
int contCaracter(char string[], char carac){
	
	int cont,total = 0;

	for (cont = 0;string[cont] != '\0';cont++){

		if (string[cont] == carac)
		{
			total++;
		}
	}

	//Retorna o número de vezes que o caracter aparece.
	return total;
}

