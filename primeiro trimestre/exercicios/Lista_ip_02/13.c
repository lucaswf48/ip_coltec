/*13. Escrever uma fun¸c˜ao int somavet(int vetor[], int tamanho), que recebe por parˆametro um vetor
de inteiros e o seu tamanho e retorna a soma de seus elementos.
Ex: int lista[4]={100, 20, 10, 5};
int total;
total = somavet(lista, 4); /* total recebe 135 */

#include <stdio.h>

int somavet(int *n,int tamanho);

int main (){

	int total,tamanho,c = 0;
	printf("Qual é o tamanho do vetor:\n");
	scanf("%d",&tamanho);
	int v[tamanho];
	while (c < tamanho){

    	printf("Digite um valor:\n");
        scanf("%d",&v[c]);
        c++;
    }
    
	total = somavet(v,tamanho);

	printf("A soma dos elementos é %d.\n",total);
	return 0;
}

int somavet(int *n,int tamanho){

	int a,soma = 0;
	for (a = 0; a < tamanho; a++){
		
		soma = soma + n[a];
	}
	return soma;
}
