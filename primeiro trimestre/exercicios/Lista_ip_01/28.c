/*28. Escreva um programa que receba dois numeros reais e um codigo de selecao do usuario.
Se o codigo digitado for 1, faca o programa adicionar os dois numeros previamente digitados
e mostrar o resultado; se o codigo de selecao for 2, os numeros devem ser multiplicados; se o
codigo de selecao for 3, o primeiro numero deve ser dividido pelo segundo. Se nenhuma das
opcoes acima for escolhida, mostrar ”Codigo invalido”.*/
#include <stdio.h>

int main ()

{
	float num1,num2,resposta;
	int codigo;

	printf("Digite dois números reais(separados por um espaço):\n");
	scanf("%f %f",&num1,&num2);

	printf("Digite 1: para efetuar uma soma.\n");
	printf("Digite 2: para efetuar uma multiplicação..\n");
	printf("Digite 3: para efetuar uma divisão do primeiro pelo segundo.\n");
	scanf("%d",&codigo);

	switch (codigo)
	{
		case 1:
				resposta = num1 + num2;
				printf("A soma destes dois números é: %f.\n",resposta);
				break;
		case 2:
				resposta = num1 * num2;
				printf("A multiplicação destes dois números é: %f.\n",resposta);
				break;
		case 3:
				resposta = num1 / num2;
				printf("A divisão destes dois números é: %f.\n",resposta);
				break;
		default:
				printf("Codigo invalido.\n");
	}
	return 0;
}