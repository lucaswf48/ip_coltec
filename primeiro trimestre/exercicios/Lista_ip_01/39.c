/*39. Escrever um algoritmo que le 10 valores, um de cada vez, e conte quantos deles estao 
no intervalo [10,20] e quantos deles estao fora do intervalo, escrevendo estas informacoes.*/

#include <stdio.h>

int main ()

{
	int cont = 1,dentro,fora;
	float num1;
	while(cont <= 10)
	{
		printf("Digite um valor:\n");
		scanf("%f",&num1);
		if (num1 >= 10 && num1 <= 20)
		{
			++dentro;
		}
		else
		{
			++fora;
		}
	}

	printf("%d valor(es) dentro da faixa.\n", dentro);
	printf("%d valor(s) fora da faixa.\n", fora);
	return 0;
}