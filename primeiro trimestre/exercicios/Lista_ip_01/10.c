/*10. Calcular o sal´ario l´ıquido do funcion´ario sabendo que este ´e constitu´ıdo pelo sal´ario bruto
mais o valor das horas extras subtraindo 8% de INSS do total. Ser˜ao lidos nesse problema o
sal´ario bruto, o valor das horas extras e o n´umero de horas extras. Apresentar ao final o sal´ario
l´ıquido.*/

/////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#define INSS 0.08

int main ()

{
	int horas_extras;
	float salario_bruto,salario_liquido,valor_hora_extra;

	printf("Qual é o seu salario bruto?\n");	
	scanf("%f",&salario_bruto);

	printf("Qual é o valor da horas extra?\n");
	scanf("%f",&valor_hora_extra);

	printf("Quantas horas extras você trabalhou?\n");
	scanf("%d",&horas_extras);

	salario_liquido = (salario_bruto + (valor_hora_extra * horas_extras))-((salario_bruto + (valor_hora_extra * horas_extras))*INSS);

	printf("O salario liquido é de: %f.\n",salario_liquido);
	return 0;
}