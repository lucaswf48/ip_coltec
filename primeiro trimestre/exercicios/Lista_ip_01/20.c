/*20. Faca a leitura do ano atual e do ano de nascimento de uma pessoa e exibir sua idade.
A seguir, informe se a pessoa e bebe (0 a 3 anos), crianca (4 a 10 anos), adolescente (11 a 18
anos), adulta (19 a 50 anos) ou idosa (51 anos em diante).*/
#include <stdio.h>

int main ()

{
	int ano_atual,ano_nascimento,idade;

	printf("Digite o ano atual:\n");
	scanf("%f",&ano_atual);

	printf("Digite o ano de seu nascimento:\n");
	scanf("%f",&ano_nascimento);

	idade = ano_atual - ano_nascimento;

	if (idade >= 0 && idade <= 3)
	{
		printf("Bebe.\n");
	}
	else if ( idade >= 4 && idade <= 10)
	{
		printf("Criança.\n");
	}
	else if (idade >= 11 && idade < 18)
	{
		printf("Adolescente.\n");
	}
	else if (idade >= 19 && idade <= 50)
	{
		printf("Adulto.\n");
	}
	else
	{
		printf("idoso.\n");		
	}


	return 0;
}