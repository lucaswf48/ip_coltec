/*19. Faca a leitura do salario atual e do tempo de servico de um funcionario. A seguir, calcule
o seu salario reajustado. Funcionarios com ate 1 ano de empresa, receberao aumento de 10%.
Funcionarios com mais de um ano de tempo de servico, receberao aumento de 20%.*/
#include <stdio.h>

int main ()
{
	float salario_atual,tempo_servico,aumento;

	printf("Digite o salario atual:\n");
	scanf("%f",&salario_atual);

	printf("Digite o seu tempo de serviço:\n");
	scanf("%f",&tempo_servico);

	if ( tempo_servico <= 1)
	{
		aumento = salario_atual * 1.1;
		printf("Seu novo salario é: %f\n", aumento);
	}
	else
	{
		aumento = salario_atual * 1.2;
		printf("Seu novo salario é: %f\n", aumento);
	}

	return 0;
}