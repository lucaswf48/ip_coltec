/*15. Efetuar a leitura de uma nota e, se o valor for maior ou igual a 60, imprimir na tela
”APROVADO”, se for menor, imprimir ”REPROVADO”. Testar ainda se o valor lido foi maior
do que 100 ou menor do que zero. Neste caso, imprimir ”NOTA INVALIDA”.*/

#include <stdio.h>

int main ()

{
	float nota;

	printf("Digite sua nota:\n");
	scanf("%f",&nota);

	if(nota > 100 && nota < 0)
	{
		printf("Nota invalida.\n");
	}
	else
	{
		if (nota >= 60)
		{
			printf("APROVADO\n");
		}
		else
		{
			printf("REPROVADO\n");
		}
	}
	return 0;
}