/*14. Efetuar a leitura de uma nota e, se o valor for maior ou igual a 60, imprimir na tela
”APROVADO”, se for menor, imprimir ”REPROVADO”.*/

#include <stdio.h>

int main ()

{
	float nota;

	printf("Digite sua nota:\n");
	scanf("%f",&nota);

	if (nota >= 60)
	{
		printf("APROVADO\n");
	}
	else
	{
		printf("REPROVADO\n");
	}
	return 0;
}