#include <stdio.h>

int main ()

{
	float tempo;

	printf("Qual é o tempo que os fundos foram mantidos:\n");
	scanf("%f",&tempo);

	if (tempo < 1)
	{
		printf("A taxa de juros é de: %f%%. \n", (0.55*100));
	}
	else if ( tempo >= 1 && tempo < 2)
	{
		printf("A taxa de juros é de: %f%%. \n", (0.65*100));
	}
	else if ( tempo >= 2 && tempo < 3)
	{
		printf("A taxa de juros é de: %f%%. \n", (0.75*100));
	}
	else if (tempo >= 3 && tempo < 4)
	{
		printf("A taxa de juros é de: %f%%. \n", (0.85*100));
	}
	else if (tempo >= 4 && tempo < 5)
	{
		printf("A taxa de juros é de: %f%%. \n", (0.9*100));
	}
	else
	{
		printf("A taxa de juros é de: %f%%. \n", (0.95*100));
	}


	return 0;
}