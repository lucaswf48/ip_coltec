/*8. Calcular o aumento que sera dado a um funcionario, obtendo do usuario as seguintes
informacoes : salario atual e a porcentagem de aumento. Apresentar o novo valor do salario e o
valor do aumento.*/
#include <stdio.h>

int main ()

{
	float salario_atual,salario_final,porcentagem_aumento,aumento;
	printf("Digite o seu salario atual:\n");
	scanf("%f",&salario_atual);

	printf("Digite a porcentagem de aumento:\n");
	scanf("%f",&porcentagem_aumento);

	aumento = salario_atual*(porcentagem_aumento/100);
	salario_final = salario_atual + aumento;

	printf("O aumento foi de %.3f reais.\n", aumento);
	printf("O salario final é de: %.3f reais.\n", salario_final);


	return 0;
}