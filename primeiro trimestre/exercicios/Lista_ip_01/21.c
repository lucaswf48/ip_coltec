/*21. Informar o numero do mes do ano e mostrar o nome do mes por extenso. Caso o numero do mes nao exista, exibir a mensagem ”Mˆes invalido”.*/
#include <stdio.h>

int main ()

{
	int mes;

	printf("Digite o número do mês\n");
	scanf("%d",&mes);
	
	switch (mes)
	{
		case 1:
				printf("Janeiro\n");
				break;
		case 2:
				printf("Fevereiro\n");
				break;
		case 3:
				printf("Março\n");
				break;
		case 4:
				printf("Abril\n");
				break;
		case 5:
				printf("Maio\n");
				break;
		case 6:
				printf("Junho\n");
				break;
		case 7:
				printf("julho\n");
				break;
		case 8:
				printf("Agosto\n");
				break;
		case 9:
				printf("Setembro\n");
				break;
		case 10:
				printf("Outubro\n");
				break;
		case 11:
				printf("Novembro\n");
				break;
		case 12:
				printf("Dezembro\n");
				break;
		default:
				printf("Mês invalido.\n");		
	}
	return 0;
}