/*23. Escreva um programa para calcular e mostrar o salario semanal de uma pessoa, de-
terminado pelas condicoes que seguem. Se o numero de horas trabalhadas for inferior a 40, a
pessoa recebe R$15,00 por hora, senao a pessoa recebe R$600,00 mais R$21,00 para cada hora
trabalhada acima de 40 horas. O programa deve pedir o numero de horas trabalhadas como
entrada e deve dar o salario como saıda.*/
#include <stdio.h>

int main ()

{
	int horas,salario;

	printf("Digite quantas horas você trabalhou:\n");
	scanf("%d",&horas);

	if (horas < 40)
	{
		salario = horas * 15.0;
	}
	else
	{
		salario = 600.0 + ((horas - 40) * 21.0);
	}
	return 0;
}