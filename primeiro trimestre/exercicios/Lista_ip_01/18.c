/*18. Ler um numero e informar se ele e positivo, negativo ou neutro (zero).*/
#include <stdio.h>

int main ()

{
	float num1;
	printf("Digite um número:\n");
	scanf("%f",&num1);

	if (num1 < 0)
	{
		printf("O número digitado é menor que 0.\n");
	}
	else if(num1 == 0)
	{
		printf("O número digitado é igual a 0.\n");
	}
	else
	{
		printf("o número digitado é maior do que 0.\n");
	}
	return 0;
}