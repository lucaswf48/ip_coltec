/*32. Escreva um algoritmo que leia 6 valores e encontre o maior e o menor deles. Mostre o
resultado.*/
#include <stdio.h>

int main ()

{
	int contador = 1,cont = 0;
	float valor,compara_maior,compara_menor;
	while (contador <= 6)
	{
		printf("Digite um valor:\n");
		scanf("%f",&valor);

		if (cont == 0)
		{
			compara_maior = valor;
			compara_menor = valor;
			++cont;
		}
		
		if(valor > compara_maior)
		{
			compara_maior = valor;
		}
		if(valor < compara_menor)
		{
			compara_menor = valor;
		}
		contador++;
	}

	printf("Maior número digitador: %f\n", compara_maior);
	printf("Menor número digitador: %f\n", compara_menor);
	return 0;
}