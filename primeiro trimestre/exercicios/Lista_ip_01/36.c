/*36. Escrever um algoritmo que leia uma vari´avel n e calcule a tabuada de 1 at´e n. Mostre a
tabuada na forma:*/
#include <stdio.h>

int main ()

{
	int num,cont=1;
	printf("Digite um número inteiro:\n");
	scanf("%d",&num);

	while(cont <= num)
	{
		printf("%d x %d = %d\n", cont,num,(num * cont));
		++cont;
	}
	return 0;
}