/*17. Ler um numero inteiro e testar se o valor lido termina com 0 (divisıvel por 10). Em caso
positivo, exiba a metade deste numero. Caso contrario, exibir a mensagem ”O numero digitado nao termina com 0”.*/
#include <stdio.h>

int main ()

{
	int num1;

	printf("Digite um número inteiro:\n");
	scanf("%d",&num1);

	if(num1 % 10 == 0)
	{
		printf("A metade do número %d é: %d.\n",num1,(num1/2));
	}
	else
	{
		printf("O número digitado não termina com 0.\n");
	}
	return 0;
}