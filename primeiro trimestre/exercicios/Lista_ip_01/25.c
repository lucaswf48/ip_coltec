/*25. Desenvolva um algoritmo que leia duas notas de um aluno, um trabalho (todos os
valores entre 0 e 10) e sua frequencia, definindo e imprimindo se ele foi aprovado, reprovado ou
se fara prova final. O aluno ser a reprovado se faltou mais de 15 aulas. Sera aprovado se nao for
reprovado por falta e sua media for maior que 6,0. Caso tenha media menor, dever ́a fazer prova
final. O calculo da media deve ser feito com peso 3 para a primeira prova, 5 para a segunda
prova e 2 para o trabalho.*/
#include <stdio.h>

int main ()

{
	float prova1,prova2,trabalho,faltas,media;

	printf("Qual foi a sua nota na primeira prova:\n");
	scanf("%f",&prova1);

	printf("Qual foi a sua nota na segunda prova:\n");
	scanf("%f",&prova2);

	printf("Qual foi a sua nota no trabalho:\n");
	scanf("%f",&trabalho);

	printf("Qual foi o número de faltas.\n");
	scanf("%f",&faltas);

	if (faltas > 15)
	{
		printf("Reprovado.\n");
	}
	else
	{
		media = (prova1 * 3) + (prova2 * 5) + (trabalho * 2)/10;
		if (media > 6.0)
		{
			printf("Aprovado.\n");
		}
		else
		{
			printf("Prova de recuperação.\n");
		}
	}
	return 0;
}