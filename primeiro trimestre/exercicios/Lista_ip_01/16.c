/*16. Ler um numero inteiro e informar se o numero lido e par ou ımpar.*/
#include <stdio.h>

int main ()

{
	int num1;

	printf("Digite um número:\n");
	scanf("%d",&num1);

	if (num1 % 2 == 0)
	{
		printf("Número par.\n");
	}
	else
	{
		printf("Número impar.\n");
	}

	return 0;
}