#include <stdio.h>

int main ()

{
	int nota;
	printf("Digite sua nota:\n");
	scanf("%d",&nota);

	if (nota == 10 || nota == 9)
	{
		printf("Conceito: A.\n");
	}
	else if (nota == 8 || nota == 7)
	{
		printf("Conceito: B.\n");
	}
	else if (nota == 6 || nota == 5)
	{
		printf("Conceito: C.\n");
	}
	else if (nota <= 4 && nota >= 0)
	{
		printf("Conceito: D.\n");
	}
	else
	{
		printf("Nota invalida.\n");
	}
	return 0;
}