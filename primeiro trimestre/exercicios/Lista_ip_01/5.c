//5. Efetuar a soma dos numeros 5 e 10 e imprimir o resultado.
#include <stdio.h>

int main ()

{

	printf("A soma de 10 e 5 é: %d\n",(10+5));
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

//6. Efetuar a soma de tres numeros digitados pelo usuario e imprimir o resultado.
#include <stdio.h>

int main ()

{
	float num1,num2,num3;

	printf("Digitados três números:(separados por espaços)\n");
	scanf("%f %f %f",&num3,&num2,&num1);

	printf("A soma dos números digitados é: %.3f\n", (num3 + num2 +num1));

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

//7. Efetuar a multiplicacao de dois numeros digitados pelo usuario e imprimir o resultado.
#include <stdio.h>

int main ()

{
	float num1,num2;

	printf("Digite dois números, separados por espaço:\n");
	scanf("%f %f",&num1,&num2);

	printf("O multiplicação dos números digitados é: %.3f\n", (num2*num1));
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*8. Calcular o aumento que sera dado a um funcionario, obtendo do usuario as seguintes
informacoes : salario atual e a porcentagem de aumento. Apresentar o novo valor do salario e o
valor do aumento.*/
#include <stdio.h>

int main ()

{
	float salario_atual,salario_final,porcentagem_aumento,aumento;
	printf("Digite o seu salario atual:\n");
	scanf("%f",&salario_atual);

	printf("Digite a porcentagem de aumento:\n");
	scanf("%f",&porcentagem_aumento);

	aumento = salario_atual*(porcentagem_aumento/100);
	salario_final = salario_atual + aumento;

	printf("O aumento foi de %.3f reais.\n", aumento);
	printf("O salario final é de: %.3f reais.\n", salario_final);


	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*9. Converter uma quantidade de horas digitadas pelo usuario em minutos. Informe o resul-
tado em minutos.*/
#include <stdio.h>

int main ()

{
	float horas,minutos;
	printf("Digite um valor em horas:\n");
	scanf("%f",&horas);

	minutos = horas * 60;

	printf("%f horas são %f minutos.\n", horas,minutos);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////
/*10. Calcular o sal´ario l´ıquido do funcion´ario sabendo que este ´e constitu´ıdo pelo sal´ario bruto
mais o valor das horas extras subtraindo 8% de INSS do total. Ser˜ao lidos nesse problema o
sal´ario bruto, o valor das horas extras e o n´umero de horas extras. Apresentar ao final o sal´ario
l´ıquido.*/

#include <stdio.h>
#define INSS 0.08

int main ()

{
	int horas_extras;
	float salario_bruto,salario_liquido,valor_hora_extra;

	printf("Qual é o seu salario bruto?\n");	
	scanf("%f",&salario_bruto);

	printf("Qual é o valor da horas extra?\n");
	scanf("%f",&valor_hora_extra);

	printf("Quantas horas extras você trabalhou?\n");
	scanf("%d",&horas_extras);

	salario_liquido = (salario_bruto + (valor_hora_extra * horas_extras))-((salario_bruto + (valor_hora_extra * horas_extras))*INSS);

	printf("O salario liquido é de: %f.\n",salario_liquido);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*11. Efetuar a leitura do n´umero de quilowatts consumido e calcular o valor a ser pago de
energia el´etrica, sabendo-se que o valor a pagar por quilowatt ´e de 0,12. Apresentar o valor total
a ser pago pelo usu´ario acrescido de 18% de ICMS.*/

#include <stdio.h>
#define ICMS 0.18
#define watt 0.12

int main ()

{
	float quilo,valor;
	
	printf("Quantos quilowatts foram consomidos?\n");
	scanf("%f",&quilo);

	valor = (quilo * watt) * 1.18;

	printf("O valor a ser pago é de: %f reais.\n", valor);

	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////

/*12. Calcular a m´edia de combust´ıvel gasto pelo usu´ario, sendo informado a quantidade de
quilˆometros rodados e a quantidade de combust´ıvel consumido.*/
#include <stdio.h>

int main ()

{

	float rodados,consumido;

	printf("Quantos quilometros rodados:\n");
	scanf("%f",&rodados);

	printf("Quantos litros de combustivel foram consumidos:\n");
	scanf("%f",&consumido);

	printf("A media de combustivel consumido foi de %f quilometros por litro.\n", (rodados/consumido));
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*13. Efetuar a leitura de uma nota e, se o valor for maior ou igual a 60, imprimir na tela
”APROVADO”.*/
#include <stdio.h>

int main ()

{
	float nota;

	printf("Digite sua nota:\n");
	scanf("%f",&nota);

	if (nota >= 60)
	{
		printf("APROVADO\n");
	}
	
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*14. Efetuar a leitura de uma nota e, se o valor for maior ou igual a 60, imprimir na tela
”APROVADO”, se for menor, imprimir ”REPROVADO”.*/

#include <stdio.h>

int main ()

{
	float nota;

	printf("Digite sua nota:\n");
	scanf("%f",&nota);

	if (nota >= 60)
	{
		printf("APROVADO\n");
	}
	else
	{
		printf("REPROVADO\n");
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*15. Efetuar a leitura de uma nota e, se o valor for maior ou igual a 60, imprimir na tela
”APROVADO”, se for menor, imprimir ”REPROVADO”. Testar ainda se o valor lido foi maior
do que 100 ou menor do que zero. Neste caso, imprimir ”NOTA INVALIDA”.*/

#include <stdio.h>

int main ()

{
	float nota;

	printf("Digite sua nota:\n");
	scanf("%f",&nota);

	if(nota > 100 && nota < 0)
	{
		printf("Nota invalida.\n");
	}
	else
	{
		if (nota >= 60)
		{
			printf("APROVADO\n");
		}
		else
		{
			printf("REPROVADO\n");
		}
	}
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////

/*16. Ler um numero inteiro e informar se o numero lido e par ou ımpar.*/
#include <stdio.h>

int main ()

{
	int num1;

	printf("Digite um número:\n");
	scanf("%d",&num1);

	if (num1 % 2 == 0)
	{
		printf("Número par.\n");
	}
	else
	{
		printf("Número impar.\n");
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*17. Ler um numero inteiro e testar se o valor lido termina com 0 (divisıvel por 10). Em caso
positivo, exiba a metade deste numero. Caso contrario, exibir a mensagem ”O numero digitado nao termina com 0”.*/
#include <stdio.h>

int main ()

{
	int num1;

	printf("Digite um número inteiro:\n");
	scanf("%d",&num1);

	if(num1 % 10 == 0)
	{
		printf("A metade do número %d é: %d.\n",num1,(num1/2));
	}
	else
	{
		printf("O número digitado não termina com 0.\n");
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*18. Ler um numero e informar se ele e positivo, negativo ou neutro (zero).*/
#include <stdio.h>

int main ()

{
	float num1;
	printf("Digite um número:\n");
	scanf("%f",&num1);

	if (num1 < 0)
	{
		printf("O número digitado é menor que 0.\n");
	}
	else if(num1 == 0)
	{
		printf("O número digitado é igual a 0.\n");
	}
	else
	{
		printf("o número digitado é maior do que 0.\n");
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*19. Faca a leitura do salario atual e do tempo de servico de um funcionario. A seguir, calcule
o seu salario reajustado. Funcionarios com ate 1 ano de empresa, receberao aumento de 10%.
Funcionarios com mais de um ano de tempo de servico, receberao aumento de 20%.*/
#include <stdio.h>

int main ()
{
	float salario_atual,tempo_servico,aumento;

	printf("Digite o salario atual:\n");
	scanf("%f",&salario_atual);

	printf("Digite o seu tempo de serviço:\n");
	scanf("%f",&tempo_servico);

	if ( tempo_servico <= 1)
	{
		aumento = salario_atual * 1.1;
		printf("Seu novo salario é: %f\n", aumento);
	}
	else
	{
		aumento = salario_atual * 1.2;
		printf("Seu novo salario é: %f\n", aumento);
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*20. Faca a leitura do ano atual e do ano de nascimento de uma pessoa e exibir sua idade.
A seguir, informe se a pessoa e bebe (0 a 3 anos), crianca (4 a 10 anos), adolescente (11 a 18
anos), adulta (19 a 50 anos) ou idosa (51 anos em diante).*/
#include <stdio.h>

int main ()

{
	int ano_atual,ano_nascimento,idade;

	printf("Digite o ano atual:\n");
	scanf("%f",&ano_atual);

	printf("Digite o ano de seu nascimento:\n");
	scanf("%f",&ano_nascimento);

	idade = ano_atual - ano_nascimento;

	if (idade >= 0 && idade <= 3)
	{
		printf("Bebe.\n");
	}
	else if ( idade >= 4 && idade <= 10)
	{
		printf("Criança.\n");
	}
	else if (idade >= 11 && idade < 18)
	{
		printf("Adolescente.\n");
	}
	else if (idade >= 19 && idade <= 50)
	{
		printf("Adulto.\n");
	}
	else
	{
		printf("idoso.\n");		
	}


	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*21. Informar o numero do mes do ano e mostrar o nome do mes por extenso. Caso o numero do mes nao exista, exibir a mensagem ”Mˆes invalido”.*/
#include <stdio.h>

int main ()

{
	int mes;

	printf("Digite o número do mês\n");
	scanf("%d",&mes);
	
	switch (mes)
	{
		case 1:
				printf("Janeiro\n");
				break;
		case 2:
				printf("Fevereiro\n");
				break;
		case 3:
				printf("Março\n");
				break;
		case 4:
				printf("Abril\n");
				break;
		case 5:
				printf("Maio\n");
				break;
		case 6:
				printf("Junho\n");
				break;
		case 7:
				printf("julho\n");
				break;
		case 8:
				printf("Agosto\n");
				break;
		case 9:
				printf("Setembro\n");
				break;
		case 10:
				printf("Outubro\n");
				break;
		case 11:
				printf("Novembro\n");
				break;
		case 12:
				printf("Dezembro\n");
				break;
		default:
				printf("Mês invalido.\n");		
	}
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////

/*22. Faca um algoritmo que receba o valor do salario de uma pessoa e o valor de um finan-
ciamento pretendido. Caso o financiamento seja menor ou igual a 5 vezes o salario da pessoa,
o algoritmo dever a escrever ”Financiamento Concedido”; senao, ele dever a escrever ”Financia-
mento Negado”.*/
#include <stdio.h>

int main ()

{
	float salario,financiamento,teste;

	printf("Digite o seu salario:\n");
	scanf("%f"&salario);

	printf("Digite o valor do financiamento:\n");
	scanf("%f",&financiamento);

	if (salario / financiamento <= 5)
	{
		printf("Financiamento concedido.\n");
	}
	else
	{
		printf("Financiamento negado.\n");
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*23. Escreva um programa para calcular e mostrar o salario semanal de uma pessoa, de-
terminado pelas condicoes que seguem. Se o numero de horas trabalhadas for inferior a 40, a
pessoa recebe R$15,00 por hora, senao a pessoa recebe R$600,00 mais R$21,00 para cada hora
trabalhada acima de 40 horas. O programa deve pedir o numero de horas trabalhadas como
entrada e deve dar o salario como saıda.*/
#include <stdio.h>

int main ()

{
	int horas,salario;

	printf("Digite quantas horas você trabalhou:\n");
	scanf("%d",&horas);

	if (horas < 40)
	{
		salario = horas * 15.0;
	}
	else
	{
		salario = 600.0 + ((horas - 40) * 21.0);
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*24. A taxa de juros aplicada em fundos depositados em um banco ´e determinada pelo tempo
em que estes ficam depositados. Para um banco em particular, a seguinte tabela ´e usada:
Usando esta informa¸c˜ao, escreva um programa que receba o tempo em que os fundos foram
mantidos em dep´osito e informe a taxa de juros correspondente.*/
#include <stdio.h>

int main ()

{
	float tempo;

	printf("Qual é o tempo que os fundos foram mantidos:\n");
	scanf("%f",&tempo);

	if (tempo < 1)
	{
		printf("A taxa de juros é de: %f%%. \n", (0.55*100));
	}
	else if ( tempo >= 1 && tempo < 2)
	{
		printf("A taxa de juros é de: %f%%. \n", (0.65*100));
	}
	else if ( tempo >= 2 && tempo < 3)
	{
		printf("A taxa de juros é de: %f%%. \n", (0.75*100));
	}
	else if (tempo >= 3 && tempo < 4)
	{
		printf("A taxa de juros é de: %f%%. \n", (0.85*100));
	}
	else if (tempo >= 4 && tempo < 5)
	{
		printf("A taxa de juros é de: %f%%. \n", (0.9*100));
	}
	else
	{
		printf("A taxa de juros é de: %f%%. \n", (0.95*100));
	}


	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*25. Desenvolva um algoritmo que leia duas notas de um aluno, um trabalho (todos os
valores entre 0 e 10) e sua frequencia, definindo e imprimindo se ele foi aprovado, reprovado ou
se fara prova final. O aluno ser a reprovado se faltou mais de 15 aulas. Sera aprovado se nao for
reprovado por falta e sua media for maior que 6,0. Caso tenha media menor, dever ́a fazer prova
final. O calculo da media deve ser feito com peso 3 para a primeira prova, 5 para a segunda
prova e 2 para o trabalho.*/
#include <stdio.h>

int main ()

{
	float prova1,prova2,trabalho,faltas,media;

	printf("Qual foi a sua nota na primeira prova:\n");
	scanf("%f",&prova1);

	printf("Qual foi a sua nota na segunda prova:\n");
	scanf("%f",&prova2);

	printf("Qual foi a sua nota no trabalho:\n");
	scanf("%f",&trabalho);

	printf("Qual foi o número de faltas.\n");
	scanf("%f",&faltas);

	if (faltas > 15)
	{
		printf("Reprovado.\n");
	}
	else
	{
		media = (prova1 * 3) + (prova2 * 5) + (trabalho * 2)/10;
		if (media > 6.0)
		{
			printf("Aprovado.\n");
		}
		else
		{
			printf("Prova de recuperação.\n");
		}
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*26. Desenvolva um programa que pergunte um c´odigo e, de acordo com o valor digitado, seja
apresentado o cargo correspondente. Caso o usu´ario digite um c´odigo que n˜ao esteja na tabela,
mostrar uma mensagem de c´odigo inv´alido. Utilize a tabela abaixo:
*/
#include <stdio.h>

int main ()

{
	int codigo;

	printf("Digite o codigo:\n");
	scanf("%d",&codigo);

	switch (codigo)
	{
		case 101:
				printf("Vendedor.\n");
				break;
		case 102:
				printf("Atendente.\n");
				break;
		case 103:
				printf("Auxiliar Técnico.\n");
				break;
		case 104:
				printf("Assitente.\n");
				break;
		case 105:
				printf("Coodenador de Grupo.\n");
				break;
		case 106:
				printf("Gerente.\n");
				break;
		default:
				printf("Código invalido.\n");
				break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*27. Uma encomenda de unidades de disco cont ́em unidades marcadas com um codigo de 1 a
4, que indica o tipo seguinte:
Escreva um programa que receba o numero de um codigo como entrada e, baseado no valor
digitado, informe o tipo correto de unidade de disco.*/
#include <stdio.h>

int main ()

{
	int codigo;

	printf("Digite o codigo(1 a 4):\n");
	scanf("%d",&codigo);

	switch (codigo)
	{
		case 1:
				printf("O disco desejado é:CD-ROM(700MB).\n");
				break;
		case 2:
				printf("O disco desejado é:DVD-ROM(4.7GB).\n");				
				break;
		case 3:
				printf("O disco desejado é:DVD-9(8.54 GB).\n");				
				break;
		case 4:
				printf("O disco desejado é:Blu-Ray(25 GB).\n");				
				break;
		default:
				printf("Codigo errado, tente outra vez.\n");
				break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////


/*28. Escreva um programa que receba dois numeros reais e um codigo de selecao do usuario.
Se o codigo digitado for 1, faca o programa adicionar os dois numeros previamente digitados
e mostrar o resultado; se o codigo de selecao for 2, os numeros devem ser multiplicados; se o
codigo de selecao for 3, o primeiro numero deve ser dividido pelo segundo. Se nenhuma das
opcoes acima for escolhida, mostrar ”Codigo invalido”.*/
#include <stdio.h>

int main ()

{
	float num1,num2,resposta;
	int codigo;

	printf("Digite dois números reais(separados por um espaço):\n");
	scanf("%f %f",&num1,&num2);

	printf("Digite 1: para efetuar uma soma.\n");
	printf("Digite 2: para efetuar uma multiplicação..\n");
	printf("Digite 3: para efetuar uma divisão do primeiro pelo segundo.\n");
	scanf("%d",&codigo);

	switch (codigo)
	{
		case 1:
				resposta = num1 + num2;
				printf("A soma destes dois números é: %f.\n",resposta);
				break;
		case 2:
				resposta = num1 * num2;
				printf("A multiplicação destes dois números é: %f.\n",resposta);
				break;
		case 3:
				resposta = num1 / num2;
				printf("A divisão destes dois números é: %f.\n",resposta);
				break;
		default:
				printf("Codigo invalido.\n");
	}
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////

/*29. Fa¸ca um algoritmo que transforme a nota de um aluno em conceito. As notas 10 e 9
receber˜ao conceito A, as notas 8 e 7 receber˜ao conceito B, as notas 6 e 5 receber˜ao conceito C
e abaixo de 5 conceito D.*/
#include <stdio.h>

int main ()

{
	int nota;
	printf("Digite sua nota:\n");
	scanf("%d",&nota);

	if (nota == 10 || nota == 9)
	{
		printf("Conceito: A.\n");
	}
	else if (nota == 8 || nota == 7)
	{
		printf("Conceito: B.\n");
	}
	else if (nota == 6 || nota == 5)
	{
		printf("Conceito: C.\n");
	}
	else if (nota <= 4 && nota >= 0)
	{
		printf("Conceito: D.\n");
	}
	else
	{
		printf("Nota invalida.\n");
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////


/*30. Desenvolva um algoritmo para que, dados dois valores inteiros entre 1 e 10 lidos, calcule
e imprima: a media dos numeros caso a soma deles seja menor que 8, seu produto caso a soma
seja igual a 8 ou a divisao do maior pelo menor caso a soma dos valores seja maior que 8.*/
#include <stdio.h>

int main ()

{
	int num1,num2;

	printf("Digite um número:\n");
	scanf("%d",&num1);
	printf("Digite outro número:\n");
	scanf("%d",&num2);

	if ((num1 >= 1) && (num1 <=10) && (num2 >= 1) && (num2<= 10))
	{
		if (num1 + num2 < 8)
		{
			printf("A media destes valores é: %d.\n", ((num1 + num2)/2));
		}
		else if(num2 + num1 == 8)
		{
			printf("O produto dos números digitados é: %d.\n", (num1 * num2));
		}
		else
		{

			if (num1 > num2)
			{
				printf("O resultado da divisão é: %d.\n",(num1/num2));	
			}
			else
			{
				printf("O resultado da divisão é: %d.\n",(num2/num1));	
			}
			
		}
	}

	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////

/*31. Escrever um algoritmo que lˆe 10 valores e conte quantos destes valores s˜ao negativos.
Imprima esta informa¸c˜ao.*/
#include <stdio.h>

int main ()

{
	float valor;
	int contador,cont = 1;
	do{
		printf("Digite algum valor:\n");
		scanf("%f",&valor);
		if (valor < 0)
		{
			++contador;
		}
		cont++;
	}while(cont <= 10);
	
	printf("Foram digitados %.0f valores negativos.\n",contador);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*32. Escreva um algoritmo que leia 6 valores e encontre o maior e o menor deles. Mostre o
resultado.*/
#include <stdio.h>

int main ()

{
	int contador = 1,cont = 0;
	float valor,compara_maior,compara_menor;
	while (contador <= 6)
	{
		printf("Digite um valor:\n");
		scanf("%f",&valor);

		if (cont == 0)
		{
			compara_maior = valor;
			compara_menor = valor;
			++cont;
		}
		
		if(valor > compara_maior)
		{
			compara_maior = valor;
		}
		if(valor < compara_menor)
		{
			compara_menor = valor;
		}
		contador++;
	}

	printf("Maior número digitador: %f\n", compara_maior);
	printf("Menor número digitador: %f\n", compara_menor);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*33. Fa¸ca um algoritmo que lˆe um valor N inteiro e positivo e que calcula e escreve o fatorial
de N (N!).*/
#include <stdio.h>

int main ()

{
	int num,contador = 1;
	long double faltorial = 1;
	printf("Digite um valor:\n");
	scanf("%d",&num);
	while (contador <= num)
	{
		faltorial = faltorial * contador;
		++contador;
	}

	printf("Fatorial: %Lf\n", faltorial);//LE LONG DOUBLE 
											//lf DOUBLE
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*34. A prefeitura de uma cidade fez uma pesquisa entre seus habitantes, coletando dados
sobre o sal´ario e n´umero de filhos. A prefeitura deseja saber:*/
#include <stdio.h>

int main ()

{
	float salario_media,filhos_media,maior_salario,menos_de_cem,salario,
		  soma_salario = 0,soma_filhos = 0,porcem;
	int cont = 0,filhos;

	do{
		printf("Qual é o seu salario?\n");
		scanf("%f",&salario);
		if (cont == 0)
		{
			maior_salario = salario;
		}
		if(salario > 0)
		{
			++cont;
			soma_salario = salario + soma_salario;
		
			if (salario > maior_salario)
			{
				maior_salario = salario;
			}


			printf("Quantos filhos você tem?\n");
			scanf("%d",&filhos);
			if (salario <= 100)
			{
				porcem++;
			}
		
			soma_filhos = filhos + soma_filhos;

		}

	}while(salario >= 0);

	salario_media = soma_salario/cont;
	filhos_media = soma_filhos/cont;
	menos_de_cem = (porcem/cont)*100;
	printf("Media dos salarios:%f\n",salario_media);
	printf("Media filhos: %f\n",filhos_media);
	printf("%f maior salario\n", maior_salario);
	printf("%f porcenttual de pessoas com salario abaixo de 100 reais.\n", menos_de_cem);


	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*35. Chico tem 1,50 metro e cresce 2 centımetros por ano, enquanto Ze tem 1,30 metro e
cresce 3 centımetros por ano. Construa um algoritmo que calcule e imprima quantos anos serao necessarios para que 
Ze seja maior que Chico.*/
#include <stdio.h>

int main ()

{
	int contador = 0;
	float altura_chico = 1.5,altura_ze = 1.30;
	while(altura_chico > altura_ze)
	{
		altura_chico = altura_chico + 0.02;
		altura_ze = altura_ze + 0.03;
		contador++;
	}

	printf("Anos %d\n", contador);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*36. Escrever um algoritmo que leia uma vari´avel n e calcule a tabuada de 1 at´e n. Mostre a
tabuada na forma:*/
#include <stdio.h>

int main ()

{
	int num,cont=1;
	printf("Digite um número inteiro:\n");
	scanf("%d",&num);

	while(cont <= num)
	{
		printf("%d x %d = %d\n", cont,num,(num * cont));
		++cont;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*37. Escrever um algoritmo que leia um n´umero n˜ao determinado de valores e calcule a m´edia
aritm´etica dos valores lidos, a quantidade de valores positivos, a quantidade de valores negativos
e o percentual de valores negativos e positivos. Mostre os resultados. O n´umero que encerrar´a
a leitura ser´a zero.*/
#include <stdio.h>

int main ()

{
	float num,por_neg,por_posi,media;
	int contador_positivos = 0,contador_negativos = 0,contador_valores = 0;
	do{
		printf("Digite um valor\n");
		scanf("%f",&num);
		media = media + num;
		if(num > 0)
		{
			++contador_positivos;
		}
		if(num < 0)
		{
			++contador_negativos;
		}
		if(num != 0)
		{
			++contador_valores;
		}

	}while (num != 0);
	por_posi = (contador_negativos/contador_valores)*100;
	por_neg = (contador_positivos/contador_valores)*100;
	media = media/contador_valores;

	printf("A media dos valores lidos é: %.3f.\n", media);
	printf("%d valor(s) negativo(s).\n", contador_negativos);
	printf("%d valor(s) positivo(s).\n", contador_positivos);
	printf("O percentual de valores negativos: %f porcento.\n",por_neg);
	printf("O percentual de valores positivos: %f porcento.\n", por_posi);

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////////

/*38. Faca um algoritmo que leia uma quantidade nao determinada de numeros positivos.
Calcule a quantidade de numeros pares e ımpares, a media de valores pares e a media geral dos
numeros lidos. O numero que encerrara a leitura sera zero.*/
#include <stdio.h>

int main ()

	
{
	int num,num_pares = 0,num_impares = 0;
	float media_pares = 0,media_geral = 0,total = 0;
	do{
		printf("\n\nDigite um numero inteiro e positivo:\n");
		scanf("%d",&num);
		if(num > 0)
		{
			if (num % 2 == 0)
			{
				++num_pares;
				media_pares = media_pares + num;
			}
			else
			{
				++num_impares;
			}

			media_geral = media_geral + num;
			++total;

		}
		else
		{
			printf("Número invalido ou fim do programa.\n");
		}
	} while (num != 0);

	media_pares = media_pares/num_pares;
	media_geral = media_geral/total;
	printf("\n\nValores pares:%d.\n", num_pares);
	printf("Valores impares: %d.\n",num_impares);
	printf("Media pares: %f.\n", media_pares);
	printf("Media total:%f.\n\n", media_geral);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/*39. Escrever um algoritmo que le 10 valores, um de cada vez, e conte quantos deles estao 
no intervalo [10,20] e quantos deles estao fora do intervalo, escrevendo estas informacoes.*/

#include <stdio.h>

int main ()

{
	int cont = 1,dentro,fora;
	float num1;
	while(cont <= 10)
	{
		printf("Digite um valor:\n");
		scanf("%f",&num1);
		if (num1 >= 10 && num1 <= 20)
		{
			++dentro;
		}
		else
		{
			++fora;
		}
	}

	printf("%d valor(es) dentro da faixa.\n", dentro);
	printf("%d valor(s) fora da faixa.\n", fora);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////


