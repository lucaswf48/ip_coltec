/*35. Chico tem 1,50 metro e cresce 2 centımetros por ano, enquanto Ze tem 1,30 metro e
cresce 3 centımetros por ano. Construa um algoritmo que calcule e imprima quantos anos serao necessarios para que 
Ze seja maior que Chico.*/
#include <stdio.h>

int main ()

{
	int contador = 0;
	float altura_chico = 1.5,altura_ze = 1.30;
	while(altura_chico > altura_ze)
	{
		altura_chico = altura_chico + 0.02;
		altura_ze = altura_ze + 0.03;
		contador++;
	}

	printf("Anos %d\n", contador);
	return 0;
}