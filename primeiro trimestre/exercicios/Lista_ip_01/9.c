/*9. Converter uma quantidade de horas digitadas pelo usuario em minutos. Informe o resul-
tado em minutos.*/
#include <stdio.h>

int main ()

{
	float horas,minutos;
	printf("Digite um valor em horas:\n");
	scanf("%f",&horas);

	minutos = horas * 60;

	printf("%f horas são %f minutos.\n", horas,minutos);
	return 0;
}