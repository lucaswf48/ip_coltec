/*11. Efetuar a leitura do n´umero de quilowatts consumido e calcular o valor a ser pago de
energia el´etrica, sabendo-se que o valor a pagar por quilowatt ´e de 0,12. Apresentar o valor total
a ser pago pelo usu´ario acrescido de 18% de ICMS.*/

#include <stdio.h>
#define ICMS 0.18
#define watt 0.12

int main ()

{
	float quilo,valor;
	
	printf("Quantos quilowatts foram consomidos?\n");
	scanf("%f",&quilo);

	valor = (quilo * watt) * 1.18;

	printf("O valor a ser pago é de: %f reais.\n", valor);

	return 0;
}