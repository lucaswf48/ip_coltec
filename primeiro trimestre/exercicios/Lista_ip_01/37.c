/*37. Escrever um algoritmo que leia um n´umero n˜ao determinado de valores e calcule a m´edia
aritm´etica dos valores lidos, a quantidade de valores positivos, a quantidade de valores negativos
e o percentual de valores negativos e positivos. Mostre os resultados. O n´umero que encerrar´a
a leitura ser´a zero.*/
#include <stdio.h>

int main ()

{
	float num,por_neg,por_posi,media;
	int contador_positivos = 0,contador_negativos = 0,contador_valores = 0;
	do{
		printf("Digite um valor\n");
		scanf("%f",&num);
		media = media + num;
		if(num > 0)
		{
			++contador_positivos;
		}
		if(num < 0)
		{
			++contador_negativos;
		}
		if(num != 0)
		{
			++contador_valores;
		}

	}while (num != 0);
	por_posi = (contador_negativos/contador_valores)*100;
	por_neg = (contador_positivos/contador_valores)*100;
	media = media/contador_valores;

	printf("A media dos valores lidos é: %.3f.\n", media);
	printf("%d valor(s) negativo(s).\n", contador_negativos);
	printf("%d valor(s) positivo(s).\n", contador_positivos);
	printf("O percentual de valores negativos: %f porcento.\n",por_neg);
	printf("O percentual de valores positivos: %f porcento.\n", por_posi);

	return 0;
}
