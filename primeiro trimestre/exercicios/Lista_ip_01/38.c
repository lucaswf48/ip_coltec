/*38. Faca um algoritmo que leia uma quantidade nao determinada de numeros positivos.
Calcule a quantidade de numeros pares e ımpares, a media de valores pares e a media geral dos
numeros lidos. O numero que encerrara a leitura sera zero.*/
#include <stdio.h>

int main ()

	
{
	int num,num_pares = 0,num_impares = 0;
	float media_pares = 0,media_geral = 0,total = 0;
	do{
		printf("\n\nDigite um numero inteiro e positivo:\n");
		scanf("%d",&num);
		if(num > 0)
		{
			if (num % 2 == 0)
			{
				++num_pares;
				media_pares = media_pares + num;
			}
			else
			{
				++num_impares;
			}

			media_geral = media_geral + num;
			++total;

		}
		else
		{
			printf("Número invalido ou fim do programa.\n");
		}
	} while (num != 0);

	media_pares = media_pares/num_pares;
	media_geral = media_geral/total;
	printf("\n\nValores pares:%d.\n", num_pares);
	printf("Valores impares: %d.\n",num_impares);
	printf("Media pares: %f.\n", media_pares);
	printf("Media total:%f.\n\n", media_geral);
	return 0;
}