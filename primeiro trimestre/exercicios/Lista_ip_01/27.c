/*27. Uma encomenda de unidades de disco cont ́em unidades marcadas com um codigo de 1 a
4, que indica o tipo seguinte:
Escreva um programa que receba o numero de um codigo como entrada e, baseado no valor
digitado, informe o tipo correto de unidade de disco.*/
#include <stdio.h>

int main ()

{
	int codigo;

	printf("Digite o codigo(1 a 4):\n");
	scanf("%d",&codigo);

	switch (codigo)
	{
		case 1:
				printf("O disco desejado é:CD-ROM(700MB).\n");
				break;
		case 2:
				printf("O disco desejado é:DVD-ROM(4.7GB).\n");				
				break;
		case 3:
				printf("O disco desejado é:DVD-9(8.54 GB).\n");				
				break;
		case 4:
				printf("O disco desejado é:Blu-Ray(25 GB).\n");				
				break;
		default:
				printf("Codigo errado, tente outra vez.\n");
				break;
	}
	return 0;
}