/*30. Desenvolva um algoritmo para que, dados dois valores inteiros entre 1 e 10 lidos, calcule
e imprima: a media dos numeros caso a soma deles seja menor que 8, seu produto caso a soma
seja igual a 8 ou a divisao do maior pelo menor caso a soma dos valores seja maior que 8.*/
#include <stdio.h>

int main ()

{
	int num1,num2;

	printf("Digite um número:\n");
	scanf("%d",&num1);
	printf("Digite outro número:\n");
	scanf("%d",&num2);

	if ((num1 >= 1) && (num1 <=10) && (num2 >= 1) && (num2<= 10))
	{
		if (num1 + num2 < 8)
		{
			printf("A media destes valores é: %d.\n", ((num1 + num2)/2));
		}
		else if(num2 + num1 == 8)
		{
			printf("O produto dos números digitados é: %d.\n", (num1 * num2));
		}
		else
		{

			if (num1 > num2)
			{
				printf("O resultado da divisão é: %d.\n",(num1/num2));	
			}
			else
			{
				printf("O resultado da divisão é: %d.\n",(num2/num1));	
			}
			
		}
	}

	return 0;
}