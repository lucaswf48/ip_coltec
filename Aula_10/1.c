/*1. Faça um programa para leitura, via teclado, dos dados de um
aluno. Os dados a serem guardados na estrutura aluno são os
seguintes: nome, curso, idade. Ao ﬁnal, imprima estas informações
na tela.*/
#include <stdio.h>
#define DIM 50

//Estrutura aluno
typedef struct aluno{

	char nome[DIM]; 	//Nome do aluno
	char curso[DIM];	//Curso do aluno
	int idade;			//Idade do aluno
}alun;

int main(){

	alun al1;

	printf("Digite o nome do aluno:\n");
	fgets(al1.nome,49,stdin);

	printf("Digite o curso do aluno:\n");
	fgets(al1.curso,49,stdin);

	printf("Digite a idade do aluno:\n");
	scanf("%d",&al1.idade);

	printf("\n");

	//printf("O aluno %s do curso de %s tem %d anos de idade.\n", al1.nome,al1.curso,al1.idade);
	printf("Nome do aluno: %s", al1.nome);
	printf("Curso do aluno: %s", al1.curso);
	printf("Idade: %d\n\n", al1.idade);

	return 0;
}