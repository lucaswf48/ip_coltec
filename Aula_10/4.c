/*4. Acrescente um procedimento ao exercício 3 que ordene a
estrutura de alunos em ordem crescente da média das notas. Ao
final, imprima todas as informações na tela (ordenadas pela média
das notas).*/
#include <stdio.h>
#include <stdlib.h>
#define DIM 50

typedef struct aluno{

	char nome[DIM]; 	//Nome do aluno
	char curso[DIM];	//Curso do aluno
	int idade;			//Idade do aluno
	float media;
	float notas[3];

}alun;



int main(){

	alun *nalunos,al;
	int n,cont,cont1;
	float mediaturma = 0;
	int a,b;	

	printf("Quantos alunos:\n");
	scanf("%d",&n);
	__fpurge(stdin);

	nalunos = (alun*)malloc(n*sizeof(alun));

	for ( cont = 0; cont < n; cont++){

		nalunos[cont].media = 0;

		printf("\nDigite o nome do aluno:\n");
		fgets(nalunos[cont].nome,49,stdin);
		

		printf("Digite o curso do aluno:\n");
		fgets(nalunos[cont].curso,49,stdin);

		printf("Digite a idade do aluno:\n");
		scanf("%d",&nalunos[cont].idade);
		__fpurge(stdin);

		for (cont1 = 0;cont1 < 3;cont1++){
			
			printf("Digite a nota %d:\n", cont1+1);
			scanf("%f",&nalunos[cont].notas[cont]);
			__fpurge(stdin);


			nalunos[cont].media = nalunos[cont].notas[cont] + nalunos[cont].media;
		}
		nalunos[cont].media = nalunos[cont].media/3;
		mediaturma = mediaturma + nalunos[cont].media;
	}

	
	
	for (a = 0; a < n;a++){
		for(b = 0; b < n;b++){
		
			if (nalunos[a].media < nalunos[b].media){
				al = nalunos[a];
				nalunos[a] = nalunos[b];
				nalunos[b] = al;
			}
		}
		
	}
	mediaturma = mediaturma/n;

	for(cont = 0;cont < n;cont++){

		printf("\nNome do aluno: %s", nalunos[cont].nome);
		printf("Curso do aluno: %s", nalunos[cont].curso);
		printf("Idade: %d\n", nalunos[cont].idade);
		printf("Media: %f\n\n", nalunos[cont].media);
		if (nalunos[cont].media < 60)
			printf("Situação:\nreprovado.\n\n");
		else
			printf("Situação:\nAprovado.\n\n");
		printf("Digite qualquer tecla para continuar:\n");
		scanf("%*c");
	}
	

	printf("A media da turma é: %f\n\n", mediaturma);	

	return 0;
}

