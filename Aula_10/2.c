/*2. Considere a mesma estrutura deﬁnida anteriormente. Acrescente
à estrutura um vetor com as notas das três provas feitas pelo aluno,
calcule a sua média e diga se ele foi aprovado ou não (media >=
60).*/
#include <stdio.h>
#define DIM 50

typedef struct aluno{

	char nome[DIM]; 	//Nome do aluno
	char curso[DIM];	//Curso do aluno
	int idade;			//Idade do aluno
	float notas[3];
	float media;
}alun;

int main(){

	alun al1;
	int cont;

	al1.media = 0;

	printf("Digite o nome do aluno:\n");
	fgets(al1.nome,49,stdin);

	printf("Digite o curso do aluno:\n");
	fgets(al1.curso,49,stdin);

	printf("Digite a idade do aluno:\n");
	scanf("%d",&al1.idade);

	for (cont = 0;cont < 3;cont++){
		
		printf("Digite a nota %d:\n", cont+1);
		scanf("%f",&al1.notas[cont]);

		al1.media = al1.notas[cont] + al1.media;
	}

	al1.media = al1.media/3;

	printf("\n");

	//printf("O aluno %s do curso de %s tem %d anos de idade.\n", al1.nome,al1.curso,al1.idade);
	printf("Nome do aluno: %s", al1.nome);
	printf("Curso do aluno: %s", al1.curso);
	printf("Idade: %d\n", al1.idade);
	printf("Media: %f\n\n", al1.media);


	if (al1.media < 60)
		printf("Situação:\nreprovado.\n\n");
	else
		printf("Situação:\nAprovado.\n\n");
	
	return 0;
}
