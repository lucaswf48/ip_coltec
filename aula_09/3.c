/*3. Faça um programa que leia um vetor de dimensão 5 e uma
matriz quadrada de dimensão 5. Crie um procedimento que
multiplique o vetor pela matriz. Imprima o resultado.
*/
#include <stdio.h>
#define DIM 5
//Procedimento responsavel por ler matriz 5x5
void lerMatriz(float m[][DIM],int j,int i);
//Procedimento responsavel por imprimir a matriz
void imprimiMatriz(float m[][DIM],int j,int i);
//Procedimento reponsavel por imprimir o vetor
void imprimiVetor(float v[],int j);
//Procedimento reponsavel por ler o vetor
void lerVetor(float v[],int j);


int main(){

	float matriz[DIM][DIM],						//Matriz 5x5 a ser lida
		  vet[DIM],								//Vetor a ser lido
		  vet2[DIM] = {0.0,0.0,0.0};	//Vetor qua vai receber o resultado
	
	int j,	//Contador de linhas
		i;	//Contador de colunas

	char es; //Escolha s/n para ver o vetor ou a matriz

	//Procedimento responsavel por ler a matriz
	lerMatriz(matriz,j,i);
	
	printf("\n");
	
	//O loop continua enquanto for digitado um caracter diferente de n/s
	do{

		printf("Você deseja ver a matriz criada:(s/n)\n");
		scanf("%c",&es);

		//Limpa o buffer
		__fpurge(stdin);

		//Se for digitado um caracter diferente de n/s
		if (es != 's' && es != 'n')
		{
			printf("Opção invalida.\n");
		}

	}while(es != 's' && es != 'n');

	//Se es == 's' chama a procedimento imprimiMatriz
	if (es == 's')
	{
		imprimiMatriz(matriz,j,i);
	}
	
	//Procedimento responsavel por ler o vetor
	lerVetor(vet,j);

	//O loop continua enquanto for digitado um caracter diferente de n/s
	do{

		printf("\nVocê deseja ver a vetor criado:(s/n)\n");
		scanf("%c",&es);

		//Limpa o buffer
		__fpurge(stdin);

		//Se for digitado um caracter diferente de n/s
		if (es != 's' && es != 'n')
		{
			printf("Opção invalida.\n");
		}

	}while(es != 's' && es != 'n');

	//Se es == 's' chama a procedimento imprimiMatriz
	if (es == 's')
	{
		imprimiVetor(vet,j);
	}


	printf("O resultado da multiplicação:\n");
	//Multiplica os elementos da matriz pelos elementos do 
	//Varia as linhas da matriz
	for(j = 0;j < DIM;j++){

		//Varia as colunas da matriz
		for(i = 0;i < DIM;i++){

			vet2[j] = (matriz[j][i] * vet[i]) + vet2[j];
		}
		//Imprime o vetor criado
		printf("%f, ", vet2[j]);
	}

	printf("\n");

	return 0;
}

//Procedimento responsavel por ler matriz 5x5
void lerMatriz(float m[][DIM],int j,int i){
	
	
	printf("\nPrencha a matriz de 25 posições\n");	
	//Varia as linhas da matriz
	for (j = 0;j < DIM;j++){

		//Varia as colunas da matriz
		for (i = 0;i < DIM;i++){
				printf("Digite o valor da posição [%d][%d]\n", j,i);
				scanf("%f",&m[j][i]);
		}
	}

	//Limpa o buffer
	__fpurge(stdin);

}

//Procedimento responsavel por imprimir a matriz
void imprimiMatriz(float m[][DIM],int j,int i){

	//Varia as linhas da matriz
	for (j = 0;j < DIM;j++){
		//Varia as colunas da matriz
		for (i = 0;i < DIM;i++){
			printf("%.3f ",m[j][i]);
		}
		///Quebra a linha apenas quando acabar a linha da matriz
		printf("\n");
	}

}
//Procedimento reponsavel por ler o vetor
void lerVetor(float v[],int j){
	printf("\n");
	printf("Prencha o vetor de 5 posições\n");
	//Varia os valores do vetor
	for (j = 0;j < DIM;j++){
		printf("Digite o valor da posição [%d]\n", j);
		scanf("%f",&v[j]);
	}
	__fpurge(stdin);
}

//Procedimento reponsavel por imprimir o vetor
void imprimiVetor(float v[],int j){
	//Varia os valores do vetor
	for (j = 0;j < DIM;j++)
		printf("%.3f ",v[j]);

	printf("\n");	
}