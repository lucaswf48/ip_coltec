/*4. Faça um programa para ler a quantidade de um total de 5
produtos que uma empresa tem em suas 7 lojas e imprimir em uma
tabela:
a) o total de cada produto nestas lojas
b) a loja que tem menos produtos*/
#include <stdio.h>
#include <time.h>
#define DIM1 5
#define DIM2 7

//Função responsavel por calcular o total de cada produto, e a loja com menos produtos
int calculaTotalEMenor(int m[][DIM1],int a,int b,int *menor);

int main(){

	int matriz[DIM2][DIM1], //linhas:loja colunas:produtos
		j,					//Contador de linhas
		i,					//Contador de colunas
		soma,				//Soma de cada produto
		menos;				//Loja com menos produtos de cada tipo

	char escolha;			//Escolha para ver ou não ver a matriz gerada

	printf("\n");

	//Varia as linhas da matriz
	for (j = 0;j < DIM2;j++){
		
		//Varia as colunas da matriz
		for(i = 0;i < DIM1;i++){
			printf("Loja de número %d\n",j+1);
			printf("Produto de número %d\n",i+1);
			//printf("Digite a quantidade total do produto:\n");
			
			printf("Digite qual tecla para adicionar um número:");
			//Ler um caracter, mas não o armazena em uma variavel
			scanf("%*c");
			//A leitura do caracter é feita apenas para que o srand(time(NULL)) gere um número
			//diferente
			//Gera um número aleatorio para a matriz;

			srand(time(NULL));
			// 0 a 99	
			matriz[j][i] = rand()%100;
			
			printf("Número gerado: %d\n", matriz[j][i]);
			printf("\n");
		}
	}


		printf("Você deseja ver a matriz gera?[s/n]");
		scanf("%c",&escolha);

		//Se for digitado um caracter igual de s
		if (escolha=='s'){

			//Imprime a matriz gerada
			printf("---------------------------------\n");
			for (j = 0;j < DIM2;j++){
				for (i = 0;i < DIM1;i++){
					printf("%d ",matriz[j][i]);
				}
				printf("\n");
			}
			printf("---------------------------------\n");
		}

	//Tabela
	printf("	  |Soma da quantidade de produtos|Loja com menos produtos|\n");
	for (i = 0;i < DIM1;i++){

		//Chama a função calculaTotalEMenor
		//Soma = recebe o total de cada produto
		soma = calculaTotalEMenor(matriz,j,i,&menos);

		printf("Produto %d | %d                          | %d\n", i + 1,soma,menos+1);
	}
	
	
	
}

//Função responsavel por calcular o total de cada produto, e a loja com menos produtos
int calculaTotalEMenor(int m[][DIM1],int a,int b,int *menor){
	
	//Soma
	int s = 0;

	for (a = 0;a < DIM2;a++){

		s += m[a][b];
		if(a==0){
			*menor = a;
		}

		if(m[a][b] < m[*menor][b]){
			*menor = a;
		}
		
	}

	return s;
}