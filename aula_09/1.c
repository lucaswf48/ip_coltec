/*1. Faça um programa que leia uma matriz 5x5 e um procedimento
que imprima sua transposta.*/

#include <stdio.h>
#include <time.h>

#define DIM 5 //Define o tamanho da matriz

void transposta(int m[DIM][DIM],int m2[DIM][DIM],int j,int i);

int main(){

	int matriz[DIM][DIM],  //Matriz inicial 5x5
		matriz2[DIM][DIM], //Matriz transposta 5x5
		j,				   //Contador de linhas
		i;				   //COntador de colunas


	//Variavel i representa a quantidade de colunas
	//Variavel j representa a quantidade de linhas

	//Gera os elentos da matriz

	//Varia as linhas da matriz 
	for (j = 0;j < DIM;j++){
		//Varia as colunas da matriz
		for (i = 0;i < DIM;i++){
		
			printf("\nDigite qual tecla para adicionar um número:");
			//Ler um caracter, mas não o armazena em uma variavel
			scanf("%*c");
			//A leitura do caracter é feita apenas para que o srand(time(NULL)) gere um número
			//diferete

			//Gera um número aleatorio para a matriz;
			srand(time(NULL));	
			matriz[j][i] = rand()%100;
			
		}//Fim for
	}//Fim for

	//Imprime a matriz gerada
	printf("Matriz gerada:\n");

	//Varia as linhas da matriz
	for (j = 0;j < DIM;j++){
		//Varia as colunas da matriz
		for (i = 0;i < DIM;i++){

			printf("%d ",matriz[j][i]);
		}//Fim for
		//Quebra a linha quando a linha da matriz acaba
		printf("\n");
	}//Fim for 

	//Chama o procedimento transposta
	transposta(matriz,matriz2,j,i);


	printf("\nA matriz transposta é:\n");

	//Imprime a matriz transposta
	for (j = 0;j < DIM;j++){
		for (i = 0;i < DIM;i++){
			printf("%d ",matriz2[j][i]);
		}//Fim for
		//Quebra a linha quando a linha da matriz acaba
		printf("\n");
	}//Fim for

	printf("\n");

}//Fim main

void transposta(int m[DIM][DIM],int m2[DIM][DIM],int j,int i){

	//Transposta a matriz
	//Varia as linhas da matriz
	for (j = 0;j < DIM;j++){
		//Varia as colunas da matriz
		for (i = 0;i < DIM;i++){

			//Copia o valor da posição [j][i] para a posição [i][j] matriz2 
			m2[j][i] = m[i][j];
		}
	}	
			
}//Fim transposta

