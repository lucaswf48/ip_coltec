/*2. Crie uma função que receba uma matriz de números reais e retorne a soma dos elementos desta matriz.*/
#include <stdio.h>
#include <stdlib.h>

int main(){

	int linhas,	 //Quantidade de linhas da matriz
		colunas, //Quantidade de colunas da matriz
		i,		 //Contador de colunas
		j;
	float **matriz,soma = 0.0;

	do{
		printf("-------------------------------------\n");
		printf("A matriz possue duas dimensões.\n");
		printf("Digite o numero de linhas:\n");
		scanf("%d",&linhas);
		printf("Digite o numero de linhas:\n");
		scanf("%d",&colunas);
		printf("-------------------------------------\n");
		__fpurge(stdin);
		if( linhas < 1 || colunas <1 )
			printf("Alguma das dimensões é invalida.\n\nTente novamente\n");

	}while( linhas < 1 || colunas <1 );

	matriz = (float **) malloc(linhas*sizeof(float *));

	if (matriz == NULL)
	{
		printf("** Erro: memoria insuficiente.");
	}

	for(i = 0;i<linhas;i++){
		matriz[i]=(float *) malloc(colunas*sizeof(float *));
		if (matriz[i] == NULL)
		{
			printf("** Erro: memoria insuficiente.");
		}		
	}

	for (j = 0;j < linhas;j++){
		for (i = 0;i < colunas;i++){
		
			printf("\nDigite qual tecla para adicionar um número:");
			scanf("%*c");
			srand(time(NULL));	
			matriz[j][i] = rand()%100;
			soma+=matriz[j][i];
			printf("\n");
		}
	}

	for (j = 0;j < linhas;j++){
		for (i = 0;i < colunas;i++){
			printf("%.3f ",matriz[j][i]);
		}
		printf("\n");
	}


	printf("A soma dos valores da matriz é %.3f.\n\n", soma);

}