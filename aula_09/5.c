#include <stdio.h>
#include <stdlib.h>

int main(){
	
	float **matriz1,**matriz2,**matrizproduto;
	int linhamatriz1,colunamatriz1,linhamatriz2,colunamatriz2,cont,cont2,j,i,r = 0;

	printf("Digite a quantidade de linhas da primeira matriz: ");
	scanf("%d",&linhamatriz1);
	printf("Digite a quantidade de colunas da primeira matriz: ");
	scanf("%d",&colunamatriz1);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	matriz1 = (float **) malloc(linhamatriz1*sizeof(float *));

	if (matriz1 == NULL)
	{
		printf("\tERRO: MEMORIA INSUFICIENTE!");
		return 0;
	}

	for (cont = 0;cont < linhamatriz1;cont++)
	{
		matriz1[cont] =(float *) malloc(colunamatriz1*sizeof(float *));

		if (matriz1[cont] == NULL)
		{
			printf("\tERRO: MEMORIA INSUFICIENTE!");
			return 0;
		}
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	printf("Digite a quantidade de linhas da segunda matriz:");
	scanf("%d",&linhamatriz2);
	printf("Digite a quantidade de colunas da segunda matriz:");
	scanf("%d",&colunamatriz2);

	if (colunamatriz1 != linhamatriz2)
	{
		printf("Matrizes invalidas!!!!!!!!!\n");
		return 0;
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	matriz2 = (float **) malloc(linhamatriz2*sizeof(float *));

	if (matriz2 == NULL)
	{
		printf("ERRO: MEMORIA INSUFICIENTE!");
		return 0;
	}

	for (cont = 0;cont < linhamatriz2;cont++)
	{
		matriz2[cont] =(float *) malloc(colunamatriz2*sizeof(float *));

		if (matriz2[cont] == NULL)
		{
			printf("ERRO: MEMORIA INSUFICIENTE!");
		}
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	matrizproduto = (float **) calloc(linhamatriz1,sizeof(float *));

	if (matrizproduto == NULL)
	{
		printf("ERRO: MEMORIA INSUFICIENTE!");
		return 0;
	}

	for (cont = 0;cont < linhamatriz1;cont++)
	{
		matrizproduto[cont] =(float *) malloc(colunamatriz2*sizeof(float *));

		if (matrizproduto[cont] == NULL)
		{
			printf("ERRO: MEMORIA INSUFICIENTE!");
		}
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	printf("\n");

	for(j = 0;j < linhamatriz1;j++)
		for(i = 0;i < colunamatriz1;i++){
			printf("Digite o valor da posição [%d][%d] da primeira matriz:",j,i);
			scanf("%f",&matriz1[j][i]);
		}

	printf("\n");

	for(j = 0;j < linhamatriz2;j++)
		for(i = 0;i < colunamatriz2;i++){
			printf("Digite o valor da posição [%d][%d] da segunda matriz:",j,i);
			scanf("%f",&matriz2[j][i]);
		}


	for(i=0;i<linhamatriz1;i++){
		for(j=0;j<colunamatriz2;j++){
			for(r=0;r<linhamatriz1;r++){
					matrizproduto[i][j]=matrizproduto[i][j]+(matriz1[i][r]*matriz2[r][j]);
			}
		}
	}

	printf("\nA soma das matrizes é:\n");
	for(j = 0;j < linhamatriz1;j++){
		for(i = 0;i < colunamatriz2;i++){
			//printf("Digite o valor da posição [%d][%d] da segunda matriz:",j,i);
			printf("%.3f ", matrizproduto[j][i]);
			
		}
		printf("\n");
	}
	return 0;
}


