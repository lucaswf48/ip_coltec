#include <stdio.h>

int main ()
{
	int ava,count = 0,faltas,nota1,nota2,trabalho;
	float media;
	printf("\nQuantos alunos serão avaliados?\n");
	scanf("%d",&ava);

	while (count < ava)
	{
		printf("Digite a nota da primeira prova:\n");
		scanf("%d",&nota1);
		printf("Digite a nota da segunda prova:\n");
		scanf("%d",&nota2);
		printf("Digite a nota do trabalho:\n");
		scanf("%d",&trabalho);
		printf("Digite o numero de faltas:\n");
		scanf("%d",&faltas);

		media = ((nota2*5) + (nota1*3) + (trabalho*2)) / 10;

		if (faltas > 15)
		{
			printf("Reprovado.\n\n");
		}
		else if (media > 6.0)
		{
			printf("Aprovado.\n\n");
		}
		else
		{
			printf("Prova final.\n\n");
		}
		count++;

	}
	return 0;
}