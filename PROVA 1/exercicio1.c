#include <stdio.h>

int main ()

{
	int opcao;

	printf("\nForma de pagamento:\n");
	printf("1. A vista.\n");
	printf("2. Cheque para trinta dias.\n");
	printf("3. Em duas vezes.\n");
	printf("4. Em três vezes.\n");
	printf("Entre com sua opção:");
	scanf("%d",&opcao);

	if (opcao == 1)		printf("\nOpção = 1: Desconto de 20%%.\n\n");

	else if (opcao == 2 || opcao == 3)
	{
		printf("\nOpção = 2 ou opção = 3: Mesmo preço a vista.\n\n");
	}
	else if (opcao == 4)
	{
		printf("\nJuros de 3%% ao mês.\n\n");
	}
	else
	{
		printf("\nOpção < 1 ou opção > 4: Opção invalida.\n\n");
	}
	return 0;
}