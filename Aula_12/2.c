/*2. Seja S um vetor de inteiros. Descreva funções recursivas para
calcular:
a) o elemento máximo de S;
b) a soma dos elementos de S;
c) média aritmética dos elementos de S*/
#include <stdio.h>
#include <stdlib.h>

int* alocaVetor(int tam){

	int *vet;

	vet = (int*)calloc(tam,sizeof(int));

	return vet;
}

void ler_vetor_int(int vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		scanf("%d",&vet[j]);
	}
	printf("\n");
}

void imprime_vetor_int(int vet[],int tam){

	int j;
	
	for(j = 0;j < tam;++j){
		printf("%d, ",vet[j]);
	}
	printf("\n");
}


int partition(int a[],int l, int r){
	int pivot,i,j,aux;
	pivot = a[l];
	i=l;
	j=r+1;
	while(1){
		do ++i;while(a[i] <= pivot && i <= r);
		do --j;while(a[j] > pivot);
		if(i >= j)break;
		aux = a[i];
		a[i]=a[j];
		a[j]= aux;
	}
	aux= a[l];
	a[l]= a[j];
	a[j]= aux;
	return j;
}

void quickSort(int a[],int l,int r){
	int j;
	if(l < r){
		j=partition(a,l,r);
		quickSort(a,l,j-1);
		quickSort(a,j+1,r);
	}
}

int maiorElementoRecursivo(int vet[],int tam,int a){



	if(a==tam){
		return vet[a];
	}
	else{
		return maiorElementoRecursivo(vet,tam,a+1);
	}
}

int somaVetorRecursivo(int vet[],int tamanho,int a,int soma){


	if(a==tamanho){
		return soma;
	}
	else{
		return somaVetorRecursivo(vet,tamanho,a=a+1,soma += vet[a]);
	}

}

float mediaAritmetica(int vet[],int tamanho){

	float soma1;
	soma1 = somaVetorRecursivo(vet,tamanho,0,0);
	return soma1/tamanho;
}

void menu(){
	printf("Digite:\n");
	printf("a) Para descobrir o maior elemento do vetor.\n");
	printf("b) Para somar todos elementos do vetor.\n");
	printf("c) Para obter a media aritmética dos elementos do vetor.\n");
	printf("q) Para sair do programa.\n");
}

int main(){

	int tamanho,
		maior,
		soma;

	int *s;

	char escolha;

	float media;


	printf("Qual o tamanho do vetor:\n");
	scanf("%d",&tamanho);
	s = alocaVetor(tamanho);

	printf("Digite os elemtos do vetor:\n");
	ler_vetor_int(s,tamanho);
	do{
		menu();
		scanf(" %c",&escolha);

		switch(escolha){
			case 'a':
				quickSort(s,0,tamanho);
				maior = maiorElementoRecursivo(s,tamanho,0);
				printf("\nO maior elemento do vetor: %d\n\n", maior);
				break;
			case 'b':
				soma = somaVetorRecursivo(s,tamanho,0,0);
				printf("\nA soma dos valores do vetor: %d\n\n", soma);
				break;
			case 'c':
				media = mediaAritmetica(s,tamanho);
				printf("\nA media aritmética é: %.3f\n\n", media);
				break;
			case 'q':
				exit(0);
				break;
			default:
				printf("Caracter invalido!!\n Tente novament.\n");
				break;
		}

	}while(escolha!='q');
	return 0;
}