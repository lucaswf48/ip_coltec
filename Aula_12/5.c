#include <stdio.h>
#include <math.h>

const int DIM=10;

void menu(){
	printf("Digite:\n");
	printf("a) BubleSort\n");
	printf("b) MergeSort\n");
	printf("c) Quicksort\n");
}

void bubbleSort(int vet[],int t){
	int b,a,al;
	for (a = 0; a <= t;a++){
		 for(b = 0; b <= t;b++){
			 if (vet[a] < vet[b]){
			 al = vet[a];
			 vet[a] = vet[b];
			 vet[b] = al;
			 }
		 }
	 }
}

int partition(int a[],int l, int r){
	int pivot,i,j,aux;
	pivot = a[l];
	i=l;
	j=r+1;
	while(1){
		do ++i;while(a[i] <= pivot && i <= r);
		do --j;while(a[j] > pivot);
		if(i >= j)break;
		aux = a[i];
		a[i]=a[j];
		a[j]= aux;
	}
	aux= a[l];
	a[l]= a[j];
	a[j]= aux;
	return j;
}

void quickSort(int a[],int l,int r){
	int j;
	if(l < r){
		j=partition(a,l,r);
		quickSort(a,l,j-1);
		quickSort(a,j+1,r);
	}
}

/*void merge(int a[],int low,int high,int mid){
	int i,j,k,c[50];
	i=low;
	j=mid+1;
	k=low;
	while((i<=mid)&&(j <= high)){
		if(a[i]<a[j]){
			c[k]=a[i];
			k++;
			i++;
		}
		else{
			c[k]=a[j];
			k++;
			j++;
		}
	}
	while(i<=mid){
		c[k]=a[i];
		k++;
		i++;
	}
	while(j<=high){
		c[k]=a[i];
		k++;
		j++;
	}
	for(i=low;i<k;i++)
		a[i]=c[i];
}

void mergeSort(int a[],int low,int high){
	int mid;
	if(low < high){
		mid=(low+high)/2;
		mergeSort(a,low,mid);
		mergeSort(a,mid+1,high);
		merge(a,low,high,mid);
	}
}*/

	void merge(int arr[], int l, int m, int r)
{
    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;
 
    
    int L[n1], R[n2];
 
    
    for(i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for(j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];
 
    
    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2){
        if (L[i] <= R[j]){

            arr[k] = L[i];
            i++;
        }
        else{

            arr[k] = R[j];
            j++;
        }
        k++;
    }
 
    
    while (i < n1){
        arr[k] = L[i];
        i++;
        k++;
    }
 
    
    while (j < n2){
        arr[k] = R[j];
        j++;
        k++;
    }
}
 
void mergeSort(int arr[], int l, int r)
{
    if (l < r)
    {
        int m = l+(r-l)/2;
        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);
        merge(arr, l, m, r);
    }
}


void imprime_vetor_int(int vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		printf("%d, ",vet[j]);
	}
	printf("\n");
}

int main(){

	int v[DIM],
		cont;
	char escolha;
	do{
		menu();


		srand(time(NULL));
		for(cont = 0;cont < DIM;cont++){

			v[cont]=rand()%100;

		}
		scanf(" %c",&escolha);
		switch(escolha){
			case 'a':
				imprime_vetor_int(v,DIM);

				printf("Ordenando\n");
				bubbleSort(v,DIM);
				imprime_vetor_int(v,DIM);

				break;
			case 'b':
				imprime_vetor_int(v,DIM);
				printf("Ordenando\n");
				mergeSort(v,0,10);

				imprime_vetor_int(v,DIM);
				break;
			case 'c':
				imprime_vetor_int(v,DIM);

				printf("Ordenando\n");
				quickSort(v,0,DIM);

				imprime_vetor_int(v,DIM);

				break;
			default:
				printf("Tente outra vez.\n");
		}

	}while(escolha!='q');
	return 0;
}
