/*4. Escreva uma função recursiva para determinar o número de
dígitos de um inteiro N.*/

#include <stdio.h>

int numeroDeDigitos(int n,int t){
	if(n/10<1){
		return t+1;
	}
	else{
		return numeroDeDigitos(n/10,t=t+1);
	}
}

int main(){

	int num,total;

	printf("Digite um número inteiro:\n");
	scanf("%d",&num);

	total = numeroDeDigitos(num,0);

	printf("Total de digitos: %d\n",total);

	return 0;
}