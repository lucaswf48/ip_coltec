/*1. Faça um procedimento recursivo que receba dois valores inteiros
a e b e imprima o intervalo fechado entre eles. Se a for maior que b
mostrar mensagem de erro.*/

#include <stdio.h>

void imprimeIntervalo(int a,int b){

	if(a>b){
		return;
	}
	else{
		printf("%d\n",a);
		imprimeIntervalo(a+1,b);
	}
}

int main(){

	int a,b;

	printf("Digite dois valores:\n");
	scanf("%d %d",&a,&b);

	if(a>b){
		printf("Tente novamente!\n");
	}
	else{
		printf("O intervalo fechado entre %d e %d é:\n", a,b);
		imprimeIntervalo(a,b);
	}
	return 0;
}