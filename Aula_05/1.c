/*1. Elabore um programa que calcule N! (fatorial de N), sendo que o valor
inteiro de N é fornecido pelo usuário.*/

#include <stdio.h>

int main ()

{
	int cont = 1,num;
	long double fatorial = 1;
	
	printf("Digite um número inteiro positivo:\n");
	scanf("%d",&num);
	
	while (cont <= num)
	{
		fatorial = fatorial * cont;
		++cont;
	}

	printf("O fatorial de %d é %.0Lf.\n", num,fatorial);
	return 0;
}