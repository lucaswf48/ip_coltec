/*3. Faça um programa que, dado um conjunto de valores inteiros e
positivos (fornecidos um a um pelo usuário), determine qual o menor e o
maior valor do conjunto. O final do conjunto de valores é conhecido
através do valor zero, que não deve ser considerado.*/
#include <stdio.h>

int main ()

{
	int num,cont = 0,maior,menor;
	do{
		printf("Digite um numero inteiro positivo:\n");
		scanf("%d",&num);
		if (num > 0)
		{	
			if (cont < 1)
			{
				maior = num;
				menor = num;
			}

			if (num > maior)
			{
				maior = num;
			}
			if (num < menor)
			{
				menor = num;
			}
		}
		else
		{
			printf("Número invalido.\n");
		}
		cont++;
	}while (num != 0);

	printf("O maior número digitado foi %d.\n",maior);
	printf("O menor número digitado foi %d\n",menor);

	return 0;
}