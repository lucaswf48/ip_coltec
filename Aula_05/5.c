/*5. A conversão de graus Fahrenheit para Centígrados é obtida pela
fórmula C = 9*(F-32)/5. Escreva um programa que calcule e escreva
uma tabela de graus centígrados em função de graus Fahrenheit que
variem de 50 a 150 de 1 em 1.*/
#include <stdio.h>

int main ()

{

	float c,f = 50;
		printf("Fahrenheit | Centígrados\n");
	
	while (f <= 150)
	{
		c = (f-32)/1.8;
		printf("   %f      |       %f     \n",f,c);
		f++;
	}
	return 0;
}