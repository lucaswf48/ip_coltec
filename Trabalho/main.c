#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "funcoes.h"
#include "cliente.h"
#include "gerente.h"

#define DIM 50



int loginSenha(char *tdu,char *login){

	char senha[DIM],
		 senha_arquivo[DIM],
		 login_arquivo[DIM],
		 usuario;

	int i = 0,da;

	FILE *p; //Arquivo de login

	p = fopen("login.txt","r");

	//Verifica se o arquivo existe
	//e se não existir, escreve nele o login e senha padrão
	if(p == NULL){

		printf("Erro em abrir o arquivo login.txt\n");
		printf("Aperte enter para criar um novo arquivo\n");
		scanf("%*c");
		p = fopen("login.txt","w");
		fprintf(p, "\n%s %s %c", "123","321",'g');
		fclose(p);
		p = fopen("login.txt","r");
		limpa();
	}

	printf("Login:");
	scanf("%s",login);
	limparBu(); 		//Limpa o buffer
	printf("\nSenha:");
	lersenha(senha); //Chama a função que le a senha
	limparBu();//Limpa o buffer

	//Procura no arquivo de login 
	while(fgetc(p) !=EOF){

		fscanf(p,"%s %s %c",login_arquivo,senha_arquivo,&usuario);

		if(strcmp (login,login_arquivo) == 0){
			i++;
			if(strcmp (senha,senha_arquivo) == 0){
				i++;
				*tdu=usuario;
				break;
			}
			else{
				i = 0;
			}
		}
	}

	fclose(p); //Fecha o arquivo
	return i; //Retorna 2 se o login foi feito do sucesso
}

void menuGerente(){

	int i;

	do{
		//Tela de menu do gerente
		printf("Criar ou excluir conta - Digite 1\n");
		printf("Alterar limites de transferência - Digite 2\n");
		printf("Listar clientes cadastrados - Digite 3\n");
		printf("Listar o total de reservas bancárias do banco - Digite 4\n");
		printf("Para deslogar do seu usuário - Digite 0\n");


		i = lerGerente();

		if(i == 0)
			return;
		else if(i == 1)
			criaOuExclue();
		else if(i == 2)
			alteraLimite();
		else if(i == 3)
			listaClientes();
		else if(i == 4)
			listaReserva();
	}while(i!=0);
}


void controleCliente(char *login){

	int i;
	do{
		//Tela de menu do cliente
		printf("Depositar e retirar valores - Digite 1\n");
		printf("Realizar transferências entre clientes - Digite 2\n");
		printf("Visualizar data do cadastro - Digite 3\n");
		printf("Visualizar limite de transferência - Digite 4\n");
		printf("Visualizar extrato - Digite 5\n");
		printf("Para deslogar do seu usuário - Digite 0\n");

		i = lerCliente();

		if(i == 0)
			return;
		else if(i == 1)
			depositaOuTira(login);
		else if(i == 2)
			transferencias(login);
		else if(i == 3)
			dataDeCadastro(login);
		else if(i == 4)
			verLimite(login);
		else if(i == 5)
			verExtrato(login);

	}while(i != 0);
}

//A função recebe como parametro a variavel que ira armazenar o login
char pedeLoginESenha(char *login){

	int i=0; //Armazena o retorno da função loginSenha
	int tenta=0; //Tentativas de login
	char tipo_de_usuario;

	int escolha;

	do{

		printf("Seja bem-vindo\n");
		printf("Digite 1 para fazer login\n");
		printf("Digite 2 para sair do programa\n");
		scanf("%d",&escolha);
		limpa();
	}while(escolha != 1 && escolha != 2);

	//Se a escolha for 2 sai do programa
	if (escolha==2)
		exit(0);
	//Senão chama a função de login e senha
	//Envia como parametro o tipo de usuario e o login
	i = loginSenha(&tipo_de_usuario,login);

	while(i != 2){
		limpa();
		if (tenta==2){
            exit(0);  //O usuário não vai mais poder digitar porém a tela não é fechada.
		}
		printf("Login ou senha invalidos.\nTente novamente.\nSomente 3 tentativas são permitidas.\n\n");
		tenta++;

		i = loginSenha(&tipo_de_usuario,login);

	}

	return tipo_de_usuario;
}

int main(){

	//Limpa a tela
	limpa();

	char tipo; //Armazena o tipo de usuario
	char login[DIM]; //Ira armazena a matricula

	tipo = pedeLoginESenha(login); //Função de login

	limpa();

	do{
		//Dependendo do tipo de usuario
		//E chamado sua tela de menu especifica 
		switch(tipo){
			case 'g':
				limpa();
				menuGerente();
				tipo = pedeLoginESenha(login);
				break;
			case 'c':
				limpa();
				controleCliente(login);
				tipo = pedeLoginESenha(login);
				break;

		}
	}while(1);//

}
