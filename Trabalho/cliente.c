#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "funcoes.h"
#include "cliente.h"

#define DIM 101
//Estrutura que armazena os dados do usuario
typedef struct cliente{


	char matricula[DIM];
	char aux[DIM];
	char nome[DIM];
	char data[DIM];
	char hora[DIM];
	char tipo[DIM];

	int saldo;
	int limite;
	int saldo_ataul;
	int saldo_novo;
	int deposita;
	int sacado;
	FILE *arquivo_do_cliente;
	FILE *arquivo_de_extrato;


}client;

int lerCliente(){

	int a;

	scanf("%d",&a);
	limparBu();
	limpa();

	return a;
}

//Função que realiza saque e depositos ou transferencias
//Copia tudo do arquivo do cliente e armazena em um arquivo temporario
//ja feita as modificações e copia devolta para o arquivo do cliente
void alteraArquivoCliente(int l,char *m){

	FILE *aux;

	client daw;

	daw.arquivo_do_cliente = fopen(m,"r");

	if(daw.arquivo_do_cliente == NULL){
		return;
	}

	aux = fopen("temp.txt","w");
	if(aux == NULL){
		return;
	}

	daw.deposita = l;

	

	fscanf(daw.arquivo_do_cliente,"Nome: %[^\n]\n",daw.nome);
	fscanf(daw.arquivo_do_cliente,"Saldo: %d\nLimite de transferência: %d\n",&daw.saldo,&daw.limite);
	fscanf(daw.arquivo_do_cliente,"Data de cadastro: %s %s\n",daw.hora,daw.data);


	daw.saldo += daw.deposita;


	fprintf(aux,"Nome: %s\nSaldo: %d\nLimite de transferência: %d\nData de cadastro: %s %s",daw.nome,daw.saldo,daw.limite,daw.hora,daw.data);

	fclose(aux);
	fclose(daw.arquivo_do_cliente);

	daw.arquivo_do_cliente = fopen(m,"w");
	aux = fopen("temp.txt","r");


	fscanf(aux,"Nome: %[^\n]\n",daw.nome);
	fscanf(aux,"Saldo: %d\nLimite de transferência: %d\n",&daw.saldo,&daw.limite);
	fscanf(aux,"Data de cadastro: %s %s\n",daw.hora,daw.data);

	fprintf(daw.arquivo_do_cliente,"Nome: %s\nSaldo: %d\nLimite de transferência: %d\nData de cadastro: %s %s",daw.nome,daw.saldo,daw.limite,daw.hora,daw.data);


	fclose(aux);
	fclose(daw.arquivo_do_cliente);

	remove("temp.txt");

	return;
}

//Função que faz o controle de depositos e saques
//Determina se o saque e possivel
//E gera uma string com o nome do arquivo do usuario
void depositaOuTira(char *login){

	int escolha;

	client atual;

	limpa();

	printf("Para fazer um deposito - Digite 1\n");
	printf("Para fazer retirar - Digite 2\n");
	scanf("%d",&escolha);

	limpa();

	strcpy(atual.matricula,login);

	strcpy(atual.aux,"");
	strcat(atual.aux,atual.matricula);
	strcat(atual.aux,".txt");//String com o nome do arquivo do cliente

	if(escolha == 1){

		printf("Digite o valor a ser depositado:\n");
		scanf("%d",&atual.deposita);

		alteraArquivoCliente(atual.deposita,atual.aux);

		strcpy(atual.aux,"ex");
		strcat(atual.aux,atual.matricula);
		strcat(atual.aux,".txt");


		atual.arquivo_de_extrato = fopen(atual.aux,"a");


		fprintf(atual.arquivo_de_extrato,"\n%s %d %s %s %s %s\n","Deposito",atual.deposita, atual.matricula,atual.matricula,escreveHoraFormatada(),escreveDataFormatada());

		fclose(atual.arquivo_de_extrato);

		limparBu();
		continua();
		limpa();
	}
	else if(escolha == 2){

		atual.arquivo_do_cliente = fopen(atual.aux,"r");

		fscanf(atual.arquivo_do_cliente,"Nome: %s\nSaldo: %d\nLimite de transferência: %d\nData de cadastro: %s %s",atual.nome,&atual.saldo,&atual.limite,atual.hora,atual.data);

		do{
			printf("Digite o valor a ser sacado:");
			scanf("%d",&atual.sacado);
		}while(atual.sacado>atual.saldo); //Está criando um loop

		fclose(atual.arquivo_do_cliente);

		alteraArquivoCliente(atual.sacado*(-1),atual.aux);

		strcpy(atual.aux,"ex");
		strcat(atual.aux,atual.matricula);
		strcat(atual.aux,".txt");

		atual.arquivo_de_extrato = fopen(atual.aux,"a");


		fprintf(atual.arquivo_de_extrato,"\n%s %d %s %s %s %s\n","Saca",atual.sacado, atual.matricula,atual.matricula,escreveHoraFormatada(),escreveDataFormatada());
		fclose(atual.arquivo_de_extrato);


		limparBu();
		continua();
		limpa();
	}
}

//Função que faz o controle de transferencias entre contas
//Determina se o transferencia e possivel
void transferencias(char *login){

	client atual,recebe;

	strcpy(atual.matricula,login);

	printf("Digite o número de matricula da outra conta:\n");
	scanf("%s",recebe.matricula);

	strcpy(recebe.aux,"");
	strcat(recebe.aux,recebe.matricula);
	strcat(recebe.aux,".txt");//String com o nome do arquivo do cliente

	recebe.arquivo_do_cliente = fopen(recebe.aux,"r");

	if(recebe.arquivo_do_cliente ==NULL){
		limpa();
		printf("Matricula não encontrada\n");
		limparBu();
		continua();
		limpa();
		return;
	}

	strcpy(atual.aux,"");
	strcat(atual.aux,atual.matricula);
	strcat(atual.aux,".txt");//String com o nome do arquivo do cliente


	atual.arquivo_do_cliente = fopen(atual.aux,"r");

	fscanf(atual.arquivo_do_cliente,"Nome: %s\nSaldo: %d\nLimite de transferência: %d\nData de cadastro: %s %s",atual.nome,&atual.saldo,&atual.limite,atual.hora,atual.data);

	do{
			limpa();
			printf("Digite o valor a ser sacado:");
			scanf("%d",&atual.sacado);
	}while(atual.sacado>atual.saldo || atual.sacado > atual.limite);



	alteraArquivoCliente(atual.sacado*(-1),atual.aux);

	fclose(atual.arquivo_do_cliente);

	recebe.deposita = atual.sacado;

	strcpy(recebe.aux,"");
	strcat(recebe.aux,recebe.matricula);
	strcat(recebe.aux,".txt");//String com o nome do arquivo do cliente

	alteraArquivoCliente(recebe.deposita,recebe.aux);


	strcpy(atual.aux,"ex");
	strcat(atual.aux,atual.matricula);
	strcat(atual.aux,".txt");

	atual.arquivo_de_extrato = fopen(atual.aux,"a");


	fprintf(atual.arquivo_de_extrato,"\n%s %d %s %s %s %s\n","Transfere",atual.sacado, atual.matricula,recebe.matricula,escreveHoraFormatada(),escreveDataFormatada());
	fclose(atual.arquivo_de_extrato);

	strcpy(recebe.aux,"ex");
	strcat(recebe.aux,recebe.matricula);
	strcat(recebe.aux,".txt");

	recebe.arquivo_de_extrato = fopen(recebe.aux,"a");


	fprintf(recebe.arquivo_de_extrato,"\n%s %d %s %s %s %s\n","Transfere",recebe.deposita, atual.matricula,recebe.matricula,escreveHoraFormatada(),escreveDataFormatada());
	fclose(recebe.arquivo_de_extrato);

	limpa();
	limparBu();

}


//Função que imprime a data de cadastro do usuario
void dataDeCadastro(char *login){

	client atual;


	strcpy(atual.matricula,login);

	strcpy(atual.aux,"");
	strcat(atual.aux,atual.matricula);
	strcat(atual.aux,".txt");

	atual.arquivo_do_cliente = fopen(atual.aux,"r");

	fscanf(atual.arquivo_do_cliente,"Nome: %s\nSaldo: %d\nLimite de transferência: %d\nData de cadastro: %s %s",atual.nome,&atual.saldo,&atual.limite,atual.hora,atual.data);

	limpa();
	printf("Data de cadastro:\n");
	printf("%s %s\n",atual.hora,atual.data);

	fclose(atual.arquivo_do_cliente);

	limparBu();
	continua();
	limpa();

	return;
}

//Função que imprime o limite de transferencia do usuario
void verLimite(char *login){

	client atual;

	strcpy(atual.matricula,login);

	strcpy(atual.aux,"");
	strcat(atual.aux,atual.matricula);
	strcat(atual.aux,".txt");

	atual.arquivo_do_cliente = fopen(atual.aux,"r");

	fscanf(atual.arquivo_do_cliente,"Nome: %s\nSaldo: %d\nLimite de transferência: %d\nData de cadastro: %s %s",atual.nome,&atual.saldo,&atual.limite,atual.hora,atual.data);

	limpa();

	printf("Limite de transferência: %d\n",atual.limite);


	fclose(atual.arquivo_do_cliente);

	limparBu();
	continua();
	limpa();

	return;
}

//Função que imprime o extrato do usuario
void verExtrato(char *login){


	client atual;

	strcpy(atual.matricula,login);

	strcpy(atual.aux,"ex");
	strcat(atual.aux,atual.matricula);
	strcat(atual.aux,".txt");



	atual.arquivo_de_extrato = fopen(atual.aux,"r");

	if(atual.arquivo_de_extrato== NULL){
		printf("oadsssssssssque\n");
	}
        printf("Tipo\tValor\tConta 1\tConta 2\tHora\tData\n");
	while(!feof(atual.arquivo_de_extrato)){


		fscanf(atual.arquivo_de_extrato,"%s\t%d\t%s\t%s\t%s\t%s\n",atual.tipo,&atual.sacado,atual.matricula,atual.aux,atual.hora,atual.data);

		printf("%s %d %s %s %s %s\n\n",atual.tipo,atual.sacado,atual.matricula,atual.aux,atual.hora,atual.data);
	}


	limparBu();
	continua();
	limpa();

	fclose(atual.arquivo_de_extrato);
}
