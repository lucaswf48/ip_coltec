#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "funcoes.h"

#define DIM 50

#ifdef linux //Se o sistema operacional for linux 

//Inclue a biblioteca terminos.h
#include <termios.h>

//Define backspace com o valor 127 na tabela acsii
#define VOLTA 127

//Cria a função getch para linux, ja que ela esta apenas disponivel na conio.h
int getch(void) {
	struct termios lxgetch_orig, lxgetch_new;
	int lxgetch_ch;
	tcgetattr(STDIN_FILENO, &lxgetch_orig);
	lxgetch_new = lxgetch_orig;
	lxgetch_new.c_lflag &= ~(ICANON | ECHO);
	lxgetch_new.c_iflag &= ~(ISTRIP | INPCK);
	tcsetattr(STDIN_FILENO, TCSANOW, &lxgetch_new);
	lxgetch_ch = getchar();
	tcsetattr(STDIN_FILENO, TCSANOW, &lxgetch_orig);
	return lxgetch_ch;
}

//Cria a função para limpar o buffer com o __fpurge(stdin)
void limparBu(){

	__fpurge(stdin);
}

//Cria a função que limpa a tela com system("clear")
void limpa(){

	system("clear");
}

#elif WIN32 //Se o sistema operacional for windows

//Inclue a conio.h para para utilizar o getch
#include <conio.h>

//Define backspace com o valor 8 na tabela acsii
#define VOLTA 8

//Cria a função para limpar o buffer com o fflush(stdin)
void limparBu(){

	fflush(stdin);
}

//Cria a função que limpa a tela com system("cls")
void limpa(){

	system("cls");
}

#endif

//Função responsavel por ler a senha, sem exibir os caracteres

//Recebe como paramentro um ponteiro aque aponta para a string senha
void lersenha(char *daw){


	int cont;

	//Lê 15 caracteres
	for(cont = 0;cont < 16;++cont){

		//Armazena o caracter digitado na posição cont da string
		daw[cont]=getch();

		//
		if(cont<0){

			cont=1;
		}

		//se o caracter digitado é enter, sai do loop
		if(daw[cont]==13 || daw[cont]==10){

			//daw[cont]='\0'
			daw[cont]=0;
			break;
		}
		//senão se(?) o caracter digitado é  o backspace
		else if(daw[cont]==VOLTA){

			//o caracter digitado é o backspace
			while(daw[cont]==VOLTA){

				//Descrementa o contador
				--cont;

				//Se o contador ficar menor que zero, ele e forçado em 0
				if(cont<0)
					cont=0;
				//Lê novo caracter que sera armazenado no lugar do que foi a
				daw[cont]= getch();
			}

			//se o caracter digitado é enter, sai do loop
			if(daw[cont]==13){

				daw[cont]=0;
				break;
			}
		}
	}

}

//Função que retorna a hora atual no formato hora/minuto/segundo, na forma de uma string
char* escreveHoraFormatada(){

	int segundo,
		minuto,
		hora;

	char segundoS[DIM],
		minutoS[DIM],
		horaS[DIM],
		*horaformatado;

	horaformatado = (char *)malloc(DIM*sizeof(char));

	time_t hora_atual = time (NULL);
	struct tm *t = localtime (&hora_atual);

	segundo = t->tm_sec;
	minuto = t->tm_min;
	hora = t->tm_hour;

	sprintf(horaS,"%02d",hora);
	sprintf(minutoS,"%02d",minuto);
	sprintf(segundoS,"%02d",segundo);
	sprintf(horaformatado,"%s:%s:%s",horaS,minutoS,segundoS);

	return horaformatado;
}

//Função que retorna a hora atual no formato dd/mm/aaaa, na forma de uma string
char* escreveDataFormatada(){


	int dia,
		mes,
		ano;

	char diaS[DIM],
		mesS[DIM],
		anoS[DIM],
		*dataformatado;

	dataformatado = (char *)malloc(DIM*sizeof(char));

	time_t hora_atual = time (NULL);
	struct tm *t = localtime (&hora_atual);

	dia = t->tm_mday;
	mes = t->tm_mon + 1;
	ano = t->tm_year + 1900;

	sprintf(diaS,"%02d",dia);
	sprintf(mesS,"%02d",mes);
	sprintf(anoS,"%02d",ano);
	sprintf(dataformatado,"%s/%s/%s",diaS,mesS,anoS);

	return dataformatado;
}


void continua(){

	printf("Aperte qualquer tecla para continuar\n");
	getch();
}

