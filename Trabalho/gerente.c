#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "funcoes.h"
#include "gerente.h"

#define DIM 101

typedef struct cliente{

	char nome[DIM];
	char matriculaS[DIM];
	char nome_do_arquivo[DIM];
	char arquivo_de_extrato[DIM];
	char hora[DIM];
	char data[DIM];
	char senha[16];
	int limite;
	int saldo;
	long int numero_gerado;

	FILE *arquivo_do_cliente;
	
}cliente;

typedef struct clona{

	char numero[DIM];
	char sen[DIM];
	char tipo;

}clona;

typedef struct arq{

	char name[DIM];
	char h[DIM];
	char d[DIM];

}arq;

typedef struct gerente{
	
	char matriculaGerente[DIM];
	long int gerado;
	char senhaGerente[16];
}gerente;


int lerGerente(){

	int a;

	scanf("%d",&a);
	limparBu();
	limpa();

	return a;
}

//Função que verifica se a matricula ja existe no arquivo login.txt
int avaliaMatricula(char *m){

	FILE *p;
	char ma[50];
	int r = 1;

	p = fopen("login.txt","r");

	while(!feof(p)){

		fscanf(p,"%s",ma);
		if(strcmp(ma,m)==0){
			r=0;
			break;
		}
		else{
            r = 1;
		}

	}
	fclose(p);
	return r;
}


int procuraMatricula(char *m){

	FILE *p;
	char ma[50];
	int i=0;

	p = fopen("login.txt","r");
	while(!feof(p)){
		fscanf(p,"%s",ma);
		if (strcmp(m,ma)==0){
			i=1;
			break;
		}
		else{
			i=0;
		}
	}

	fclose(p);
	return i;
}

//Função que clona e faz as modificaçoes no arquivo de login
//caso algum usuario seja deletado
void clonaArquivoLogin(char *daw){

	FILE *arquivo_de_login,
		*arquivo_temporario;

	clona p;

	arquivo_de_login = fopen("login.txt","r");
	arquivo_temporario = fopen("temp.txt","w");

	while(!feof(arquivo_de_login)){

		fscanf(arquivo_de_login,"%s %s %c\n",p.numero,p.sen,&p.tipo);
		if(strcmp(p.numero,daw)!=0){

			fprintf(arquivo_temporario, "\n%s %s %c\n", p.numero,p.sen,p.tipo);
		}
	}

	fclose(arquivo_de_login);
	fclose(arquivo_temporario);


	arquivo_de_login = fopen("login.txt","w");
	arquivo_temporario = fopen("temp.txt","r");

	while(!feof(arquivo_temporario)){

		fscanf(arquivo_temporario,"%s %s %c\n",p.numero,p.sen,&p.tipo);

			
			fprintf(arquivo_de_login, "\n%s %s %c\n", p.numero,p.sen,p.tipo);
	}

	fclose(arquivo_de_login);
	fclose(arquivo_temporario);

	remove("temp.txt");
}

//Função que clona e faz as modificaçoes no arquivo do usuario
//caso o limite seja alterado
void clonaArquivoCliente(int l,char *m){

	FILE *arquivo_do_cliente,
		*aux;

	cliente daw;

	char hora[DIM],data[DIM];

	arquivo_do_cliente = fopen(m,"r");
	aux = fopen("temp.txt","w");



	fscanf(arquivo_do_cliente,"Nome: %[^\n]\n",daw.nome);
	fscanf(arquivo_do_cliente,"Saldo: %d\nLimite de transferência: %d\n",&daw.saldo,&daw.limite);
	fscanf(arquivo_do_cliente,"Data de cadastro: %s %s\n",hora,data);
	
	daw.limite = l;

	fprintf(aux,"Nome: %s\nSaldo: %d\nLimite de transferência: %d\nData de cadastro: %s %s",daw.nome,daw.saldo,daw.limite,hora,data);
		


	fclose(aux);
	fclose(arquivo_do_cliente);

	arquivo_do_cliente = fopen(m,"w");
	aux = fopen("temp.txt","r");


	//fgets(daw.nome,DIM,aux);
	fscanf(aux,"Nome: %[^\n]",daw.nome);
	fscanf(aux,"Saldo: %d\nLimite de transferência: %d",&daw.saldo,&daw.limite);
	fscanf(aux,"Data de cadastro: %s %s\n",hora,data);
		
	fprintf(arquivo_do_cliente,"Nome: %s\nSaldo: %d\nLimite de transferência: %d\nData de cadastro: %s %s",daw.nome,daw.saldo,daw.limite,hora,data);
		


	fclose(aux);
	fclose(arquivo_do_cliente);

	remove("temp.txt");

}

//Função que cria ou exclu cliente, cria ou exclue gerente
void criaOuExclue(){

	srand(time(NULL));

	int a,
		cont=0;
	char nome[51],
		es;

	do{
		if(cont != 0){
			printf("Opção invalida:\n");
			printf("Tente novamente\n\n");
		}
		printf("Para criar uma nova conta cliente - Digite 1\n");
		printf("Para deletar uma conta cliente - Digite 2\n");
		printf("Para criar uma nova conta gerente - Digite 3\n");
		printf("Para deletar uma conta de gerente - Digite 4\n");
		printf("Para voltar - Digite 0\n");
		scanf("%d",&a);

		limparBu();
		limpa();
		if (a == 0){
			return;
		}
		cont++;
	}while((a != 1) && (a != 2) && (a != 3) && (a != 4));


	if(a==1){

		int r = 1;		//Variavel utilizada para verificar se a matricula ja exite

		//Arquivos que serão abertos
		FILE *arquivo_de_login, //Arquivo login.txt que armazena o login e senha
			 *ordem,//Arquivo cadastro.txt que é utilizado para imprimir os usuarios por ordem de cadastro
			 *extrato; 			

		cliente novo;			//Struct que armazena os dados do cliente
		char res;

		limpa();

		do{

            novo.numero_gerado = rand()%10000000; //Gera um número aleatorio para ser o número da conta
			sprintf(novo.matriculaS,"%ld",novo.numero_gerado); //Converte o número gerado para um string
			r  = avaliaMatricula(novo.matriculaS); //Verifica se o número da conta ja exite no arquivo login.txt

		}while(r == 0);


		printf("Digite o nome completo do cliente:\n");
		gets(novo.nome);

		printf("\nDigite o limite de transferência:\n");
		scanf("%d",&novo.limite);

        r=0;
		do{
            if(r!=0){
                limpa();
                printf("Senha invalida");
            }
			printf("\nDigite a senha da nova conta:(minimo 8 digitos e maximo 15)\n");
			scanf("%s",&novo.senha);
			r++;
		}while(strlen(novo.senha)<8);	//Senha com no minimo de 8 caracteres e no maximo de 15 caracteres
		limparBu();
		limpa();

		do{
			printf("Nome do cliente: %s\n", novo.nome);
			printf("Limite de transferência: %d\n", novo.limite);
			printf("Senha: %s\n",novo.senha);
			printf("Matricula: %s\n",novo.matriculaS);
			printf("As informações estão certas?[s/n]\n");
			scanf("%c",&res);
			limparBu();
			limpa();
		}while(res != 'n'&&res != 's');

		if(res == 'n'){
			limpa();
			printf("Operação abortada\n");
			continua();
			limpa();

			return;
		}

		arquivo_de_login = fopen("login.txt","a"); //Abre o arquivo login.txt
		
		if(arquivo_de_login == NULL){ //Se ocorrer alguma falha em abrir o arquivo a operação é abortada

			limpa();

			printf("Erro em abrir o arquivo \"login.txt\" \n");
			printf("Operação abortada\n");

			continua();
			return;
		}


		//Deixa a string que vai receber o nome do arquivo do usuario vazia
		strcpy(novo.nome_do_arquivo,"");
		
		//Concatena o número da conta do cliente com outra string
		strcat(novo.nome_do_arquivo,novo.matriculaS); 

		//Concatena a estensão .txt, para criar o nome do arquivo do usuário
		strcat(novo.nome_do_arquivo,".txt"); 


		novo.arquivo_do_cliente = fopen(novo.nome_do_arquivo,"w");//Criar o arquivo do cliente

		
		strcpy(novo.arquivo_de_extrato,"ex");
		strcat(novo.arquivo_de_extrato,novo.matriculaS);
		strcat(novo.arquivo_de_extrato,".txt"); 

		extrato = fopen(novo.arquivo_de_extrato,"w");

		if(novo.arquivo_do_cliente == NULL){ //Se ocorrer alguma falha em abrir o arquivo a operação é abortada

			limpa();

			printf("Erro em abrir o arquivo \"login.txt\" \n");
			printf("Operação abortada\n");

			continua();
			return;
		}

		
		//Escreve no  arquivo do cliente


		fprintf(novo.arquivo_do_cliente,"Nome: %s\nSaldo: %d\nLimite de transferência: %d\nData de cadastro: %s %s",novo.nome,0,novo.limite,escreveHoraFormatada(),escreveDataFormatada());

		//Escreve no arquivo login.txt
		fprintf(arquivo_de_login, "\n\n%s %s %c\n", novo.matriculaS,novo.senha,'c');

		//Fecha os arquivos
		fclose(novo.arquivo_do_cliente);
		fclose(arquivo_de_login);
		fclose(extrato);

		//Limpa a tela
		limpa();

		printf("Conta criada com sucesso\n");

		printf("Nome do cliente: %s\n", novo.nome);
		printf("Matricula do cliente: %s\n",novo.matriculaS);
		printf("Senha: %s\n", novo.senha);
		printf("Limite de transferência: %d\n", novo.limite);

		continua();
		limpa();
	}
	else if(a==2){

        char matriculaS[DIM];
        char aux[DIM];
		char escolha;
		int r,j;
		int cont = 0;
		char daw[DIM];

		limpa();
		printf("Digite a matricula da conta a ser deletada:\n");
		scanf("%s",matriculaS);
		limparBu();
		r = procuraMatricula(matriculaS);		
		if(r==1){
			clonaArquivoLogin(matriculaS);	
		}
		else{
			limpa();
			printf("Matricula não encontrada\n");
			printf("Operação abortada\n");
			limparBu();
			continua();
			limpa();
			return;
		}
		limparBu();
		
		strcat(matriculaS,".txt");
			
		remove(matriculaS);
		strcpy(aux,"ex");
		strcat(aux,matriculaS);
		remove(aux);
		
		limparBu();
		continua();
		limpa();
	}
	else if(a==3){

		int r,i=0;
		
		long int matricula;
		
		FILE *arquivo_de_login;

		gerente novo;
		
		limpa();
		do{
			novo.gerado = rand()%10000000;
			sprintf(novo.matriculaGerente,"%ld",novo.gerado);
			r  = avaliaMatricula(novo.matriculaGerente);
		}while(r == 0);


		do{

			printf("Digite a senha da nova conta:(minimo 5 digitos e maximo 15)\n");
			scanf("%s",novo.senhaGerente);
			limparBu();

		}while(strlen(novo.senhaGerente)<5);

		arquivo_de_login = fopen("login.txt","a");
		fprintf(arquivo_de_login, "\n\n%s %s %c", novo.matriculaGerente,novo.senhaGerente,'g');
		limpa();

		fclose(arquivo_de_login);
		printf("Gerente criado com sucesso\n");

		printf("Matricula: %s\n",novo.matriculaGerente);
	
		continua();
		limparBu();
		limpa();
	}
	else if(a==4){

		int r;
		char matriculaS[50];
		FILE *p;

		p = fopen("login.txt","a");
		limpa();
		printf("Digite a matricula do gerente que você deseja excluir:\n");
		scanf("%s",matriculaS);
		r = procuraMatricula(matriculaS);
		if(r==1){
			//printf("Matricula encontrada\n");
			clonaArquivoLogin(matriculaS);
		}
		else{
			limpa();
			printf("Matricula não encontrada.\n");
			printf("Operação abortada.\n");
			limparBu();
			continua();
			limpa();
			return;
		}
		fclose(p);
		limparBu();
		getch();
		limpa();
	}
}

//Função que faz o controle da alteração de limite
void alteraLimite(){

	limpa();

	char aux[DIM]="";
	int r;

	cliente altera;

	printf("Digite a matricula do cliente:\n");
	gets(altera.matriculaS);



	strcat(aux,altera.matriculaS);
	strcat(aux,".txt");

	r = procuraMatricula(altera.matriculaS);
	if(r==1){
		
		printf("Digite o novo limite:\n");
		scanf("%d",&altera.limite);
		limparBu();

		clonaArquivoCliente(altera.limite,aux);

		limpa();

		printf("Operação feita com sucesso\n");
		limparBu();
		continua();
		limpa();
		return;
	}
	else{
		limpa();
		printf("Matricula não encontrada.\n");
		printf("Operação abortada.\n");
		limparBu();
		continua();
		limpa();
		return;
	}

}

//Função que lista os clientes
void listaClientes(){


	FILE *daw,*cl;
	char login_arquivo[DIM],
		 senha_arquivo[DIM],aux[DIM],
		 usuario;

	char n[DIM],h[DIM],d[DIM];
	int li,sald;




	daw = fopen("login.txt","r");

	while(!feof(daw)){



		fscanf(daw,"%s %s %c\n",login_arquivo,senha_arquivo,&usuario);

		if(usuario == 'c'){
			strcpy(aux,login_arquivo);
			strcat(aux,".txt");

			cl = fopen(aux,"r");

			fscanf(cl,"Nome: %[^\n]\n",n);
			fscanf(cl,"Saldo: %d\nLimite de transferência: %d\n",&li,&sald);
			fscanf(cl,"Data de cadastro: %s %s\n",h,d);

			
			fclose(cl);
			printf("Nome: %s\nSaldo: %d\nLimite de transferência: %d\nData de cadastro: %s %s\n",n,li,sald,h,d);
			printf("\n\n");
		}
	}
	fclose(daw);
	
	limparBu();
	continua();
	limpa();

	return;
}

//Função que lista todo o dinheiro do banco
void listaReserva(){

	cliente reserva;

	FILE *arquivo_de_login;
	char daww,aux[DIM];
	int total = 0;
	arquivo_de_login = fopen("login.txt","r");
	if(arquivo_de_login == NULL){

	}
	while(!feof(arquivo_de_login)){
		fscanf(arquivo_de_login,"%s %s %c\n",reserva.matriculaS,reserva.senha,&daww);

		if (daww =='c'){

			strcpy(aux,reserva.matriculaS);
			strcat(aux,".txt");
			
			reserva.arquivo_do_cliente = fopen(aux,"r");

			fscanf(reserva.arquivo_do_cliente,"Nome: %[^\n]\n",reserva.nome);
			fscanf(reserva.arquivo_do_cliente,"Saldo: %d\nLimite de transferência: %d\n",&reserva.saldo,&reserva.limite);
			fscanf(reserva.arquivo_do_cliente,"Data de cadastro: %s %s\n",reserva.hora,reserva.data);
			
	
			fclose(reserva.arquivo_do_cliente);
			total += reserva.saldo;
		}	
	}
	
	fclose(arquivo_de_login);
	printf("As reservas do banco dão um total de %d\n", total);

	limparBu();
	continua();
	limpa();
}
