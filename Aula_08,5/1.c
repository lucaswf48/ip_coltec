#include <stdio.h>
#include <stdlib.h>

float* aloca(int alu);
float media(float nota[],int t);

int main(){

	float *notas,r;
	int j,i;

	printf("Quantos alunos:\n");
	scanf("%d",&j);
	notas = aloca(j);
	r = media(notas,j);
	printf("A media dos alunos é %f\n", r);

	return 0;
}

float* aloca(int alu){

	int i;
	float *notas;
	notas = (float *) malloc(alu*sizeof(float));


	for (i = 0;i < alu;i++){
			printf("Digite a nota do aluno %d\n", i+1);
			scanf("%f",&notas[i]);
	}

	return notas;
}

float media(float nota[],int t){
	
	float total = 0.0;
	int cont;

	for (cont = 0;cont < t;cont++){
			total +=  nota[cont];
	}

	total = total/t;

	return total;
}