#include <stdio.h>
#define DIM 26

char calculaMontanteCategoria(int vet[],int *res);


int main(){

		int cont = 0,v[DIM],r = 0;
		char categoria;
		
		do{
			
			printf("Digite salario de número %d:\n",(cont+1));
			scanf("%d",&v[cont]);
			if (v[cont] < 600 || v[cont] > 1000)
			{
				printf("Salário invalido. Digite outro:\n");
			}
			else
			{
				cont++;
			}
			
		}while(cont < 25);

		categoria = calculaMontanteCategoria(v,&r);

		printf("A empresa se encaixa na categoria: %c\n", categoria);
		printf("O montante da empresa é %d\n",r);

	return 0;
}

char calculaMontanteCategoria(int vet[],int *res){

	int c;

	for(c = 0;c < 25;c++){
		*res = *res + vet[c];
	}

	if (*res <= 15000){
		return 'M';
	}
	else if(*res > 15000 && *res <= 21250){
		return 'P';
	}
	else if(*res > 21250){
		return 'G';
	}
	
}

